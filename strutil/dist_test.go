// Copyright 2023 Daniel Erat.
// All rights reserved.

package strutil

import (
	"testing"
)

func TestLevenshtein(t *testing.T) {
	for _, tc := range []struct {
		a, b string
		want Edits
	}{
		{"a", "", Edits{Dels: 1}},
		{"", "a", Edits{Ins: 1}},
		{"a", "b", Edits{Subs: 1}},
		{"ab", "b", Edits{Dels: 1}},
		{"a", "ab", Edits{Ins: 1}},
		{"abde", "bcd", Edits{Ins: 1, Dels: 2}},
		{"my name is john", "my first name is john", Edits{Ins: 6}},
		{"a red cow", "a tan cow", Edits{Subs: 3}},
	} {
		if got := Levenshtein(tc.a, tc.b); got != tc.want {
			t.Errorf("Levenshtein(%q, %q) = %+v; want %+v", tc.a, tc.b, got, tc.want)
		}
	}
}

func FuzzLevenshtein(f *testing.F) {
	f.Add("foo", "foo")
	f.Add("food", "foo")
	f.Add("foo", "food")
	f.Add("food", "ford")
	f.Fuzz(func(t *testing.T, a, b string) {
		edits := Levenshtein(a, b)
		max := len(a)
		if lb := len(b); lb > max {
			max = lb
		}
		if dist := edits.Dist(); dist > max {
			t.Errorf("Got dist %d for strings of lengths %d and %d", dist, len(a), len(b))
		} else if dist < 0 {
			t.Errorf("Got negative dist %d", dist)
		} else if a == b && dist != 0 {
			t.Errorf("Got non-zero dist %d for matching strings", dist)
		}
	})
}
