// Copyright 2024 Daniel Erat.
// All rights reserved.

package strutil

// Truncate truncates orig to at most max UTF-8 characters.
// If elide is true, the final character of a truncated string is changed to "…".
func Truncate(orig string, max int, elide bool) string {
	if orig == "" || max <= 0 {
		return ""
	}

	runes := []rune(orig)
	if len(runes) <= max {
		return orig
	}
	if elide {
		return string(runes[:max-1]) + "…"
	}
	return string(runes[:max])
}

