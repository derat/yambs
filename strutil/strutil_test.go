// Copyright 2024 Daniel Erat.
// All rights reserved.

package strutil

import (
	"testing"
)

func TestTruncate(t *testing.T) {
	for _, tc := range []struct {
		orig   string
		max    int
		ellide bool
		want   string
	}{
		{"abc", 4, true, "abc"},
		{"abc", 3, true, "abc"},
		{"abc", 2, true, "a…"},
		{"abc", 1, true, "…"},
		{"abc", 0, true, ""},
		{"abc", -1, true, ""},
		{"abc", 4, false, "abc"},
		{"abc", 3, false, "abc"},
		{"abc", 2, false, "ab"},
		{"abc", 1, false, "a"},
		{"abc", 0, false, ""},
		{"abc", -1, false, ""},

		// Check that empty input is handled reasonably.
		{"", 1, true, ""},
		{"", 1, false, ""},
		{"", 0, true, ""},
		{"", 0, false, ""},

		// Check that multi-byte characters aren't split.
		{"Straße", 6, false, "Straße"},
		{"Straße", 5, false, "Straß"},
		{"Straße", 4, false, "Stra"},
		{"街道", 2, false, "街道"},
		{"街道", 1, false, "街"},
	} {
		if got := Truncate(tc.orig, tc.max, tc.ellide); got != tc.want {
			t.Errorf("Truncate(%q, %v, %v) = %q; want %q", tc.orig, tc.max, tc.ellide, got, tc.want)
		}
	}
}

