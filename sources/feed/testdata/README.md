# Feed testdata

This directory contains feed data for use in unit tests.

The feeds have been manually shortened to contain fewer items.

Original sources:

* `heavyweight.xml`: <https://web.archive.org/web/20211201012815id_/https://feeds.megaphone.fm/heavyweight>
* `lexicon_valley.xml`: <https://feeds.megaphone.fm/slatelexiconvalley>
* `this_american_life.xml`: <https://feeds.feedburner.com/talpodcast>
