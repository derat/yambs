// Copyright 2024 Daniel Erat.
// All rights reserved.

package feed

import (
	"os"
	"path/filepath"
	"testing"
	"time"

	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestReadRSSFeed(t *testing.T) {
	for _, tc := range []struct {
		fn   string
		want feed
	}{
		{
			"heavyweight.xml", feed{
				title:   "Heavyweight",
				feedURL: "https://feeds.megaphone.fm/heavyweight",
				link:    "https://gimletmedia.com/show/heavyweight/",
				author:  "Gimlet",
				items: []item{
					{
						title:    "Barbara Shutt",
						author:   "Gimlet",
						pubDate:  parseTime(t, "2021-11-18T09:00:00Z"),
						episode:  40,
						duration: 2496 * time.Second,
						audioURL: "https://traffic.megaphone.fm/GLT3040947282.mp3?updated=1637172965",
					},
					{
						title:    "Stephen",
						author:   "Gimlet",
						pubDate:  parseTime(t, "2021-10-28T08:00:00Z"),
						episode:  39,
						duration: 2236 * time.Second,
						audioURL: "https://traffic.megaphone.fm/GLT4613409293.mp3?updated=1635368199",
					},
				},
			},
		},
		{
			"lexicon_valley.xml", feed{
				title:   "Slate Debates",
				feedURL: "https://feeds.megaphone.fm/slatelexiconvalley",
				link:    "slate.com/podcasts",
				author:  "Slate Podcasts",
				items: []item{
					{
						title:    "Lexicon Valley #4: Jumpin' Salty in The O",
						author:   "Slate Podcasts",
						pubDate:  parseTime(t, "2012-02-27T03:54:00Z"),
						duration: 2468 * time.Second,
						audioURL: "https://www.podtrac.com/pts/redirect.mp3/pdst.fm/e/chtbl.com/track/28D492/traffic.megaphone.fm/SM1294041497.mp3",
					},
					{
						title:    "Lexicon Valley #3: Consider the Lamppost",
						author:   "Slate Podcasts",
						pubDate:  parseTime(t, "2012-02-20T05:01:00Z"),
						duration: 1809 * time.Second,
						audioURL: "https://www.podtrac.com/pts/redirect.mp3/pdst.fm/e/chtbl.com/track/28D492/traffic.megaphone.fm/SM6658044341.mp3",
					},
				},
			},
		},
		{
			"this_american_life.xml", feed{
				title:   "This American Life",
				feedURL: "https://www.thisamericanlife.org/podcast/rss.xml",
				link:    "https://www.thisamericanlife.org",
				author:  "This American Life",
				items: []item{
					{
						title:    "825: Yousef",
						link:     "https://www.thisamericanlife.org/825/yousef",
						author:   "This American Life",
						pubDate:  parseTime(t, "2024-03-03T18:00:00-05:00"),
						duration: time.Hour + 6*time.Minute + 3*time.Second,
						audioURL: "https://pfx.vpixl.com/6qj4J/dts.podtrac.com/redirect.mp3/chtbl.com/track/8DB4DB/pdst.fm/e/nyt.simplecastaudio.com/bbbcc290-ed3b-44a2-8e5d-5513e38cfe20/episodes/4ffd11f6-dc7b-4682-8080-378f24f41c59/audio/128/default.mp3?awCollectionId=bbbcc290-ed3b-44a2-8e5d-5513e38cfe20&awEpisodeId=4ffd11f6-dc7b-4682-8080-378f24f41c59",
					},
					{
						title:    "552: Need To Know Basis",
						link:     "https://www.thisamericanlife.org/552/need-to-know-basis",
						author:   "This American Life",
						pubDate:  parseTime(t, "2015-03-28T00:00:00Z"),
						duration: time.Hour + 4*time.Minute + 12*time.Second,
						audioURL: "https://pfx.vpixl.com/6qj4J/dts.podtrac.com/redirect.mp3/chtbl.com/track/8DB4DB/pdst.fm/e/nyt.simplecastaudio.com/2c7f3db9-8155-4a67-92db-baa5d5646134/episodes/c4a1bbda-76cf-4357-9f57-be3360fe6166/audio/128/default.mp3?awCollectionId=2c7f3db9-8155-4a67-92db-baa5d5646134&awEpisodeId=c4a1bbda-76cf-4357-9f57-be3360fe6166",
					},
				},
			},
		},
	} {
		t.Run(tc.fn, func(t *testing.T) {
			f, err := os.Open(filepath.Join("testdata", tc.fn))
			if err != nil {
				t.Fatal(err)
			}
			t.Cleanup(func() { f.Close() })

			if got, err := readRSSFeed(f); err != nil {
				t.Errorf("readRSSFeed(%q) failed: %v", tc.fn, err)
			} else if diff := cmp.Diff(tc.want, *got, cmp.AllowUnexported(feed{}, item{})); diff != "" {
				t.Error("Bad feed:\n" + diff)
			}
		})
	}
}

// parseTime parses an RFC 3339 timestamp.
func parseTime(t *testing.T, s string) time.Time {
	tm, err := time.Parse(time.RFC3339, s)
	if err != nil {
		t.Fatal(err)
	}
	return tm
}

func TestParseRSSPubDate(t *testing.T) {
	const wantErr = -13 * time.Second // arbitrary
	for _, tc := range []struct {
		in   string
		want string // time.RFC3339 or empty for error
	}{
		{"Fri, 23 Aug 2024 13:42:01 -0400", "2024-08-23T13:42:01-04:00"},
		{"Fri, 23 Aug 2024 13:42:01 +0400", "2024-08-23T13:42:01+04:00"},
		{"Fri, 23 Aug 2024 13:42:01 +0000", "2024-08-23T13:42:01Z"},
		{"Fri, 23 Aug 2024 13:42:01 GMT", "2024-08-23T13:42:01Z"},
		{"Fri, 23 Aug 2024 13:42:01 UTC", "2024-08-23T13:42:01Z"},
		{"Fri, 23 Aug 2024 13:42:01 PDT", "2024-08-23T13:42:01-07:00"},
		{"Fri, 23 Aug 2024 13:42:01 EDT", "2024-08-23T13:42:01-04:00"},
		{"23 Aug 2024 13:42:01 -0700", "2024-08-23T13:42:01-07:00"},
		{"23 Aug 2024 13:42:01 PDT", "2024-08-23T13:42:01-07:00"},
		{"23 Aug 24 13:42:01 -0700", "2024-08-23T13:42:01-07:00"},
		{"23 Aug 24 13:42:01 PDT", "2024-08-23T13:42:01-07:00"},
		{"Fri, 23 Aug 2024 13:42:01 BST", ""}, // unsupported time zone
		{"Fri, 23 Aug 2024 13:42:01 ZZZ", ""}, // invalid time zone
		{"Fri, 23 Aug 2024 13:42:01", ""},     // no time zone
		{"foo bar", ""},
		{"foo", ""},
		{"", ""},
	} {
		if tm, err := parseRSSPubDate(tc.in); tc.want == "" {
			if err == nil {
				t.Errorf("parseRSSPubDate(%q) = %q; wanted an error", tc.in, tm.Format(time.RFC3339))
			}
		} else if err != nil {
			t.Errorf("parseRSSPubDate(%q) failed: %v", tc.in, err)
		} else if got := tm.Format(time.RFC3339); got != tc.want {
			t.Errorf("parseRSSPubDate(%q) = %v; want %v", tc.in, got, tc.want)
		}
	}
}

func FuzzParseRSSPubDate(f *testing.F) {
	f.Add("Fri, 23 Aug 2024 13:42:01 -0400")
	f.Add("Mon, 08 Jan 1996 04:21:37 EST")
	f.Add("23 Dec 03 23:06:41 GMT")
	f.Fuzz(func(t *testing.T, in string) {
		if tm, err := parseRSSPubDate(in); !tm.IsZero() && err != nil {
			t.Errorf("Got nonzero time %v but also got error %v", tm, err)
		}
	})
}

func TestParseRSSDuration(t *testing.T) {
	const wantErr = -13 * time.Second // arbitrary
	for _, tc := range []struct {
		in   string
		want time.Duration
	}{
		{"10", 10 * time.Second},
		{"210", 210 * time.Second},
		{"", wantErr},
		{"0", wantErr},
		{"-1", wantErr},
		{"10.5", wantErr},
		{"9223372036854775808", wantErr},
		{"0:03", 3 * time.Second},
		{"00:03", 3 * time.Second},
		{"0:00:03", 3 * time.Second},
		{"00:00:03", 3 * time.Second},
		{"2:13:57", 2*time.Hour + 13*time.Minute + 57*time.Second},
		{"00:00", wantErr},
		{":57", wantErr},
		{"123:57", wantErr},
		{":5", wantErr},
		{":", wantErr},
	} {
		if got, err := parseRSSDuration(tc.in); tc.want == wantErr {
			if err == nil {
				t.Errorf("parseRSSDuration(%q) = %v; wanted an error", tc.in, got)
			}
		} else if err != nil {
			t.Errorf("parseRSSDuration(%q) failed: %v", tc.in, err)
		} else if got != tc.want {
			t.Errorf("parseRSSDuration(%q) = %v; want %v", tc.in, got, tc.want)
		}
	}
}

func FuzzParseRSSDuration(f *testing.F) {
	f.Add("2")
	f.Add("3601")
	f.Add("01:56")
	f.Add("1:23:42")
	f.Add("-20")
	f.Fuzz(func(t *testing.T, in string) {
		dur, err := parseRSSDuration(in)
		if dur != 0 && err != nil {
			t.Errorf("Got nonzero duration %v but also got error %v", dur, err)
		}
		if dur < 0 {
			t.Errorf("Got negative duration %v", dur)
		} else if dur > seed.MaxAudioLength {
			t.Errorf("Got too-large duration %v", dur)
		}
	})
}
