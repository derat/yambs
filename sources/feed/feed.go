// Copyright 2024 Daniel Erat.
// All rights reserved.

// Package feed extracts information from feeds (typically describing podcasts).
package feed

import (
	"bufio"
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"

	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/sources/text"
)

const peekBytes = 8

// ReadFile reads the passed-in feed data and returns a recording or release edit for each item.
func ReadFile(ctx context.Context, r io.Reader, typ seed.Entity, rawSetCmds []string) ([]seed.Edit, error) {
	setCmds, err := text.ParseSetCommands(rawSetCmds, typ)
	if err != nil {
		return nil, err
	}

	// Detect the feed type and parse it.
	br := bufio.NewReader(r)
	b, err := br.Peek(peekBytes)
	if err != nil {
		return nil, err
	} else if b = bytes.TrimSpace(b); len(b) == 0 {
		return nil, errors.New("didn't find feed start")
	}
	var fd *feed
	switch b[0] {
	case '<':
		fd, err = readRSSFeed(br)
	default:
		// TODO: Support Atom and/or JSON Feed if they're actually used for any podcasts.
		// Apple's RSS 2.0-based specification (if one can call it that) seems to be firmly
		// entrenched: https://podcasters.apple.com/support/823-podcast-requirements
		return nil, fmt.Errorf("couldn't detect feed type from %q", b[0])
	}
	if err != nil {
		return nil, err
	} else if len(fd.items) == 0 {
		return nil, errors.New("no items in feed")
	}

	// Generate the edits from the feed.
	var edits []seed.Edit
	switch typ {
	case seed.RecordingEntity:
		edits = createRecordings(fd)
	case seed.ReleaseEntity:
		edits = createReleases(ctx, fd)
	default:
		return nil, fmt.Errorf("unsupported type %q", typ)
	}

	for i := range edits {
		// Make the episode number accessible via "{episode}" to it can be used in relationship
		// attributes: https://codeberg.org/derat/yambs/issues/44#issuecomment-2067614
		vars := map[string]string{}
		if len(edits) == len(fd.items) {
			vars["episode"] = strconv.Itoa(fd.items[i].episode)
		}
		for _, pair := range setCmds {
			if err := text.SetField(edits[i], pair[0], pair[1], vars); err != nil {
				return nil, fmt.Errorf("failed setting %q: %w", pair[0]+"="+pair[1], err)
			}
		}
	}
	return edits, nil
}

// feed describes a feed containing one or more items.
type feed struct {
	title   string
	feedURL string
	link    string
	author  string
	items   []item
}

// item contains an individual item (e.g. podcast episode) within a feed.
type item struct {
	title    string
	link     string
	author   string
	pubDate  time.Time
	episode  int
	duration time.Duration
	audioURL string
}

// createRecordings creates a new seed.Recording for each item in fd.
func createRecordings(fd *feed) []seed.Edit {
	var edits []seed.Edit
	for _, it := range fd.items {
		edit := seed.Recording{
			Name:   it.title,
			Length: it.duration,
		}
		if it.author != "" {
			edit.Artists = []seed.ArtistCredit{{NameAsCredited: it.author}}
		}
		// Use the feed URL if we have one before falling back to the audio URL.
		// TODO: Consider trying to simplify audio URLs (which can contain a bunch of redirectors),
		// either by removing query parameters or by looking for hostnames within path components.
		if url := nonEmpty(fd.feedURL, it.audioURL); url != "" {
			edit.URLs = []seed.URL{{
				URL:      url,
				LinkType: seed.LinkType_DownloadForFree_Recording_URL,
			}}
		}
		if !it.pubDate.IsZero() {
			edit.Disambiguation = it.pubDate.Format(time.DateOnly)
		}
		edits = append(edits, &edit)
	}
	return edits
}

// createReleases creates a new seed.Release for each item in fd.
// It attempts to follow the guidelines at
// https://musicbrainz.org/doc/Style/Specific_types_of_releases/Broadcast_programs.
func createReleases(ctx context.Context, fd *feed) []seed.Edit {
	var edits []seed.Edit
	for _, it := range fd.items {
		rel := seed.Release{
			Title:     fd.title,
			Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Broadcast},
			Status:    seed.ReleaseStatus_Official,
			Packaging: seed.ReleasePackaging_None,
			Barcode:   "none",
		}
		if author := nonEmpty(it.author, fd.author); author != "" {
			rel.Artists = []seed.ArtistCredit{{Name: author}}
		}
		if !it.pubDate.IsZero() {
			rel.Title = it.pubDate.Format(time.DateOnly) + ": " + rel.Title
			rel.Events = []seed.ReleaseEvent{{
				Date:    seed.DateFromTime(it.pubDate),
				Country: "XW",
			}}
		}
		// TODO: Also handle item titles like "Podcast Name #231: Episode Title", maybe?
		// https://feeds.megaphone.fm/slatelexiconvalley annoyingly uses "Slate Debates" as the feed
		// title and then puts the podcast name at the beginning of some item titles.
		// This seems like it has a risk of false positives, though.
		if ms := episodeRegexp.FindStringSubmatch(it.title); ms != nil {
			rel.Title += fmt.Sprintf(" #%v, “%v”", ms[1], ms[2])
		} else {
			rel.Title += fmt.Sprintf(", “%v”", it.title)
		}

		rel.Mediums = []seed.Medium{{
			Format: seed.MediumFormat_DigitalMedia,
			Tracks: []seed.Track{{
				Title:   rel.Title,
				Artists: rel.Artists,
				Length:  it.duration,
			}},
		}}

		addURL := func(url string, lt seed.LinkType) {
			rel.URLs = append(rel.URLs, seed.URL{URL: url, LinkType: lt})
		}
		if fd.feedURL != "" {
			addURL(fd.feedURL, seed.LinkType_DownloadForFree_Release_URL)
		}
		if it.link != "" {
			addURL(it.link, seed.LinkType_ShowNotes_Release_URL)
		}

		// Large feeds can generate a ton of requests to the Google Translate API,
		// so avoid hitting the network.
		rel.Autofill(ctx, false /* network */)

		edits = append(edits, &rel)
	}
	return edits
}

// episodeRegexp splits an episode title, e.g. "#231: Episode Title" or "231: Episode Title".
var episodeRegexp = regexp.MustCompile(`^#?(\d+):\s+(.*)$`)

// nonEmpty returns the first non-empty value after trimming.
func nonEmpty(vals ...string) string {
	for _, v := range vals {
		if t := strings.TrimSpace(v); t != "" {
			return t
		}
	}
	return ""
}
