// Copyright 2024 Daniel Erat.
// All rights reserved.

package feed

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestReadFile(t *testing.T) {
	ctx := context.Background()
	for _, tc := range []struct {
		fn      string
		typ     seed.Entity
		setCmds []string
		want    []seed.Edit
	}{
		{
			"heavyweight.xml", seed.RecordingEntity, []string{
				"rel0_type=740",
				"rel0_target=c06d1ff7-84a1-4508-83b2-1d873666ca17",
				"rel0_attr0_type=788",
				"rel0_attr0_text={episode}",
			}, []seed.Edit{
				&seed.Recording{
					Name:    "Barbara Shutt",
					Length:  2496 * time.Second,
					Artists: []seed.ArtistCredit{{NameAsCredited: "Gimlet"}},
					URLs: []seed.URL{{
						URL:      "https://feeds.megaphone.fm/heavyweight",
						LinkType: seed.LinkType_DownloadForFree_Recording_URL,
					}},
					Relationships: []seed.Relationship{{
						Target: "c06d1ff7-84a1-4508-83b2-1d873666ca17",
						Type:   seed.LinkType_PartOf_Recording_Series,
						Attributes: []seed.RelationshipAttribute{{
							Type:      seed.LinkAttributeType_Number,
							TextValue: "40",
						}},
					}},
					Disambiguation: "2021-11-18",
				},
				&seed.Recording{
					Name:    "Stephen",
					Length:  2236 * time.Second,
					Artists: []seed.ArtistCredit{{NameAsCredited: "Gimlet"}},
					URLs: []seed.URL{{
						URL:      "https://feeds.megaphone.fm/heavyweight",
						LinkType: seed.LinkType_DownloadForFree_Recording_URL,
					}},
					Relationships: []seed.Relationship{{
						Target: "c06d1ff7-84a1-4508-83b2-1d873666ca17",
						Type:   seed.LinkType_PartOf_Recording_Series,
						Attributes: []seed.RelationshipAttribute{{
							Type:      seed.LinkAttributeType_Number,
							TextValue: "39",
						}},
					}},
					Disambiguation: "2021-10-28",
				},
			},
		},
		{
			"this_american_life.xml", seed.ReleaseEntity, nil, []seed.Edit{
				&seed.Release{
					Title:   "2024-03-03: This American Life #825, “Yousef”",
					Artists: []seed.ArtistCredit{{Name: "This American Life"}},
					Types:   []seed.ReleaseGroupType{seed.ReleaseGroupType_Broadcast},
					Script:  "Latn",
					Status:  seed.ReleaseStatus_Official,
					Events: []seed.ReleaseEvent{{
						Date:    seed.Date{Year: 2024, Month: 3, Day: 3},
						Country: "XW",
					}},
					Packaging: seed.ReleasePackaging_None,
					Barcode:   "none",
					Mediums: []seed.Medium{{
						Format: seed.MediumFormat_DigitalMedia,
						Tracks: []seed.Track{{
							Title:   "2024-03-03: This American Life #825, “Yousef”",
							Artists: []seed.ArtistCredit{{Name: "This American Life"}},
							Length:  time.Hour + 6*time.Minute + 3*time.Second,
						}},
					}},
					URLs: []seed.URL{
						{
							URL:      "https://www.thisamericanlife.org/podcast/rss.xml",
							LinkType: seed.LinkType_DownloadForFree_Release_URL,
						},
						{
							URL:      "https://www.thisamericanlife.org/825/yousef",
							LinkType: seed.LinkType_ShowNotes_Release_URL,
						},
					},
				},
				&seed.Release{
					Title:   "2015-03-28: This American Life #552, “Need To Know Basis”",
					Artists: []seed.ArtistCredit{{Name: "This American Life"}},
					Types:   []seed.ReleaseGroupType{seed.ReleaseGroupType_Broadcast},
					Script:  "Latn",
					Status:  seed.ReleaseStatus_Official,
					Events: []seed.ReleaseEvent{{
						Date:    seed.Date{Year: 2015, Month: 3, Day: 28},
						Country: "XW",
					}},
					Packaging: seed.ReleasePackaging_None,
					Barcode:   "none",
					Mediums: []seed.Medium{{
						Format: seed.MediumFormat_DigitalMedia,
						Tracks: []seed.Track{{
							Title:   "2015-03-28: This American Life #552, “Need To Know Basis”",
							Artists: []seed.ArtistCredit{{Name: "This American Life"}},
							Length:  time.Hour + 4*time.Minute + 12*time.Second,
						}},
					}},
					URLs: []seed.URL{
						{
							URL:      "https://www.thisamericanlife.org/podcast/rss.xml",
							LinkType: seed.LinkType_DownloadForFree_Release_URL,
						},
						{
							URL:      "https://www.thisamericanlife.org/552/need-to-know-basis",
							LinkType: seed.LinkType_ShowNotes_Release_URL,
						},
					},
				},
			},
		},
		{
			"lexicon_valley.xml", seed.ReleaseEntity, []string{"artist0_name=Lexicon Valley"}, []seed.Edit{
				&seed.Release{
					Title:   "2012-02-27: Slate Debates, “Lexicon Valley #4: Jumpin' Salty in The O”",
					Artists: []seed.ArtistCredit{{Name: "Lexicon Valley"}},
					Types:   []seed.ReleaseGroupType{seed.ReleaseGroupType_Broadcast},
					Script:  "Latn",
					Status:  seed.ReleaseStatus_Official,
					Events: []seed.ReleaseEvent{{
						Date:    seed.Date{Year: 2012, Month: 2, Day: 27},
						Country: "XW",
					}},
					Packaging: seed.ReleasePackaging_None,
					Barcode:   "none",
					Mediums: []seed.Medium{{
						Format: seed.MediumFormat_DigitalMedia,
						Tracks: []seed.Track{{
							Title:   "2012-02-27: Slate Debates, “Lexicon Valley #4: Jumpin' Salty in The O”",
							Artists: []seed.ArtistCredit{{Name: "Lexicon Valley"}},
							Length:  2468 * time.Second,
						}},
					}},
					URLs: []seed.URL{{
						URL:      "https://feeds.megaphone.fm/slatelexiconvalley",
						LinkType: seed.LinkType_DownloadForFree_Release_URL,
					}},
				},
				&seed.Release{
					Title:   "2012-02-20: Slate Debates, “Lexicon Valley #3: Consider the Lamppost”",
					Artists: []seed.ArtistCredit{{Name: "Lexicon Valley"}},
					Types:   []seed.ReleaseGroupType{seed.ReleaseGroupType_Broadcast},
					Script:  "Latn",
					Status:  seed.ReleaseStatus_Official,
					Events: []seed.ReleaseEvent{{
						Date:    seed.Date{Year: 2012, Month: 2, Day: 20},
						Country: "XW",
					}},
					Packaging: seed.ReleasePackaging_None,
					Barcode:   "none",
					Mediums: []seed.Medium{{
						Format: seed.MediumFormat_DigitalMedia,
						Tracks: []seed.Track{{
							Title:   "2012-02-20: Slate Debates, “Lexicon Valley #3: Consider the Lamppost”",
							Artists: []seed.ArtistCredit{{Name: "Lexicon Valley"}},
							Length:  1809 * time.Second,
						}},
					}},
					URLs: []seed.URL{{
						URL:      "https://feeds.megaphone.fm/slatelexiconvalley",
						LinkType: seed.LinkType_DownloadForFree_Release_URL,
					}},
				},
			},
		},
	} {
		t.Run(fmt.Sprintf("%v-%v-%v", tc.fn, tc.typ, tc.setCmds), func(t *testing.T) {
			f, err := os.Open(filepath.Join("testdata", tc.fn))
			if err != nil {
				t.Fatal(err)
			}
			t.Cleanup(func() { f.Close() })

			if got, err := ReadFile(ctx, f, tc.typ, tc.setCmds); err != nil {
				t.Errorf("ReadFile(ctx, %q, %v, %v) failed: %v", tc.fn, tc.typ, tc.setCmds, err)
			} else if diff := cmp.Diff(tc.want, got); diff != "" {
				t.Error("Bad edits:\n" + diff)
			}
		})
	}
}
