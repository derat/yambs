// Copyright 2024 Daniel Erat.
// All rights reserved.

package feed

import (
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"time"

	"codeberg.org/derat/yambs/seed"
)

// readRSSFeed reads an RSS feed from r.
func readRSSFeed(r io.Reader) (*feed, error) {
	// If I put an `xml:"link"` tag on the Link struct below, it somehow causes AtomLink to be
	// empty. Setting a default namespace and using that in the tag seems to prevent this from
	// breaking.
	dec := xml.NewDecoder(r)
	dec.DefaultSpace = "default"
	var data struct {
		XMLName xml.Name `xml:"rss"`
		// <channel> is wholly unnecessary in the RSS specs (there can only be one) and it would be
		// nice to use the nested `xml:"channel>title"` syntax here, but I can't figure out how to
		// get namespaces to work. In particular, `xml:"channel>http://www.w3.org/2005/Atom link"`
		// refuses to match <atom:link>.
		Channel struct {
			Title    string `xml:"title"`
			Link     string `xml:"default link"`
			AtomLink struct {
				Href string `xml:"href,attr"`
				Rel  string `xml:"rel,attr"`
			} `xml:"http://www.w3.org/2005/Atom link"`
			Author     string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd author"`
			NewFeedURL string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd new-feed-url"`
			Items      []struct {
				Title       string `xml:"title"`
				ITunesTitle string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd title"`
				Link        string `xml:"link"`
				Author      string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd author"`
				PubDate     string `xml:"pubDate"`
				Episode     string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd episode"`
				Duration    string `xml:"http://www.itunes.com/dtds/podcast-1.0.dtd duration"`
				Enclosure   struct {
					URL  string `xml:"url,attr"`
					Type string `xml:"type,attr"`
				} `xml:"enclosure"`
			} `xml:"item"`
		} `xml:"channel"`
	}
	if err := dec.Decode(&data); err != nil {
		return nil, err
	}

	ch := data.Channel
	fd := feed{
		title:  strings.TrimSpace(ch.Title),
		link:   strings.TrimSpace(ch.Link),
		author: strings.TrimSpace(ch.Author),
	}
	if ch.AtomLink.Rel == "self" {
		fd.feedURL = strings.TrimSpace(ch.AtomLink.Href)
	} else if ch.NewFeedURL != "" {
		// This may not be the place where the feed was actually downloaded from,
		// but it's still seems better than nothing if there was no <atom:link>.
		fd.feedURL = ch.NewFeedURL
	}

	for _, di := range ch.Items {
		it := item{
			// Prefer itunes:title since it sometimes omits extra data like episode numbers.
			title:  nonEmpty(di.ITunesTitle, di.Title),
			link:   strings.TrimSpace(di.Link),
			author: strings.TrimSpace(di.Author),
		}
		if strings.HasPrefix(di.Enclosure.Type, "audio/") {
			it.audioURL = strings.TrimSpace(di.Enclosure.URL)
		}
		if s := strings.TrimSpace(di.PubDate); s != "" {
			var err error
			if it.pubDate, err = parseRSSPubDate(s); err != nil {
				return nil, fmt.Errorf("date %q: %w", s, err)
			}
		}
		if v, err := strconv.Atoi(strings.TrimSpace(di.Episode)); err == nil {
			it.episode = v
		}
		if s := strings.TrimSpace(di.Duration); s != "" {
			var err error
			if it.duration, err = parseRSSDuration(s); err != nil {
				return nil, fmt.Errorf("duration %q: %w", s, err)
			}
		}
		fd.items = append(fd.items, it)
	}
	return &fd, nil
}

// parseRSSPubDate parses a timestamp in RFC 822 format.
func parseRSSPubDate(s string) (time.Time, error) {
	// https://www.rssboard.org/rss-specification (RSS 2.0):
	// "All date-times in RSS conform to the Date and Time Specification of RFC 822, with the
	// exception that the year may be expressed with two characters or four characters (four
	// preferred)." (example: "Sat, 07 Sep 2002 00:00:01 GMT")
	//
	// In practice, most feeds seem to include the weekday and use numeric time zone offsets.
	for _, layout := range []string{
		"Mon, 02 Jan 2006 15:04:05 -0700",
		"Mon, 02 Jan 06 15:04:05 -0700",
		"02 Jan 2006 15:04:05 -0700",
		"02 Jan 06 15:04:05 -0700",
	} {
		if t, err := time.Parse(layout, s); err == nil {
			return t, nil
		}
	}

	// Check if the timestamp ends with a common time zone abbreviation.
	// time.Parse seems to assign a zero offset for all abbreviations that aren't
	// known by the current location, so we need to manually load an appropriate
	// location corresponding to the abbreviation.
	var abbr string
	if fs := strings.Fields(s); len(fs) > 0 {
		abbr = fs[len(fs)-1]
	}
	name := timeZoneAbbrevs[abbr]
	if name == "" {
		return time.Time{}, fmt.Errorf("unknown time zone abbreviation %q", abbr)
	}
	loc, err := time.LoadLocation(name)
	if err != nil {
		return time.Time{}, err
	}
	for _, layout := range []string{
		"Mon, 02 Jan 2006 15:04:05 MST",
		"Mon, 02 Jan 06 15:04:05 MST",
		"02 Jan 2006 15:04:05 MST",
		"02 Jan 06 15:04:05 MST",
	} {
		if t, err := time.ParseInLocation(layout, s, loc); err == nil {
			return t, nil
		}
	}

	return time.Time{}, errors.New("unknown format")
}

// timeZoneAbbrevs maps most of the U.S.-centric abbreviations from RFC 822 5.2 to names in
// the IANA Time Zone database. Military standard offsets (that is, A-Z except J) and "UT" are
// omitted, since I doubt anyone uses them and I don't see any way to get time.Parse to parse
// abbreviations shorter than three letters.
var timeZoneAbbrevs = map[string]string{
	"PDT": "America/Los_Angeles",
	"PST": "America/Los_Angeles",
	"MDT": "America/Denver",
	"MST": "America/Denver",
	"CDT": "America/Chicago",
	"CST": "America/Chicago",
	"EDT": "America/New_York",
	"EST": "America/New_York",
	"GMT": "UTC",
	"UTC": "UTC", // why not?
}

// parseRSSDuration parses a duration as an integer number of seconds, "HH:MM:SS", or "MM:SS".
// https://help.apple.com/itc/podcasts_connect/#/itcb54353390 just says
// "Different duration formats are accepted" (uh, thanks?), but see <itunes:duration> at
// https://support.google.com/podcast-publishers/answer/9889544?hl=en#episode_tags.
func parseRSSDuration(s string) (time.Duration, error) {
	if v, err := strconv.ParseInt(s, 10, 32); err == nil {
		if v <= 0 {
			return 0, errors.New("non-positive duration")
		} else if v > int64(seed.MaxAudioLength/time.Second) {
			return 0, errors.New("duration is too large")
		}
		return time.Duration(v) * time.Second, nil
	}
	if ms := rssDurRegexp.FindStringSubmatch(s); ms != nil {
		hours, _ := strconv.Atoi(nonEmpty(ms[1], "0"))
		mins, _ := strconv.Atoi(ms[2])
		secs, _ := strconv.Atoi(ms[3])
		dur := time.Duration(hours)*time.Hour + time.Duration(mins)*time.Minute + time.Duration(secs)*time.Second
		if dur == 0 {
			return 0, errors.New("duration is zero")
		}
		return dur, nil
	}
	return 0, errors.New("unknown format")
}

// rssDurRegexp matches (optional) hours, minutes, and seconds groups in "HH:MM:SS" or "MM:SS".
var rssDurRegexp = regexp.MustCompile(`^(?:(\d\d?):)?(\d\d?):(\d\d)$`)
