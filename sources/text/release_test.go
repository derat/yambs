// Copyright 2022 Daniel Erat.
// All rights reserved.

package text

import (
	"context"
	"strings"
	"testing"
	"time"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestRead_Release_All(t *testing.T) {
	const input = `
annotation=This was actually released accidentally.
artist0_credited=First Artist
artist0_join= feat. 
artist0_mbid=cd72c13c-a74e-4617-af5f-658409a36894
artist1_name=Second Artist
barcode=1234567890
disambiguation=Not the same
edit_note=https://www.example.org/\nsecond line
event0_country=xw
event0_date=2021-04-05
event1_day=2
event1_month=1
event1_year=2020
label0_catalog=CAT012
label0_mbid=bb9bba31-cb31-440d-a813-f5bf884f6adb
label1_name=Some Label
language=eng
make_votable=1
mbid=d98928e8-6757-4196-a945-e7145d94d9e4
medium0_format=CD
medium0_name=First Disc
medium0_track0_artist0_credited=Artist A
medium0_track0_artist0_join= & 
medium0_track0_artist0_mbid=4d0db17e-14e0-4904-a39b-a9ffa81890df
medium0_track0_artist1_name=Artist B
medium0_track0_length=1:02.56
medium0_track0_number=1
medium0_track0_recording=c347b502-7ac8-46bb-a19a-a5e758900fe1
medium0_track0_title=First Track
medium0_track1_length=45001
medium0_track1_title=Second Track
packaging=Digipak
release_group=4b52bddc-0587-4bcf-9e05-5c9fca260a21
script=Latn
status=Official
title=El título
types=Single,Soundtrack
url0_type=75
url0_url=https://www.example.org/a
url1_url=https://www.example.org/b
`
	got, err := Read(context.Background(),
		strings.NewReader(strings.TrimLeft(input, "\n")),
		KeyVal, seed.ReleaseEntity, nil, nil, mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}
	want := []seed.Edit{
		&seed.Release{
			MBID:         "d98928e8-6757-4196-a945-e7145d94d9e4",
			Title:        "El título",
			ReleaseGroup: "4b52bddc-0587-4bcf-9e05-5c9fca260a21",
			Types: []seed.ReleaseGroupType{
				seed.ReleaseGroupType_Single,
				seed.ReleaseGroupType_Soundtrack,
			},
			Disambiguation: "Not the same",
			Annotation:     "This was actually released accidentally.",
			Barcode:        "1234567890",
			Language:       "eng",
			Script:         "Latn",
			Status:         seed.ReleaseStatus_Official,
			Packaging:      seed.ReleasePackaging_Digipak,
			Events: []seed.ReleaseEvent{
				{Date: seed.MakeDate(2021, 4, 5), Country: "xw"},
				{Date: seed.MakeDate(2020, 1, 2)},
			},
			Labels: []seed.ReleaseLabel{
				{MBID: "bb9bba31-cb31-440d-a813-f5bf884f6adb", CatalogNumber: "CAT012"},
				{Name: "Some Label"},
			},
			Artists: []seed.ArtistCredit{
				{MBID: "cd72c13c-a74e-4617-af5f-658409a36894", NameAsCredited: "First Artist", JoinPhrase: " feat. "},
				{Name: "Second Artist"},
			},
			Mediums: []seed.Medium{{
				Format: seed.MediumFormat_CD,
				Name:   "First Disc",
				Tracks: []seed.Track{
					{
						Title:     "First Track",
						Number:    "1",
						Recording: "c347b502-7ac8-46bb-a19a-a5e758900fe1",
						Length:    time.Minute + 2*time.Second + 560*time.Millisecond,
						Artists: []seed.ArtistCredit{
							{MBID: "4d0db17e-14e0-4904-a39b-a9ffa81890df", NameAsCredited: "Artist A", JoinPhrase: " & "},
							{Name: "Artist B"},
						},
					},
					{Title: "Second Track", Length: 45001 * time.Millisecond},
				},
			}},
			URLs: []seed.URL{
				{URL: "https://www.example.org/a", LinkType: seed.LinkType_DownloadForFree_Release_URL},
				{URL: "https://www.example.org/b"},
			},
			EditNote:    "https://www.example.org/\nsecond line",
			MakeVotable: true,
		},
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}

func TestRead_Release_UTF16(t *testing.T) {
	// The leading BOM should be skipped, and the "ñ" should be interpreted correctly.
	// The trailing blank line should also be ignored.
	const input = "\xfe\xff\x00t\x00i\x00t\x00l\x00e\x00=\x00M\x00a\x00\xf1\x00a\x00n\x00a\x00\r\x00\n\x00\r\x00\n"
	want := []seed.Edit{&seed.Release{Title: "Mañana"}}
	if got, err := Read(context.Background(), strings.NewReader(input), KeyVal, seed.ReleaseEntity,
		nil, nil, mbdb.NewDB(mbdb.DisallowQueries), Charset("UTF-16BE")); err != nil {
		t.Error("Read failed:", err)
	} else if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}

func TestRead_Release_UnsupportedCharset(t *testing.T) {
	// As of v0.4.0, golang.org/x/text/encoding/ianaindex seems like it doesn't
	// support UTF-32. Make sure we return an error instead of segfaulting:
	// https://codeberg.org/derat/yambs/issues/41#issuecomment-1591410
	const input = "\x00\x00\x00\x4d\x00\x00\x00\x61\x00\x00\x00\xf1\x00\x00\x00\x61" +
		"\x00\x00\x00\x6e\x00\x00\x00\x61"
	if _, err := Read(context.Background(), strings.NewReader(input), CSV, seed.ReleaseEntity,
		[]string{"title"}, nil, mbdb.NewDB(mbdb.DisallowQueries), Charset("UTF-32")); err == nil {
		t.Error("Read unexpectedly accepted UTF-32")
	}
}

func TestRead_Release_RejectInvalidUTF8(t *testing.T) {
	const input = "title=Title\xff\nlanguage=eng\n"
	if _, err := Read(context.Background(), strings.NewReader(input), KeyVal,
		seed.ReleaseEntity, nil, nil, mbdb.NewDB(mbdb.DisallowQueries)); err == nil {
		t.Error("Read accepted invalid UTF-8 input")
	}
}

func TestRead_Release_SkipBOM(t *testing.T) {
	// The leading BOM should be skipped in UTF-8 input.
	const input = "\ufeffTitle,eng"
	want := []seed.Edit{&seed.Release{Title: "Title", Language: "eng"}}
	if got, err := Read(context.Background(), strings.NewReader(input), CSV, seed.ReleaseEntity,
		[]string{"title", "language"}, nil, mbdb.NewDB(mbdb.DisallowQueries)); err != nil {
		t.Error("Read failed:", err)
	} else if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}

func TestRead_Release_Synonyms(t *testing.T) {
	got, err := Read(context.Background(),
		strings.NewReader("name=Some Release\n"),
		KeyVal, seed.ReleaseEntity, nil, nil,
		mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}
	want := []seed.Edit{&seed.Release{Title: "Some Release"}}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}
