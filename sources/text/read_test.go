// Copyright 2023 Daniel Erat.
// All rights reserved.

package text

import (
	"context"
	"sort"
	"strings"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
)

func TestUnescapeValue(t *testing.T) {
	for _, tc := range []struct{ orig, want string }{
		{``, ""},
		{`foo`, "foo"},
		{`foo\nbar`, "foo\nbar"},
		{`foo\tbar`, "foo\tbar"},
		{`foo\abar`, "fooabar"},
		{`foo\ bar`, "foo bar"},
		{`foo\\bar`, `foo\bar`},
		{`foo\\nbar`, `foo\nbar`},
		{`foo\\\nbar`, "foo\\\nbar"},
		{`foo\`, `foo\`},
		{`\`, `\`},
	} {
		if got := unescapeValue(tc.orig); got != tc.want {
			t.Errorf("unescapeValue(%q) = %q; want %q", tc.orig, got, tc.want)
		}
	}
}

func FuzzNewRowReader_KeyVal(f *testing.F) {
	f.Add("title=Foo\nedit_note=Some Edit")
	f.Add(bom + "first=34\nsecond=blah\n# a comment\nthird=hello there")
	f.Fuzz(func(t *testing.T, in string) {
		rr, fieldsOut, err := newRowReader(strings.NewReader(in), KeyVal, nil)
		if err != nil {
			t.Fatal("newRowReader failed:", err)
		}
		row, err := rr.Read()
		if err == nil && len(row) != len(fieldsOut) {
			t.Fatalf("Got %d column(s) but %d field(s)", len(row), len(fieldsOut))
		}
	})
}

func FuzzRead_SingleField(f *testing.F) {
	entityFields := make(map[seed.Entity][]string)
	for _, et := range seed.EntityTypes {
		var fields []string
		for name := range typeFields[et] {
			fields = append(fields, strings.ReplaceAll(name, "*", "0"))
		}
		sort.Strings(fields)
		entityFields[et] = fields
	}

	f.Add("Some Name")
	f.Add("2024-04-03")
	f.Add("US")
	f.Add("true")
	f.Add("ab29ceea-cb39-4cfb-b719-0e7c90298169")
	f.Add("4:56:02.083")

	ctx := context.Background()
	db := mbdb.NewDB(mbdb.DisallowQueries)
	f.Fuzz(func(t *testing.T, in string) {
		for et, fields := range entityFields {
			for _, field := range fields {
				r := strings.NewReader(in)
				edits, err := Read(ctx, r, TSV, et, []string{field}, nil, db)
				if len(edits) == 0 && err == nil {
					t.Errorf("No edits or error returned for %v field %q", et, field)
				} else if len(edits) > 0 && err != nil {
					t.Errorf("Got %d edit(s) and an error for %v field %q", len(edits), et, field)
				}
			}
		}
	})
}
