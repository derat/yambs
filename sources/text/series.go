// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"codeberg.org/derat/yambs/seed"
)

// seriesFields defines fields that can be set in a seed.Series.
var seriesFields = map[string]fieldInfo{
	"disambiguation": {
		"Comment disambiguating this series from others with similar names",
		func(s *seed.Series, k, v string) error { return setString(&s.Disambiguation, v) },
	},
	"edit_note": {
		"Note attached to edit",
		func(s *seed.Series, k, v string) error { return setString(&s.EditNote, v) },
	},
	"make_votable": {
		`Make edits votable even if they would be auto-edits (“1” or “true” for true)`,
		func(s *seed.Series, k, v string) error { return setBool(&s.MakeVotable, v) },
	},
	"mbid": {
		"MBID of existing series to edit (if empty, create series)",
		func(s *seed.Series, k, v string) error { return setMBID(&s.MBID, v) },
	},
	"name": {
		"Series’s name",
		func(s *seed.Series, k, v string) error { return setString(&s.Name, v) },
	},
	"ordering_type": {
		"Integer [series ordering type](" + seriesOrderingTypeURL + ")",
		func(s *seed.Series, k, v string) error { return setInt((*int)(&s.OrderingType), v) },
	},
	"type": {
		"Integer [series type](" + seriesTypeURL + ")",
		func(s *seed.Series, k, v string) error { return setInt((*int)(&s.Type), v) },
	},
}

func init() {
	// Add common fields.
	addRelationshipFields(seriesFields,
		func(fn relFunc) interface{} {
			return func(s *seed.Series, k, v string) error {
				return indexedField(&s.Relationships, k, "rel",
					func(rel *seed.Relationship) error { return fn(rel, k, v) })
			}
		})
	addURLFields(seriesFields,
		func(fn urlFunc) interface{} {
			return func(s *seed.Series, k, v string) error {
				return indexedField(&s.URLs, k, "url",
					func(url *seed.URL) error { return fn(url, v) })
			}
		})
}
