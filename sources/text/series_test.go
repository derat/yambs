// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"bytes"
	"context"
	"encoding/csv"
	"strconv"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestRead_Series_All(t *testing.T) {
	const (
		disambig   = "this one"
		editNote   = "here’s my edit"
		mbid       = "0096a0bf-804e-4e47-bf2a-e0878dbb3eb7"
		name       = "The Series"
		orderType  = seed.SeriesOrderingType_Automatic
		seriesType = seed.SeriesType_ReleaseGroupSeries
		relTarget  = "dd141ce3-5e5a-48f2-a774-66ba427fca99"
		relType    = seed.LinkType_PartOf_ReleaseGroup_Series
		url        = "https://www.example.org/foo"
		urlType    = seed.LinkType_OfficialHomepage_Series_URL
	)

	var input bytes.Buffer
	if err := csv.NewWriter(&input).WriteAll([][]string{{
		disambig,
		editNote,
		"true",
		mbid,
		name,
		strconv.Itoa(int(orderType)),
		relTarget,
		strconv.Itoa(int(relType)),
		strconv.Itoa(int(seriesType)),
		url,
		strconv.Itoa(int(urlType)),
	}}); err != nil {
		t.Fatal("Failed writing input:", err)
	}
	got, err := Read(context.Background(), &input, CSV, seed.SeriesEntity, []string{
		"disambiguation",
		"edit_note",
		"make_votable",
		"mbid",
		"name",
		"ordering_type",
		"rel0_target",
		"rel0_type",
		"type",
		"url0_url",
		"url0_type",
	}, nil, mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}

	want := []seed.Edit{
		&seed.Series{
			Disambiguation: disambig,
			EditNote:       editNote,
			MakeVotable:    true,
			MBID:           mbid,
			Name:           name,
			OrderingType:   orderType,
			Relationships: []seed.Relationship{{
				Target: relTarget,
				Type:   relType,
			}},
			Type: seriesType,
			URLs: []seed.URL{{URL: url, LinkType: urlType}},
		},
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}
