// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package text parses entities from textual input like CSV or TSV files.
package text

import (
	"bufio"
	"bytes"
	"context"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strings"
	"unicode/utf8"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"

	"golang.org/x/text/encoding/ianaindex"
	"golang.org/x/text/transform"
)

// Format represents a textual format for supplying seed data.
type Format string

const (
	// CSV corresponds to lines of comma-separated values as described in RFC 4180.
	// See https://pkg.go.dev/encoding/csv.
	CSV Format = "csv"
	// KeyVal corresponds to an individual "field=value" pair on each line.
	// Unlike the other formats, this is used to specify a single edit.
	KeyVal Format = "keyval"
	// TSV corresponds to lines of tab-separated values. No escaping is supported.
	TSV Format = "tsv"
)

// config is passed to Options to configure Read's behavior.
type config struct {
	charset   string
	maxEdits  int
	maxFields int
}

// Read reads one or more edits of the specified type from r in the specified format.
// fields specifies the field associated with each column (unused for the KeyVal format).
// Multiple fields can be associated with a single column by separating their names with
// slashes, and empty field names indicate that the column should be ignored.
// rawSets contains "field=value" directives describing values to set for all edits.
func Read(ctx context.Context, r io.Reader, format Format, typ seed.Entity,
	fields []string, rawSetCmds []string, db *mbdb.DB, opts ...Option) ([]seed.Edit, error) {
	var cfg config
	for _, o := range opts {
		o(&cfg)
	}

	if cfg.charset != "" {
		if enc, err := ianaindex.MIME.Encoding(cfg.charset); err != nil {
			return nil, fmt.Errorf("bad charset %q: %w", cfg.charset, err)
		} else if enc == nil {
			// ianaindex annoyingly returns a nil encoding and a nil error for
			// IANA-registered-but-unsupported charsets like UTF-32.
			return nil, fmt.Errorf("unsupported charset %q", cfg.charset)
		} else {
			r = transform.NewReader(r, enc.NewDecoder())
		}
	}

	setPairs, err := ParseSetCommands(rawSetCmds, typ)
	if err != nil {
		return nil, err
	}
	rr, fields, err := newRowReader(r, format, fields)
	if err != nil {
		return nil, err
	}

	// Count fields, including slash-separated names.
	// Empty fields are included but that's arguably safer.
	var nfields int
	for _, f := range fields {
		nfields += len(strings.Split(f, "/"))
	}
	if nfields == 0 {
		return nil, errors.New("no fields specified")
	} else if cfg.maxFields > 0 && len(setPairs)+nfields > cfg.maxFields {
		return nil, errors.New("too many fields")
	}

	var edits []seed.Edit
	for {
		cols, err := rr.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}

		if cfg.maxEdits > 0 && len(edits) == cfg.maxEdits {
			return nil, errors.New("too many edits")
		}

		edit := newEdit(typ)
		if edit == nil {
			return nil, fmt.Errorf("unknown edit type %q", typ)
		}

		for _, pair := range setPairs {
			if err := SetField(edit, pair[0], pair[1], nil); err != nil {
				return nil, fmt.Errorf("failed setting %q: %w", pair[0]+"="+pair[1], err)
			}
		}
		for j, field := range fields {
			// Skip setting anything if the field name is empty.
			// This is handy if the input file contains additional columns that
			// the user doesn't want to use.
			if field == "" {
				continue
			}
			for _, fd := range strings.Split(field, "/") {
				val := cols[j]
				err := SetField(edit, fd, val, nil)
				if _, ok := err.(*fieldNameError); ok {
					return nil, fmt.Errorf("%q: %w", fd, err)
				} else if err != nil {
					return nil, fmt.Errorf("bad %v %q: %w", fd, val, err)
				}
			}
		}

		if err := edit.Finish(ctx, db); err != nil {
			return nil, err
		}

		edits = append(edits, edit)
	}
	if len(edits) == 0 {
		return nil, errors.New("empty input")
	}
	return edits, nil
}

// Option can be passed to Read to configure its behavior.
type Option func(*config)

// Charset contains the IANA or MIME name of the charset to use when reading the input
// if not UTF-8. See https://pkg.go.dev/golang.org/x/text/encoding/ianaindex.
func Charset(charset string) Option { return func(c *config) { c.charset = charset } }

// MaxEdits returns an Option that limits the maximum number of edits to be read.
// If more edits are supplied, an error will be returned.
func MaxEdits(max int) Option { return func(c *config) { c.maxEdits = max } }

// MaxEdits returns an Option that limits the maximum number of fields that can be set.
// "field=value" directives are included in the count.
func MaxFields(max int) Option { return func(c *config) { c.maxFields = max } }

// rowReader is used by Read to read entity data row-by-row.
type rowReader interface {
	Read() ([]string, error)
}

// tsvReader is a rowReader implementation for TSV input.
type tsvReader struct {
	sc      *bufio.Scanner
	nfields int
}

func (tr *tsvReader) Read() ([]string, error) {
	if !tr.sc.Scan() {
		return nil, io.EOF
	}
	cols := strings.Split(tr.sc.Text(), "\t")
	if len(cols) != tr.nfields {
		return nil, fmt.Errorf("line %q has %v field(s); want %v",
			tr.sc.Text(), len(cols), tr.nfields)
	}
	return cols, nil
}

// singleRowReader is a rowReader implementation that just returns a single row (or error)
// and then returns io.EOF. It is used to return KeyVal data (which is read by newRowReader).
type singleRowReader struct {
	row []string
	err error
}

func (sr *singleRowReader) Read() ([]string, error) {
	defer func() {
		sr.row = nil
		sr.err = io.EOF
	}()
	return sr.row, sr.err
}

const bom = "\ufeff"

// newRowReader returns a rowReader for reading the named fields from r in format.
// fieldsOut should be used afterward (fields are specified via r for the KeyVal format).
func newRowReader(r io.Reader, format Format, fields []string) (rr rowReader, fieldsOut []string, err error) {
	// Discard leading Unicode BOM if present:
	// https://codeberg.org/derat/yambs/issues/39
	// https://codeberg.org/derat/yambs/issues/41
	br := bufio.NewReader(r)
	if b, err := br.Peek(len(bom)); err == nil && bytes.Equal(b, []byte(bom)) {
		br.Discard(len(bom))
	}
	r = br

	switch format {
	case CSV:
		cr := csv.NewReader(r)
		cr.FieldsPerRecord = len(fields)
		return cr, fields, nil
	case KeyVal:
		// Transform the input into a single row and use it to synthesize the field list.
		// Note that fields isn't used in this case, since the field names are provided
		// in the input.
		var sr singleRowReader
		sc := bufio.NewScanner(r)
		for num := 0; sc.Scan(); num++ {
			// Guard against bad input (the MB server would just reject this later, anyway).
			if !utf8.Valid(sc.Bytes()) {
				sr.err = fmt.Errorf(`line %d (%q) contains non-UTF-8 data`, num, sc.Bytes())
			}
			ln := sc.Text()
			if strings.TrimSpace(ln) == "" {
				continue
			}
			parts := strings.SplitN(ln, "=", 2)
			if len(parts) != 2 {
				sr.err = fmt.Errorf(`line %d (%q) not "field=value" format`, num, sc.Text())
				break
			}
			fieldsOut = append(fieldsOut, strings.TrimSpace(parts[0]))
			sr.row = append(sr.row, unescapeValue(parts[1]))
		}
		return &sr, fieldsOut, sc.Err()
	case TSV:
		return &tsvReader{bufio.NewScanner(r), len(fields)}, fields, nil
	default:
		return nil, nil, fmt.Errorf("unknown format %q", format)
	}
}

// unescapeValue replaces literal `\n` sequences with newlines, `\t` with tabs, and all
// other backslash-character sequences with just the character. This is intended to mirror
// the unescaping code in generateEdits() in render/page.tmpl.
func unescapeValue(s string) string {
	return backslashRegexp.ReplaceAllStringFunc(s, func(m string) string {
		switch m[1] {
		case 'n':
			return "\n"
		case 't':
			return "\t"
		}
		return string(m[1])
	})
}

// backslashRegexp matches a backslash followed by a single character.
var backslashRegexp = regexp.MustCompile(`\\.`)

// newEdit returns a new seed.Edit for the specified entity type.
// nil is returned if the type is unsupported.
func newEdit(typ seed.Entity) seed.Edit {
	switch typ {
	case seed.ArtistEntity:
		return &seed.Artist{}
	case seed.EventEntity:
		return &seed.Event{}
	case seed.LabelEntity:
		return &seed.Label{}
	case seed.PlaceEntity:
		return &seed.Place{}
	case seed.RecordingEntity:
		return &seed.Recording{}
	case seed.ReleaseEntity:
		return &seed.Release{}
	case seed.ReleaseGroupEntity:
		return &seed.ReleaseGroup{}
	case seed.SeriesEntity:
		return &seed.Series{}
	case seed.WorkEntity:
		return &seed.Work{}
	default:
		return nil
	}
}
