// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"codeberg.org/derat/yambs/seed"
)

// placeFields defines fields that can be set in a seed.Place.
var placeFields = map[string]fieldInfo{
	"address": {
		"Address of place (using standard format for its country)",
		func(p *seed.Place, k, v string) error { return setString(&p.Address, v) },
	},
	"area_name": {
		"Area name to prefill search",
		func(p *seed.Place, k, v string) error { return setString(&p.AreaName, v) },
	},
	"begin_date": {
		`Date when place was founded as “YYYY-MM-DD”, “YYYY-MM”, or “YYYY”`,
		func(p *seed.Place, k, v string) error { return setDate(&p.BeginDate, v) },
	},
	"coordinates": {
		"Latitude and longitude of the place’s location as comma-separated values",
		func(p *seed.Place, k, v string) error { return setString(&p.Coordinates, v) },
	},
	"disambiguation": {
		"Comment disambiguating this place from others with similar names",
		func(p *seed.Place, k, v string) error { return setString(&p.Disambiguation, v) },
	},
	"edit_note": {
		"Note attached to edit",
		func(p *seed.Place, k, v string) error { return setString(&p.EditNote, v) },
	},
	"end_date": {
		`Date when place was closed as “YYYY-MM-DD”, “YYYY-MM”, or “YYYY”`,
		func(p *seed.Place, k, v string) error { return setDate(&p.EndDate, v) },
	},
	"ended": {
		`Whether the place has closed (“1” or “true” if true)`,
		func(p *seed.Place, k, v string) error { return setBool(&p.Ended, v) },
	},
	"make_votable": {
		`Make edits votable even if they would be auto-edits (“1” or “true” for true)`,
		func(p *seed.Place, k, v string) error { return setBool(&p.MakeVotable, v) },
	},
	"mbid": {
		"MBID of existing place to edit (if empty, create place)",
		func(p *seed.Place, k, v string) error { return setMBID(&p.MBID, v) },
	},
	"name": {
		"Place’s name",
		func(p *seed.Place, k, v string) error { return setString(&p.Name, v) },
	},
	"type": {
		"Integer [place type](" + placeTypeURL + ")",
		func(p *seed.Place, k, v string) error { return setInt((*int)(&p.Type), v) },
	},
}

func init() {
	// Add common fields.
	addRelationshipFields(placeFields,
		func(fn relFunc) interface{} {
			return func(p *seed.Place, k, v string) error {
				return indexedField(&p.Relationships, k, "rel",
					func(rel *seed.Relationship) error { return fn(rel, k, v) })
			}
		})
	addURLFields(placeFields,
		func(fn urlFunc) interface{} {
			return func(p *seed.Place, k, v string) error {
				return indexedField(&p.URLs, k, "url",
					func(url *seed.URL) error { return fn(url, v) })
			}
		})
}
