// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"codeberg.org/derat/yambs/seed"
)

// releaseGroupFields defines fields that can be set in a seed.ReleaseGroup.
var releaseGroupFields = map[string]fieldInfo{
	"disambiguation": {
		"Comment disambiguating this release group from others with similar names",
		func(rg *seed.ReleaseGroup, k, v string) error { return setString(&rg.Disambiguation, v) },
	},
	"edit_note": {
		"Note attached to edit",
		func(rg *seed.ReleaseGroup, k, v string) error { return setString(&rg.EditNote, v) },
	},
	"make_votable": {
		`Make edits votable even if they would be auto-edits (“1” or “true” for true)`,
		func(rg *seed.ReleaseGroup, k, v string) error { return setBool(&rg.MakeVotable, v) },
	},
	"mbid": {
		"MBID of existing release group to edit (if empty, create release group)",
		func(rg *seed.ReleaseGroup, k, v string) error { return setMBID(&rg.MBID, v) },
	},
	"name": {
		"Release group’s name",
		func(rg *seed.ReleaseGroup, k, v string) error { return setString(&rg.Name, v) },
	},
	"primary_type": {
		"Integer primary [release group type ID](" + rgTypeIDURL + ")",
		func(rg *seed.ReleaseGroup, k, v string) error { return setInt((*int)(&rg.PrimaryType), v) },
	},
	"secondary_types": {
		"Comma-separated integer secondary [release group type IDs](" + rgTypeIDURL + ")",
		func(rg *seed.ReleaseGroup, k, v string) error {
			var vals []int
			if err := setIntSlice(&vals, v, ","); err != nil {
				return err
			}
			for _, v := range vals {
				rg.SecondaryTypes = append(rg.SecondaryTypes, seed.ReleaseGroupTypeID(v))
			}
			return nil
		},
	},
	"title": {"name", nil}, // synonym
}

func init() {
	// Add common fields.
	addArtistCreditFields(releaseGroupFields, "",
		func(fn artistFunc) interface{} {
			return func(rg *seed.ReleaseGroup, k, v string) error {
				return indexedField(&rg.Artists, k, "artist",
					func(ac *seed.ArtistCredit) error { return fn(ac, v) })
			}
		})
	addRelationshipFields(releaseGroupFields,
		func(fn relFunc) interface{} {
			return func(rg *seed.ReleaseGroup, k, v string) error {
				return indexedField(&rg.Relationships, k, "rel",
					func(rel *seed.Relationship) error { return fn(rel, k, v) })
			}
		})
	addURLFields(releaseGroupFields,
		func(fn urlFunc) interface{} {
			return func(rg *seed.ReleaseGroup, k, v string) error {
				return indexedField(&rg.URLs, k, "url",
					func(url *seed.URL) error { return fn(url, v) })
			}
		})
}
