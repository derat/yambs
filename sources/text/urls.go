// Copyright 2022 Daniel Erat.
// All rights reserved.

package text

const (
	godocURL              = "https://pkg.go.dev/codeberg.org/derat/yambs/seed@main"
	artistTypeURL         = godocURL + "#ArtistType"
	eventTypeURL          = godocURL + "#EventType"
	formatURL             = godocURL + "#MediumFormat"
	genderURL             = godocURL + "#Gender"
	labelTypeURL          = godocURL + "#LabelType"
	langIDURL             = godocURL + "#Language"
	linkAttrTypeURL       = godocURL + "#LinkAttributeType"
	linkTypeURL           = godocURL + "#LinkType"
	packagingURL          = godocURL + "#ReleasePackaging"
	placeTypeURL          = godocURL + "#PlaceType"
	rgTypeURL             = godocURL + "#ReleaseGroupType"
	rgTypeIDURL           = godocURL + "#ReleaseGroupTypeID"
	seriesTypeURL         = godocURL + "#SeriesType"
	seriesOrderingTypeURL = godocURL + "#SeriesOrderingType"
	statusURL             = godocURL + "#ReleaseStatus"
	workAttrTypeURL       = godocURL + "#WorkAttributeType"
	workTypeURL           = godocURL + "#WorkType"

	countryURL = "https://wikipedia.org/wiki/ISO_3166-1_alpha-2"
	langURL    = "https://wikipedia.org/wiki/List_of_ISO_639-3_codes"
	scriptURL  = "https://wikipedia.org/wiki/ISO_15924"
)
