// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"strconv"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestRead_Place_All(t *testing.T) {
	const (
		address    = "123 Main St"
		areaName   = "Anytown"
		beginYear  = 2014
		beginMonth = 4
		beginDay   = 5
		coords     = "123.4, -45.3"
		disambig   = "this one"
		editNote   = "here’s my edit"
		endYear    = 2014
		endMonth   = 4
		endDay     = 6
		ended      = 1
		mbid       = "0096a0bf-804e-4e47-bf2a-e0878dbb3eb7"
		name       = "The Venue"
		placeType  = seed.PlaceType_Venue
		relTarget  = "dd141ce3-5e5a-48f2-a774-66ba427fca99"
		relType    = seed.LinkType_Owner_Artist_Place
		url        = "https://www.example.org/foo"
		urlType    = seed.LinkType_OfficialHomepage_Place_URL
	)

	var input bytes.Buffer
	if err := csv.NewWriter(&input).WriteAll([][]string{{
		address,
		areaName,
		fmt.Sprintf("%04d-%02d-%02d", beginYear, beginMonth, beginDay),
		coords,
		disambig,
		editNote,
		fmt.Sprintf("%04d-%02d-%02d", endYear, endMonth, endDay),
		"1",
		"true",
		mbid,
		name,
		relTarget,
		strconv.Itoa(int(relType)),
		strconv.Itoa(int(placeType)),
		url,
		strconv.Itoa(int(urlType)),
	}}); err != nil {
		t.Fatal("Failed writing input:", err)
	}
	got, err := Read(context.Background(), &input, CSV, seed.PlaceEntity, []string{
		"address",
		"area_name",
		"begin_date",
		"coordinates",
		"disambiguation",
		"edit_note",
		"end_date",
		"ended",
		"make_votable",
		"mbid",
		"name",
		"rel0_target",
		"rel0_type",
		"type",
		"url0_url",
		"url0_type",
	}, nil, mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}

	want := []seed.Edit{
		&seed.Place{
			Address:        address,
			AreaName:       areaName,
			BeginDate:      seed.MakeDate(beginYear, beginMonth, beginDay),
			Coordinates:    coords,
			Disambiguation: disambig,
			EditNote:       editNote,
			EndDate:        seed.MakeDate(endYear, endMonth, endDay),
			Ended:          true,
			MakeVotable:    true,
			MBID:           mbid,
			Name:           name,
			Relationships: []seed.Relationship{{
				Target: relTarget,
				Type:   relType,
			}},
			Type: placeType,
			URLs: []seed.URL{{URL: url, LinkType: urlType}},
		},
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}
