// Copyright 2024 Daniel Erat.
// All rights reserved.

package text

import (
	"bytes"
	"context"
	"encoding/csv"
	"fmt"
	"strconv"
	"strings"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestRead_ReleaseGroup_All(t *testing.T) {
	const (
		disambig       = "this one"
		editNote       = "here’s my edit"
		mbid           = "0096a0bf-804e-4e47-bf2a-e0878dbb3eb7"
		name           = "Album Title"
		primaryType    = seed.ReleaseGroupTypeID_Album
		relTarget      = "65389277-491a-4055-8e71-0a9be1c9c99c"
		relType        = seed.LinkType_Cover_ReleaseGroup_ReleaseGroup
		secondaryType1 = seed.ReleaseGroupTypeID_Soundtrack
		secondaryType2 = seed.ReleaseGroupTypeID_Live
		url            = "https://example.org/"
		urlType        = seed.LinkType_Discography_ReleaseGroup_URL
	)

	var input bytes.Buffer
	if err := csv.NewWriter(&input).WriteAll([][]string{{
		disambig,
		editNote,
		"true",
		mbid,
		name,
		strconv.Itoa(int(primaryType)),
		fmt.Sprintf("%d,%d", secondaryType1, secondaryType2),
		relTarget,
		strconv.Itoa(int(relType)),
		url,
		strconv.Itoa(int(urlType)),
	}}); err != nil {
		t.Fatal("Failed writing input:", err)
	}
	got, err := Read(context.Background(), &input, CSV, seed.ReleaseGroupEntity, []string{
		"disambiguation",
		"edit_note",
		"make_votable",
		"mbid",
		"name",
		"primary_type",
		"secondary_types",
		"rel0_target",
		"rel0_type",
		"url0_url",
		"url0_type",
	}, nil, mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}

	want := []seed.Edit{
		&seed.ReleaseGroup{
			Disambiguation: disambig,
			EditNote:       editNote,
			MakeVotable:    true,
			MBID:           mbid,
			Name:           name,
			PrimaryType:    primaryType,
			Relationships: []seed.Relationship{{
				Target: relTarget,
				Type:   relType,
			}},
			SecondaryTypes: []seed.ReleaseGroupTypeID{secondaryType1, secondaryType2},
			URLs:           []seed.URL{{URL: url, LinkType: urlType}},
		},
	}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}

func TestRead_ReleaseGroup_Synonyms(t *testing.T) {
	got, err := Read(context.Background(),
		strings.NewReader("title=Some Release Group\n"),
		KeyVal, seed.ReleaseGroupEntity, nil, nil,
		mbdb.NewDB(mbdb.DisallowQueries))
	if err != nil {
		t.Fatal("Read failed:", err)
	}
	want := []seed.Edit{&seed.ReleaseGroup{Name: "Some Release Group"}}
	if diff := cmp.Diff(want, got); diff != "" {
		t.Error("Read returned wrong edits:\n" + diff)
	}
}
