// Copyright 2022 Daniel Erat.
// All rights reserved.

package bandcamp

import (
	"context"
	"reflect"
	"strings"
	"testing"
	"time"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/sources/online/internal"
	"github.com/google/go-cmp/cmp"
)

func TestMakeEdits(t *testing.T) {
	ctx := context.Background()
	now := time.Date(2015, 2, 10, 0, 0, 0, 0, time.UTC)
	db := mbdb.NewDB(mbdb.DisallowQueries, mbdb.NowFunc(func() time.Time { return now }))
	var pr Provider

	// Add canned MBIDs for artists and label lookups.
	mkInfos := mbdb.MakeEntityInfosForTest
	for url, artists := range map[string][]mbdb.EntityInfo{
		"https://aartijadu.bandcamp.com/":       mkInfos("76cb3647-7a02-4f1d-9eef-8a6e99ce022d", "Aarti Jadu"),
		"https://anti-mass.bandcamp.com/":       mkInfos("7fa907c9-35a0-40dd-b926-8c962782ba1d", "ANTI-MASS"),
		"https://louiezong.bandcamp.com/":       mkInfos("0e2c603f-fd71-4ab6-af96-92c3e936586d", "louie zong"),
		"https://louiscole.bandcamp.com/":       mkInfos("525ef747-abf6-423c-98b4-cd49c0c07927", "Louis Cole"),
		"https://pillarsinthesky.bandcamp.com/": mkInfos("7ba8b326-34ba-472b-b710-b01dc1f14f94", "Pillars in the Sky"),
		"https://tauk.bandcamp.com/":            mkInfos("6f2adbd6-7685-47a9-932e-b1e450a561a3", "TAUK"),
		"https://thelovelymoon.bandcamp.com/":   mkInfos("f34ae170-055d-46bc-9208-a750a646481b", "The Lovely Moon"),
		// "https://nervoustestpilot.co.uk/track/open-system" omitted
		// "https://volaband.bandcamp.com/album/live-from-the-pool" omitted
	} {
		db.SetURLEntityInfosForTest(url, artists, nil)
	}
	for url, labels := range map[string][]mbdb.EntityInfo{
		"https://brainfeeder.bandcamp.com/": mkInfos("20b3d6f9-9086-48d9-802f-5f808456a0ef", "Brainfeeder"),
		// "https://mascotlabelgroup.bandcamp.com/" omitted
		"https://syssistersounds.bandcamp.com/": mkInfos("2583b10d-0528-4281-8d2f-31d9a64a570c", "SYS Sister Sounds"),
	} {
		db.SetURLEntityInfosForTest(url, nil, labels)
	}

	for _, tc := range []struct {
		url                 string
		correctPunctuation  bool
		extractTrackArtists bool
		removeParens        bool
		splitArtists        bool
		rel                 *seed.Release
		img                 string
	}{
		{
			// This album was released on a subdomain that's linked to an artist, but the artist's
			// MBID shouldn't be seeded since the credited name is very different from the name in
			// the DB: https://codeberg.org/derat/yambs/issues/26
			url:          "https://aartijadu.bandcamp.com/track/just-eyes",
			splitArtists: true,
			rel: &seed.Release{
				Title:     "Just Eyes",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2019, 4, 6), Country: "XW"}},
				Artists:   artists("Aarti Jadu", " & ", "Matthew Hayes"),
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Just Eyes", Length: sec(401.01)},
					},
				}},
				URLs: urlLinks("https://aartijadu.bandcamp.com/track/just-eyes",
					seed.LinkType_DownloadForFree_Release_URL,
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://aartijadu.bandcamp.com/track/just-eyes"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a3258735142_0.jpg",
					"https://aartijadu.bandcamp.com/track/just-eyes"),
			},
			img: "https://f4.bcbits.com/img/a3258735142_0.jpg",
		},
		{
			// This is a compilation album with artist name(s) at the beginning of each track.
			// Check that they get extracted when ExtractTrackArtists is true.
			url:                 "https://anti-mass.bandcamp.com/album/doxa",
			extractTrackArtists: true,
			splitArtists:        true,
			rel: &seed.Release{
				Title:     "DOXA",
				Types:     nil,
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2021, 10, 7), Country: "XW"}},
				Artists: []seed.ArtistCredit{{
					MBID: "7fa907c9-35a0-40dd-b926-8c962782ba1d",
					Name: "ANTI-MASS",
				}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Galiba", Artists: artists("Authentically Plastic", " & ", "Nsasi"), Length: sec(230)},
						{Title: "Grind", Artists: artists("Nsasi"), Length: sec(156)},
						{Title: "Diesel Femme", Artists: artists("Turkana", " & ", "Authentically Plastic"), Length: sec(260)},
						{Title: "Influencer Convention", Artists: artists("Turkana"), Length: sec(240)},
						{Title: "Binia Yei", Artists: artists("Nsasi", " & ", "Turkana"), Length: sec(232)},
						{Title: "Sabula", Artists: artists("Authentically Plastic"), Length: sec(234)},
					},
				}},
				URLs: urlLinks("https://anti-mass.bandcamp.com/album/doxa",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://anti-mass.bandcamp.com/album/doxa"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a2017435297_0.jpg",
					"https://anti-mass.bandcamp.com/album/doxa"),
			},
			img: "https://f4.bcbits.com/img/a2017435297_0.jpg",
		},
		{
			// This album has a hidden track, which should be included in the tracklist.
			// It also shouldn't have a stream-for-free link since the hidden track can't be streamed.
			url:                "https://louiezong.bandcamp.com/album/cartoon-funk",
			correctPunctuation: true,
			rel: &seed.Release{
				Title:     "Cartoon Funk",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Album},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2022, 6, 17), Country: "XW"}},
				Artists: []seed.ArtistCredit{{
					MBID: "0e2c603f-fd71-4ab6-af96-92c3e936586d",
					Name: "Louie Zong",
				}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "A Worm Welcome!", Length: sec(126.224)},
						{Title: "Anxiety Groove", Length: sec(137.928)},
						{Title: "Coelacanth", Length: sec(173.402)},
						{Title: "Brainfoghorn", Length: sec(120.078)},
						{Title: "Sunset Skating", Length: sec(104.019)},
						{Title: "Evolution of the Eye", Length: sec(132.928)},
						{Title: "Jar of Pickles", Length: sec(111.16)},
						{Title: "I Outsolve You", Length: sec(108.172)},
						{Title: "Rough Edges", Length: sec(126.221)},
						{Title: "Signal/Noise (feat. Turner Perez)", Length: sec(137.027)},
						{Title: "Furniture Hellscape", Length: sec(106.083)},
						{Title: "Rooftop Cats", Length: sec(98.3392)},
						{Title: "Spring Cleaning", Length: sec(139.693)},
						{Title: "My Father’s Sacred Sword", Length: sec(120.047)},
						{Title: "Virtual Home", Length: sec(93.4912)},
						{Title: "Cartoon Mines", Length: sec(208.526)},
						{Title: "Only A Toad", Length: sec(119.907)},
						{Title: "Worm Cowboy (See Ya!)", Length: sec(63.6559)},
						{Title: "[unknown]"}, // hidden track
					},
				}},
				URLs: urlLinks("https://louiezong.bandcamp.com/album/cartoon-funk",
					seed.LinkType_DownloadForFree_Release_URL,
					seed.LinkType_PurchaseForDownload_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://louiezong.bandcamp.com/album/cartoon-funk"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a1689585732_0.jpg",
					"https://louiezong.bandcamp.com/album/cartoon-funk"),
			},
			img: "https://f4.bcbits.com/img/a1689585732_0.jpg",
		},
		{
			// This is an album URL, but it has a single track that matches the album title
			// so it should be treated as a single rather than an album.
			url: "https://louiscole.bandcamp.com/album/let-it-happen",
			rel: &seed.Release{
				Title:     "Let it Happen",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Barcode:   "5054429157154",
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2022, 8, 2), Country: "XW"}},
				Labels:    []seed.ReleaseLabel{{Name: "Brainfeeder"}},
				Artists: []seed.ArtistCredit{{
					MBID: "525ef747-abf6-423c-98b4-cd49c0c07927",
					Name: "Louis cole",
				}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Let it Happen", Length: sec(403)},
					},
				}},
				URLs: urlLinks("https://louiscole.bandcamp.com/album/let-it-happen",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://louiscole.bandcamp.com/album/let-it-happen"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a3000320182_0.jpg",
					"https://louiscole.bandcamp.com/album/let-it-happen"),
			},
			img: "https://f4.bcbits.com/img/a3000320182_0.jpg",
		},
		{
			// This single is hosted on a custom domain rather than on a bandcamp.com subdomain.
			url: "https://nervoustestpilot.co.uk/track/open-system",
			rel: &seed.Release{
				Title:     "Open System",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2020, 2, 6), Country: "XW"}},
				Artists:   []seed.ArtistCredit{{Name: "nervous_testpilot"}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Open System", Length: sec(229.0)},
					},
				}},
				URLs: urlLinks("https://nervoustestpilot.co.uk/track/open-system",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://nervoustestpilot.co.uk/track/open-system"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a1704520813_0.jpg",
					"https://nervoustestpilot.co.uk/track/open-system"),
			},
			img: "https://f4.bcbits.com/img/a1704520813_0.jpg",
		},
		{
			// This is a non-album track URL, which should be treated as a single.
			url: "https://pillarsinthesky.bandcamp.com/track/arcanum",
			rel: &seed.Release{
				Title:     "Arcanum",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2015, 5, 3), Country: "XW"}},
				Artists: []seed.ArtistCredit{{
					MBID: "7ba8b326-34ba-472b-b710-b01dc1f14f94",
					Name: "Pillars In The Sky",
				}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Arcanum", Length: sec(341.294)},
					},
				}},
				URLs: urlLinks("https://pillarsinthesky.bandcamp.com/track/arcanum",
					seed.LinkType_DownloadForFree_Release_URL,
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://pillarsinthesky.bandcamp.com/track/arcanum"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a2320496643_0.jpg",
					"https://pillarsinthesky.bandcamp.com/track/arcanum"),
			},
			img: "https://f4.bcbits.com/img/a2320496643_0.jpg",
		},
		{
			// This is a non-album track with the artist's name prefixed to its title.
			// The artist name should be stripped even without setting ExtractTrackArtists.
			// The label MBID should also be derived from the hostname.
			url: "https://syssistersounds.bandcamp.com/track/apsara",
			rel: &seed.Release{
				Title:     "Apsara",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2022, 5, 18), Country: "XW"}},
				Labels:    []seed.ReleaseLabel{{MBID: "2583b10d-0528-4281-8d2f-31d9a64a570c"}},
				Artists:   []seed.ArtistCredit{{Name: "Maggie Tra"}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Apsara", Length: sec(212.574)},
					},
				}},
				URLs: urlLinks("https://syssistersounds.bandcamp.com/track/apsara",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://syssistersounds.bandcamp.com/track/apsara"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a0574189382_0.jpg",
					"https://syssistersounds.bandcamp.com/track/apsara"),
			},
			img: "https://f4.bcbits.com/img/a0574189382_0.jpg",
		},
		{
			// This album has a null 'release_date' property in its JSON data.
			// We should skip trying to parse that and use the 'publish_date' instead.
			url: "https://tauk.bandcamp.com/track/daydreams-ft-kanika-moore",
			rel: &seed.Release{
				Title:     "Daydreams ft. Kanika Moore",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Single},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2023, 3, 20), Country: "XW"}},
				Artists:   []seed.ArtistCredit{{Name: "Tauk Ft. Kanika Moore"}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Daydreams ft. Kanika Moore", Length: sec(250.768)},
					},
				}},
				URLs: urlLinks("https://tauk.bandcamp.com/track/daydreams-ft-kanika-moore",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://tauk.bandcamp.com/track/daydreams-ft-kanika-moore"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a1325172425_0.jpg",
					"https://tauk.bandcamp.com/track/daydreams-ft-kanika-moore"),
			},
			img: "https://f4.bcbits.com/img/a1325172425_0.jpg",
		},
		{
			// This album has a Creative Commons license.
			url: "https://thelovelymoon.bandcamp.com/album/echoes-of-memories",
			rel: &seed.Release{
				Title:     "Echoes of Memories",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Album},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2022, 10, 22), Country: "XW"}},
				Artists: []seed.ArtistCredit{{
					MBID: "f34ae170-055d-46bc-9208-a750a646481b",
					Name: "The Lovely Moon",
				}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "Echoes of Memories", Length: sec(2295.17)},
						{Title: "Cinnabar Sunset", Length: sec(1521.34)},
					},
				}},
				URLs: append(urlLinks("https://thelovelymoon.bandcamp.com/album/echoes-of-memories",
					seed.LinkType_DownloadForFree_Release_URL,
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				), seed.URL{
					URL:      "http://creativecommons.org/licenses/by-nc-sa/3.0/",
					LinkType: seed.LinkType_License_Release_URL,
				}),
				EditNote: internal.MakeEditNote("https://thelovelymoon.bandcamp.com/album/echoes-of-memories"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a2393960629_0.jpg",
					"https://thelovelymoon.bandcamp.com/album/echoes-of-memories"),
			},
			img: "https://f4.bcbits.com/img/a2393960629_0.jpg",
		},
		{
			// This album is released by a label and isn't downloadable for free.
			// The extra query parameter should be removed before fetching the page.
			url:          "https://volaband.bandcamp.com/album/live-from-the-pool?from=fanpub_fnb_trk",
			removeParens: true,
			rel: &seed.Release{
				Title:     "Live From The Pool",
				Types:     []seed.ReleaseGroupType{seed.ReleaseGroupType_Album},
				Script:    "Latn",
				Status:    seed.ReleaseStatus_Official,
				Packaging: seed.ReleasePackaging_None,
				Events:    []seed.ReleaseEvent{{Date: seed.MakeDate(2022, 4, 1), Country: "XW"}},
				Labels:    []seed.ReleaseLabel{{Name: "Mascot Label Group"}},
				Artists:   []seed.ArtistCredit{{Name: "VOLA"}},
				Mediums: []seed.Medium{{
					Format: seed.MediumFormat_DigitalMedia,
					Tracks: []seed.Track{
						{Title: "24 Light-Years", Length: sec(322.773)},
						{Title: "Alien Shivers", Length: sec(257.867)},
						{Title: "Head Mounted Sideways", Length: sec(345.48)},
						{Title: "Straight Lines", Length: sec(263.307)},
						{Title: "Ruby Pool", Length: sec(272.947)},
						{Title: "Owls", Length: sec(357.72)},
						{Title: "These Black Claws feat. SHAHMEN", Length: sec(355.507)},
						{Title: "Gutter Moon (October Session)", Length: sec(183.533)},
						{Title: "Ghosts", Length: sec(245.493)},
						{Title: "Smartfriend", Length: sec(256.6)},
						{Title: "Whaler", Length: sec(344.68)},
						{Title: "Inside Your Fur", Length: sec(338.24)},
						{Title: "Stray The Skies", Length: sec(257.853)},
					},
				}},
				URLs: urlLinks("https://volaband.bandcamp.com/album/live-from-the-pool",
					seed.LinkType_PurchaseForDownload_Release_URL,
					seed.LinkType_FreeStreaming_Release_URL,
				),
				EditNote: internal.MakeEditNote("https://volaband.bandcamp.com/album/live-from-the-pool"),
				RedirectURI: imgRedirURL("https://f4.bcbits.com/img/a1079469134_0.jpg",
					"https://volaband.bandcamp.com/album/live-from-the-pool"),
			},
			img: "https://f4.bcbits.com/img/a1079469134_0.jpg",
		},
	} {
		t.Run(tc.url, func(t *testing.T) {
			cfg := internal.Config{
				CorrectPunctuation:  tc.correctPunctuation,
				ExtractTrackArtists: tc.extractTrackArtists,
				RemoveParens:        tc.removeParens,
				SplitArtists:        tc.splitArtists,
				DisallowNetwork:     true,
			}
			edits, err := pr.MakeEdits(ctx, tc.url, db, &cfg)
			if err != nil {
				t.Fatal("Failed parsing page:", err)
			} else if len(edits) < 1 || len(edits) > 2 {
				t.Fatalf("Got %d edits", len(edits))
			}

			if diff := cmp.Diff(tc.rel, edits[0]); diff != "" {
				t.Error("Bad release data:\n" + diff)
			}

			var imgURL string
			if len(edits) > 1 {
				imgURL = edits[1].URL("" /* serverURL */)
			}
			if diff := cmp.Diff(tc.img, imgURL); diff != "" {
				t.Error("Bad cover image URL:\n" + diff)
			}
		})
	}
}

func sec(sec float64) time.Duration { return time.Duration(sec * float64(time.Second)) }

// imgRedirURL returns the URL for redirecting to the add-cover-art page
// with an edit note for the specified image and release URLs.
func imgRedirURL(imgURL, relURL string) string {
	return seed.MakeAddCoverArtRedirectURI(internal.MakeImageEditNote(imgURL, relURL))
}

// artists constructs artist credits from a sequence of artist names and join phrases.
func artists(in ...string) []seed.ArtistCredit {
	credits := make([]seed.ArtistCredit, (len(in)+1)/2)
	for i := 0; i < len(in); i += 2 {
		credits[i/2].Name = in[i]
		if i+1 < len(in) {
			credits[i/2].JoinPhrase = in[i+1]
		}
	}
	return credits
}

// urlLinks constructs seed.URL objects for the specified URL and link types.
func urlLinks(url string, types ...seed.LinkType) []seed.URL {
	urls := make([]seed.URL, len(types))
	for i, t := range types {
		urls[i] = seed.URL{URL: url, LinkType: t}
	}
	return urls
}

func TestExtractTrackArtists(t *testing.T) {
	for _, tc := range []struct {
		orig    string
		split   bool
		track   string
		artists []seed.ArtistCredit
	}{
		{"The Title", false, "The Title", nil},
		{"Artist Name - The Title", false, "The Title", artists("Artist Name")},
		{"Artist Name & Someone Else - The Title", true, "The Title", artists("Artist Name", " & ", "Someone Else")},
		{"Artist Name & Someone Else - The Title", false, "The Title", artists("Artist Name & Someone Else")},
		{"Artist Name - The Title - More Junk", false, "The Title - More Junk", artists("Artist Name")},
		{`Lieven Martens & Luc Dockx "The Bird Sings, Variations I"`, false,
			"The Bird Sings, Variations I", artists("Lieven Martens & Luc Dockx")},
		{`Lieven Martens & Luc Dockx "The Bird Sings, Variations I"`, true,
			"The Bird Sings, Variations I", artists("Lieven Martens", " & ", "Luc Dockx")},
		{`Artist "Quoted - Title"`, false, `Quoted - Title`, artists("Artist")},
		{" - The Title", false, " - The Title", nil},
		{"The Title - ", false, "The Title - ", nil},
		{`Artist "Too "Many" Quotes"`, false, `Artist "Too "Many" Quotes"`, nil},
		{`"Quotes" at Beginning`, false, `"Quotes" at Beginning`, nil},
		{`Quotes "in" Middle`, false, `Quotes "in" Middle`, nil},
	} {
		track, artists := extractTrackArtists(tc.orig, &internal.Config{SplitArtists: tc.split})
		if track != tc.track || !reflect.DeepEqual(artists, tc.artists) {
			t.Errorf("extractTrackArtists(%q, %v) = %q, %q; want %q, %q",
				tc.orig, tc.split, track, artists, tc.track, tc.artists)
		}
	}
}

func FuzzExtractTrackArtists(f *testing.F) {
	f.Add("Just the Title", true)
	f.Add("Artist - Title", false)
	f.Add("Artist & Someone - Long Title", true)
	f.Add("Artist feat. Someone - Even Longer Title", true)
	f.Fuzz(func(t *testing.T, in string, split bool) {
		track, artists := extractTrackArtists(in, &internal.Config{SplitArtists: split})
		if track == "" && in != "" {
			t.Fatal("Got empty track title")
		}
		if track != in && len(artists) == 0 {
			t.Errorf("Got modified title %q but no artists", track)
		}
		if !strings.Contains(in, track) {
			t.Errorf("Input %q doesn't contain title %q", in, track)
		}
	})
}

func TestParseArtists(t *testing.T) {
	for _, tc := range []struct {
		orig string
		want []seed.ArtistCredit
	}{
		{"Artist 1", artists("Artist 1")},
		{"Artist 1 & Artist 2", artists("Artist 1", " & ", "Artist 2")},
		{"Artist 1, Artist 2 & Artist 3", artists("Artist 1", ", ", "Artist 2", " & ", "Artist 3")},
		{"Artist 1 feat. Artist 2", artists("Artist 1", " feat. ", "Artist 2")},
		{"Artist 1 & Artist 2 FEAT Artist 3", artists("Artist 1", " & ", "Artist 2", " FEAT ", "Artist 3")},
		{"A con B, C y D", artists("A", " con ", "B", ", ", "C", " y ", "D")},
		{"A, B, and C", artists("A", ", ", "B", ", and ", "C")},
		{"A / B & C", artists("A", " / ", "B", " & ", "C")},
		{"A/B", artists("A", "/", "B")},
		// Check that bad input is handled reasonably.
		{"Artist 1 & ", artists("Artist 1 & ")},
		{" & Artist 1", artists(" & Artist 1")},
		{" & ", artists(" & ")},
		{", & ", artists(", & ")},
		{",  & ", artists(",  & ")},
		{"", nil},
	} {
		if got := parseArtists(tc.orig); !reflect.DeepEqual(got, tc.want) {
			t.Errorf("parseArtists(%q) = %+v; want %+v", tc.orig, got, tc.want)
		}
	}
}

func FuzzParseArtists(f *testing.F) {
	f.Add("Artist 1")
	f.Add("Artist 1 & Artist 2")
	f.Add("Artist 1, Artist 2 & Artist 3")
	f.Add("Artist 1 feat. Artist 2")
	f.Fuzz(func(t *testing.T, in string) { parseArtists(in) })
}

func TestSupportsHostname(t *testing.T) {
	var pr Provider
	ctx := context.Background()
	cfg := &internal.Config{
		DisallowNetwork: true,
		HostnameAddrs: map[string][]string{
			"bad.com":  {"127.0.0.1"},
			"good.com": {"127.0.0.1", customDomainAddr, "192.168.0.1"},
		},
	}
	for _, tc := range []struct {
		hn   string
		want bool
	}{
		{"blah.bandcamp.com", true},
		{"good.com", true},
		{"bad.com", false},
	} {
		if got := pr.SupportsHostname(ctx, tc.hn, cfg); got != tc.want {
			t.Errorf("SupportsHostname(ctx, %q, cfg) = %v; want %v", tc.hn, got, tc.want)
		}
	}
}

func TestCleanURL(t *testing.T) {
	for _, tc := range []struct {
		in   string
		want string
		ok   bool // if false, error should be returned
	}{
		{"https://louiezong.bandcamp.com/album/cartoon-funk", "https://louiezong.bandcamp.com/album/cartoon-funk", true},
		{"http://louiezong.bandcamp.com/album/cartoon-funk", "https://louiezong.bandcamp.com/album/cartoon-funk", true},
		{"https://pillarsinthesky.bandcamp.com/track/arcanum", "https://pillarsinthesky.bandcamp.com/track/arcanum", true},
		{"http://pillarsinthesky.bandcamp.com/track/arcanum", "https://pillarsinthesky.bandcamp.com/track/arcanum", true},
		{"http://user:pass@artist.bandcamp.com/album/name?foo=bar#hash", "https://artist.bandcamp.com/album/name", true},
		{"https://artist.example.org/album/name", "https://artist.example.org/album/name", true}, // custom domain
		{"https://daily.bandcamp.com/best-jazz/the-best-jazz-on-bandcamp-october-2022", "", false},
	} {
		if got, err := cleanURL(tc.in); !tc.ok && err == nil {
			t.Errorf("cleanURL(%q) = %q; wanted error", tc.in, got)
		} else if tc.ok && err != nil {
			t.Errorf("cleanURL(%q) failed: %v", tc.in, err)
		} else if tc.ok && got != tc.want {
			t.Errorf("cleanURL(%q) = %q; want %q", tc.in, got, tc.want)
		}
	}
}
