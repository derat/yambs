// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package online generates seeded edits from online music providers or databases.
package online

import (
	"context"
	"net/url"
	"strings"

	"codeberg.org/derat/yambs/clog"
	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/slog"
	"codeberg.org/derat/yambs/sources/online/bandcamp"
	"codeberg.org/derat/yambs/sources/online/internal"
	"codeberg.org/derat/yambs/sources/online/metalarchives"
	"codeberg.org/derat/yambs/sources/online/qobuz"
	"codeberg.org/derat/yambs/sources/online/tidal"
	"codeberg.org/derat/yambs/sources/text"
)

type BadURLError = internal.BadURLError

// MakeEdits generates seeded edits from the page at the supplied URL.
// If cfg is nil, a default configuration will be used.
func MakeEdits(ctx context.Context, url string, rawSetCmds []string,
	db *mbdb.DB, cfg *Config) ([]seed.Edit, error) {
	if cfg == nil {
		cfg = &Config{}
	}

	setCmds, err := text.ParseSetCommands(rawSetCmds, seed.ReleaseEntity)
	if err != nil {
		return nil, err
	}

	provider, err := selectProvider(ctx, url, cfg)
	if err != nil {
		return nil, err
	}
	slog.Infof(ctx, "Using online provider %q", provider.String())
	clog.Logf(ctx, "Using online provider %q", provider.String())
	edits, err := provider.MakeEdits(ctx, url, db, (*internal.Config)(cfg))
	if err != nil {
		return nil, err
	}

	for _, edit := range edits {
		// Apply "set" commands to non-informational edits.
		if _, ok := edit.(*seed.Info); !ok {
			for _, cmd := range setCmds {
				if err := text.SetField(edit, cmd[0], cmd[1], nil); err != nil {
					return nil, err
				}
			}
		}
		// Perform additional required updates.
		if err := edit.Finish(ctx, db); err != nil {
			return nil, err
		}
	}

	return edits, nil
}

// Config configures MakeEdits's behavior.
// Aliasing an internal type like this is weird, but it avoids a circular dependency
// (subpackages can't depend on this package since it depends on the subpackages).
type Config internal.Config

var allProviders = []internal.Provider{
	&bandcamp.Provider{},
	&metalarchives.Provider{},
	&qobuz.Provider{},
	&tidal.Provider{},
}

// ExampleURLs holds example URLs that can be displayed to the user.
var ExampleURLs []string

func init() {
	for _, p := range allProviders {
		ExampleURLs = append(ExampleURLs, p.ExampleURL())
	}
}

// selectProvider chooses a provider for handling a user-supplied URL.
func selectProvider(ctx context.Context, orig string, cfg *Config) (internal.Provider, error) {
	parsed, err := url.Parse(orig)
	if err != nil {
		return nil, internal.NewBadURLError(err)
	}
	hostname := strings.ToLower(parsed.Hostname())

	// Parallelize SupportsHostname calls so we won't be blocked by slow DNS queries.
	ch := make(chan internal.Provider, len(allProviders))
	for _, p := range allProviders {
		go func(p internal.Provider) {
			if p.SupportsHostname(ctx, hostname, (*internal.Config)(cfg)) {
				ch <- p
			}
			ch <- nil
		}(p)
	}
	for range allProviders {
		if p := <-ch; p != nil {
			return p, nil
		}
	}

	// Check if another seeder might be able to handle this URL.
	urlErr := BadURLError{Msg: "unsupported host", NoProvider: true}
	for domain, seederURL := range seederDomains {
		if hostname == domain || strings.HasSuffix(hostname, "."+domain) {
			urlErr.SeederURL = seederURL
			break
		}
	}
	return nil, &urlErr
}

// seederDomains maps from common provider domains to other seeders that handle them.
// The regular expression in showError() in render/res/form.ts may need
// to be updated when adding additional URLs.
var seederDomains = map[string]string{
	"apple.com":    atisketURL,
	"beatport.com": harmonyURL,
	"deezer.com":   atisketURL,
	"discogs.com":  userscriptsURL,
	"spotify.com":  atisketURL,
}

const (
	atisketURL     = "https://atisket.pulsewidth.org.uk/"
	harmonyURL     = "https://harmony.pulsewidth.org.uk/"
	userscriptsURL = "https://wiki.musicbrainz.org/Guides/Userscripts"
)
