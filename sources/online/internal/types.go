// Copyright 2023 Daniel Erat.
// All rights reserved.

// Package internal defines internal types and functions for online sources.
package internal

import (
	"context"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
)

// Provider gets information from an online music provider.
type Provider interface {
	// String returns a short, lowercase human-readable string describing this provider.
	String() string

	// SupportsHostname returns true if the supplied hostname (e.g. "www.bandcamp.com")
	// can be handled by this provider. Callers are responsible for lowercasing the
	// hostname and removing ":port" suffixes.
	SupportsHostname(ctx context.Context, hostname string, cfg *Config) bool

	// MakeEdits generates one or more seeded edits based on a user-supplied URL.
	// A BadURLError should be returned if the requested URL is unsupported so a
	// helpful error message can be displayed to the user.
	MakeEdits(ctx context.Context, url string, db *mbdb.DB, cfg *Config) ([]seed.Edit, error)

	// ExampleURL returns an example URL that can be displayed to the user.
	ExampleURL() string
}

// Config is passed to Provider implementations to configure their behavior.
type Config struct {
	// CountryCode contains the ISO 3166 code of the country that should be used when requesting
	// album data, e.g. "US" or "DE". This is currently only used for the Tidal API.
	CountryCode string
	// CorrectPunctuation indicates that punctuation in track titles should be replaced with
	// preferred characters (https://wiki.musicbrainz.org/User:Jacobbrett/English_Punctuation_Guide).
	CorrectPunctuation bool
	// ExtractTrackArtists indicates that artist names should be extracted from the
	// beginnings of track names, e.g. "Artist - Title". This is currently only used
	// for Bandcamp pages.
	ExtractTrackArtists bool
	// RemoveParens indicates that parenthetical expressions (plus brackets) should be removed from
	// the ends of track titles, e.g. "Title (Deluxe Edition)".
	RemoveParens bool
	// SplitArtists indicates that artist names should be automatically split on separators like
	// " & " and " feat. ". This is currently only used for Bandcamp pages.
	SplitArtists bool

	// DisallowNetwork indicates that network requests should not be made.
	// This can be set by tests.
	DisallowNetwork bool
	// TestdataDir overrides FetchPage's default directory of "testdata" for page
	// data when DisallowNetwork is true.
	TestdataDir string
	// HostnameAddrs provides answers for ResolveHostname to return if DisallowNetwork is true.
	HostnameAddrs map[string][]string
}

// BadURLError is returned by MakeEdits to indicate that the supplied URL is unsupported.
type BadURLError struct {
	// Msg contains the text from the underlying Go error.
	Msg string
	// NoProvider is true if no provider that supports the URL's hostname was found.
	NoProvider bool
	// SeederURL optionally contains the URL of an alternate seeder that might support this URL.
	SeederURL string
}

func NewBadURLError(err error) *BadURLError { return &BadURLError{Msg: err.Error()} }

func (e *BadURLError) Error() string { return e.Msg }
