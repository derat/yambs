// Copyright 2023 Daniel Erat.
// All rights reserved.

package internal

import (
	"context"
	"strings"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/slog"
	"codeberg.org/derat/yambs/strutil"
)

// maxEditDist contains the maximum edit distance between a name passed to
// Get*MBIDFromURL and a name in the MusicBrainz database.
const maxEditDist = 2

// labelSuffixes are removed from from the ends of label names before comparing them.
// There are often small differences in how label names are displayed in Bandcamp and
// how they're entered in the database.
var labelSuffixes = []string{
	" limited",
	" ltd",
	" ltd.",
	" music",
	" recordings",
	" records",
	" tapes",
}

// artistPrefixes are removed from from the beginnings of artist names before comparing them.
// See e.g. https://drunkenhearts.bandcamp.com/album/live-for-today
// (credited to "Drunken Hearts", but in MusicBrainz as "The Drunken Hearts").
var artistPrefixes = []string{
	"dj ",
	"mc ",
	"the ",
}

// GetArtistMBIDFromURL attempts to find the MBID of the artist corresponding to url.
// name should contain the artist name as seen online.
func GetArtistMBIDFromURL(ctx context.Context, db *mbdb.DB, url, name string) string {
	artists, err := db.GetArtistsFromURL(ctx, url)
	if err != nil {
		slog.Errorf(ctx, "Failed getting artist MBID from %s: %v", url, err)
		return ""
	}
	return getBestMBID(ctx, artists, name, artistPrefixes, nil)
}

// GetLabelMBIDFromURL attempts to find the MBID of the label corresponding to url.
// name should contain the label name as seen online.
func GetLabelMBIDFromURL(ctx context.Context, db *mbdb.DB, url, name string) string {
	labels, err := db.GetLabelsFromURL(ctx, url)
	if err != nil {
		slog.Errorf(ctx, "Failed getting label MBID from %s: %v", url, err)
		return ""
	}
	return getBestMBID(ctx, labels, name, nil, labelSuffixes)
}

// getBestMBID returns the MBID of the entity in infos with a name closest to the supplied name (as
// determined by Levenshtein edit distance after lowercasing). If prefixes or suffixes are supplied,
// they're trimmed from the beginning or end of infos and name before comparing. If no matching
// entity is found, an empty string is returned.
func getBestMBID(ctx context.Context, infos []mbdb.EntityInfo, name string, prefixes, suffixes []string) string {
	if len(infos) == 0 {
		return ""
	}

	// Special case: if only one entity was found in the database and we don't have a name to
	// compare it against, return it. This handles cases like https://syssistersounds.bandcamp.com/,
	// which seems to be an artist page representing a label with releases by many different artists
	// but no link back to the label page.
	if name == "" && len(infos) == 1 {
		slog.Infof(ctx, "Using %v (only entity)", infos[0].MBID)
		return infos[0].MBID
	}

	// Lowercase, de-accent, and decompose strings before comparing them.
	// Also remove prefixes and suffixes if supplied.
	norm := func(s string) string {
		cleaned := strutil.Normalize(strings.ToLower(s))
		for _, pre := range prefixes {
			cleaned = strings.TrimPrefix(cleaned, pre)
		}
		for _, suf := range suffixes {
			cleaned = strings.TrimSuffix(cleaned, suf)
		}
		return cleaned
	}
	normName := norm(name)

	var bestDist int
	var bestMBID string
	for _, info := range infos {
		infoName := norm(info.Name)
		dist := strutil.Levenshtein(normName, infoName).Dist()
		if dist > maxEditDist {
			continue
		}
		// Require matches of very short strings (e.g. "A" vs. "B") to be exact.
		if dist > 0 && len(name) <= dist {
			continue
		}
		if bestMBID == "" || dist < bestDist {
			bestDist = dist
			bestMBID = info.MBID
		}
	}
	if bestMBID != "" {
		slog.Infof(ctx, "Using %v for %q (edit distance %d)", bestMBID, name, bestDist)
	}
	return bestMBID
}
