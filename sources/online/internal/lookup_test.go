// Copyright 2023 Daniel Erat.
// All rights reserved.

package internal

import (
	"context"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
)

func TestGetBestMBID(t *testing.T) {
	mkInfos := mbdb.MakeEntityInfosForTest
	for _, tc := range []struct {
		infos    []mbdb.EntityInfo
		name     string
		prefixes []string
		suffixes []string
		want     string
	}{
		{nil, "Artist", nil, nil, ""},                                             // no entities
		{mkInfos("123", "Artist"), "Artist", nil, nil, "123"},                     // exact match
		{mkInfos("123", "Artist"), "aRTIST", nil, nil, "123"},                     // case-insensitive
		{mkInfos("123", "aRTIST"), "Artist", nil, nil, "123"},                     // case-insensitive (other direction)
		{mkInfos("123", "Artist"), "ÅřŤíşŦ", nil, nil, "123"},                     // ignore diacritics
		{mkInfos("123", "Artist"), "Artist.", nil, nil, "123"},                    // close match
		{mkInfos("123", "Artist"), "Artista.", nil, nil, "123"},                   // exactly maxEditDist (2)
		{mkInfos("123", "Artist"), ".Artista.", nil, nil, ""},                     // too different
		{mkInfos("123", "Artist", "456", "Someone"), "Artist", nil, nil, "123"},   // matches first
		{mkInfos("123", "Artist", "456", "Someone"), "Someone", nil, nil, "456"},  // matches second
		{mkInfos("123", "Artist", "456", "Someone"), "Some_one", nil, nil, "456"}, // closest to second
		{mkInfos("123", "Artist", "456", "Someone"), "Other", nil, nil, ""},       // matches neither
		{mkInfos("123", "A"), "C", nil, nil, ""},                                  // close match but too short
		{mkInfos("123", "A", "456", "B"), "C", nil, nil, ""},                      // close match but too short
		{mkInfos("123", "A", "456", "B"), "A", nil, nil, "123"},                   // allow short name if exact match
		{mkInfos("123", "Artist"), "", nil, nil, "123"},                           // special case: empty name and single entity
		{mkInfos("123", "Artist", "456", "Someone"), "", nil, nil, ""},            // no special case when multiple entities
		{mkInfos("123", "The Artist"), "Artist", artistPrefixes, nil, "123"},      // ignore common suffix in MB
		{mkInfos("123", "Artist"), "The Artist", artistPrefixes, nil, "123"},      // ignore common suffix in Bandcamp
		{mkInfos("123", "Label Records"), "Label", nil, labelSuffixes, "123"},     // ignore common suffix in MB
		{mkInfos("123", "Label"), "Label Records", nil, labelSuffixes, "123"},     // ignore common suffix in Bandcamp
		{mkInfos("123", "A Records"), "A", nil, labelSuffixes, "123"},             // accept short exact matches
		{mkInfos("123", "Other Records"), "Label", nil, labelSuffixes, ""},        // still need to compare the remainder
		{mkInfos("123", "Other"), "Label Records", nil, labelSuffixes, ""},        // still need to compare the remainder
	} {
		ctx := context.Background()
		if got := getBestMBID(ctx, tc.infos, tc.name, tc.prefixes, tc.suffixes); got != tc.want {
			t.Errorf("getBestMBID(ctx, %v, %q, %q, %q) = %q; want %q",
				tc.infos, tc.name, tc.prefixes, tc.suffixes, got, tc.want)
		}
	}
}
