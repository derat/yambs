// Copyright 2023 Daniel Erat.
// All rights reserved.

package internal

import (
	"testing"
	"unicode/utf8"
)

func TestHostnameHasDomain(t *testing.T) {
	for _, tc := range []struct {
		hostname, domain string
		want             bool
	}{
		{"example.org", "example.org", true},
		{"test.example.org", "example.org", true},
		{"google.com", "example.org", false},
		{"someexample.org", "example.org", false},
		{"example.org", "example.org.com", false},
	} {
		if got := HostnameHasDomain(tc.hostname, tc.domain); got != tc.want {
			t.Errorf("HostnameHasDomain(%q, %q) = %v; want %v", tc.hostname, tc.domain, got, tc.want)
		}
	}
}

func TestRewriteTitle(t *testing.T) {
	type flag uint32
	const (
		release flag = 1 << iota
		correctPunc
		removeParens
	)

	for _, tc := range []struct {
		title string
		flag  flag
		want  string
	}{
		{`Track's -- "Title" (Extended Edition)`, 0, `Track's -- "Title" (Extended Edition)`}, // do nothing
		{"Track's Title (Extended Edition)", correctPunc | removeParens, "Track’s Title"},     // multiple features
		{"Track's Title (Extended Edition)", release | correctPunc | removeParens, "Track’s Title (Extended Edition)"},
		{"Track Title (Extended Edition)", removeParens, "Track Title"},
		{"Track Title (remix) (Extended Edition)", removeParens, "Track Title (remix)"},
		{"(Some) Track Title (Extended Edition)", removeParens, "(Some) Track Title"},
		{"(Some) Track Title", removeParens, "(Some) Track Title"},
		{"Track Title [prod. by somebody]", removeParens, "Track Title"},
		{`"Quoted Title"`, correctPunc, `“Quoted Title”`},
		{`"Quoted Title." "More Quotes!"`, correctPunc, `“Quoted Title.” “More Quotes!”`},
		{`"Unbalanced Quote`, correctPunc, `"Unbalanced Quote`},       // leave it alone
		{`"Quoted 2" Title"`, correctPunc, `"Quoted 2″ Title"`},       // leave outer quotes alone
		{`"Quoted 2" 3" Title"`, correctPunc, `"Quoted 2″ 3″ Title"`}, // leave quoter quotes alone
		{`'Quoted Title'`, correctPunc, `‘Quoted Title’`},
		{"I Can't Help It", correctPunc, "I Can’t Help It"},
		{"Shouldn't've Done That", correctPunc, "Shouldn’t’ve Done That"},
		// TODO: This gets switched to a pair of single quotes instead of two right quotes.
		// https://en.wikipedia.org/wiki/Wikipedia:List_of_English_contractions seems to suggest
		// that either is okay for this particular contraction.
		{"This 'n' That", correctPunc, "This ‘n’ That"},
		{"Where You Goin'", correctPunc, "Where You Goin’"},
		{"'Bout That", correctPunc, "’Bout That"},
		{"Summer of '69", correctPunc, "Summer of ’69"},
		{"In The ' Middle", correctPunc, "In The ' Middle"}, // leave it alone
		{"20' Giraffe", correctPunc, "20′ Giraffe"},
		{`3" Action Figure`, correctPunc, "3″ Action Figure"},
		{"The End...", correctPunc, "The End…"},
		{"Oops! ...I Did It Again", correctPunc, "Oops! …I Did It Again"},
		{"Too many....", correctPunc, "Too many...."}, // leave it alone
		{"Not enough..", correctPunc, "Not enough.."}, // leave it alone
		{"Greatest Hits 1975-2023", correctPunc, "Greatest Hits 1975–2023"},
		{"Greatest Hits 1975-78", correctPunc, "Greatest Hits 1975–78"},
		{"Greatest Hits 75-80", correctPunc, "Greatest Hits 75–80"},
		{"867-5309", correctPunc, "867-5309"}, // leave it alone
		{"First Part -- Second Part", correctPunc, "First Part – Second Part"},
		{"First Part--Second Part", correctPunc, "First Part—Second Part"},
		{"First 1--2 Second", correctPunc, "First 1—2 Second"},
	} {
		isTrack := tc.flag&release == 0
		cfg := Config{
			CorrectPunctuation: tc.flag&correctPunc != 0,
			RemoveParens:       tc.flag&removeParens != 0,
		}
		if got := rewriteTitle(tc.title, &cfg, isTrack); got != tc.want {
			t.Errorf("rewriteTitle(%q, %+v, %v) = %q; want %q", tc.title, cfg, isTrack, got, tc.want)
		}
	}
}

func FuzzReplaceQuotes(f *testing.F) {
	f.Add(`foo`)
	f.Add(`"foo"`)
	f.Add(`"foo" "bar"`)
	f.Fuzz(func(t *testing.T, in string) {
		out := replaceQuotes(in, '"', '“', '”')
		if utf8.Valid([]byte(in)) && !utf8.Valid([]byte(out)) {
			t.Error("Output is no longer valid UTF-8")
		}
	})
}
