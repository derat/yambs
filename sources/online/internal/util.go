// Copyright 2023 Daniel Erat.
// All rights reserved.

package internal

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"unicode"

	"codeberg.org/derat/yambs/clog"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/slog"
	"codeberg.org/derat/yambs/web"
	"golang.org/x/net/html"
)

// HostnameHasDomain returns true if hostname is within domain.
// Provider implementations can use this in SupportsHostname.
func HostnameHasDomain(hostname, domain string) bool {
	return hostname == domain || strings.HasSuffix(hostname, "."+domain)
}

// ResolveHostname resolves the supplied hostname to one or more IP addresses.
func ResolveHostname(ctx context.Context, hostname string, cfg *Config) ([]string, error) {
	if cfg.DisallowNetwork {
		return cfg.HostnameAddrs[hostname], nil
	}
	var res net.Resolver
	return res.LookupHost(ctx, hostname)
}

// FetchPage fetches and parses the supplied URL. If cfg.DisallowNetwork is true, it
// attempts to load the page from a file in a testdata directory instead, with
// "https://www.example.org/foo/bar" mapped to "testdata/www.example.org_foo_bar.html".
func FetchPage(ctx context.Context, url string, cfg *Config) (*web.Page, error) {
	// Fetch the page over the network if we're allowed to do so.
	if !cfg.DisallowNetwork {
		return web.FetchPage(ctx, url)
	}

	// Otherwise, read it from disk.
	f, err := os.Open(getTestdataPathForURL(url, ".html", cfg))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	root, err := html.Parse(f)
	if err != nil {
		return nil, fmt.Errorf("parsing HTML: %w", err)
	}
	return &web.Page{Root: root}, nil
}

// FetchJSON fetches the supplied URL and decodes it into dst.
func FetchJSON(ctx context.Context, url string, dst any, headers map[string]string, cfg *Config) error {
	var r io.Reader
	if !cfg.DisallowNetwork {
		slog.Info(ctx, "Fetching ", url)
		clog.Log(ctx, "Fetching ", url)
		req, err := http.NewRequestWithContext(ctx, "GET", url, nil)
		if err != nil {
			return err
		}
		for k, v := range headers {
			req.Header.Set(k, v)
		}
		res, err := http.DefaultClient.Do(req)
		if err != nil {
			return err
		}
		defer res.Body.Close()
		if res.StatusCode != 200 {
			return fmt.Errorf("status %v: %v", res.StatusCode, res.Status)
		}
		r = res.Body
	} else {
		f, err := os.Open(getTestdataPathForURL(url, ".json", cfg))
		if err != nil {
			return err
		}
		defer f.Close()
		r = f
	}

	return json.NewDecoder(r).Decode(dst)
}

// getTestdataPathForURL returns the path under cfg.TestdataDir where
// data for url should be found.
func getTestdataPathForURL(url, ext string, cfg *Config) string {
	dir := cfg.TestdataDir
	if dir == "" {
		dir = "testdata"
	}
	fn := strings.TrimPrefix(url, "https://")
	fn = strings.NewReplacer("/", "_", "?", "_").Replace(fn)
	fn += ext
	return filepath.Join(dir, fn)
}

// MakeEditNote returns an edit note consisting of the supplied URL and a common suffix.
func MakeEditNote(url string) string {
	return url + "\n\n(seeded using https://codeberg.org/derat/yambs)"
}

// MakeImageEditNote returns an edit note describing an image extracted from a release page.
func MakeImageEditNote(imgURL, relURL string) string { return imgURL + " from " + relURL }

// AppendImageEdit appends an informational edit with the supplied cover image URL
// to edits and returns the updated slice. If edits contains a *seed.Release, it may
// be updated to redirect to the add-cover-art page.
func AppendImageEdit(edits []seed.Edit, imgURL, relURL string) ([]seed.Edit, error) {
	img, err := seed.NewInfo("[cover image]", imgURL)
	if err != nil {
		return edits, err
	}

	if len(edits) > 0 {
		if rel, ok := edits[0].(*seed.Release); ok {
			editNote := MakeImageEditNote(imgURL, relURL)
			if rel.MBID != "" {
				// If we're editing an existing release, we can link directly to its add-cover-art page.
				addCover, err := seed.NewAddCoverArtEdit("[add cover art]", rel.MBID, editNote)
				if err != nil {
					return edits, err
				}
				edits = append(edits, addCover)
			} else {
				// Otherwise, redirect when we get the new MBID after the edit is complete.
				rel.RedirectURI = seed.MakeAddCoverArtRedirectURI(editNote)
			}
		}
	}

	return append(edits, img), nil
}

// RewriteReleaseTitle rewrites the supplied release title as requested by cfg.
func RewriteReleaseTitle(title string, cfg *Config) string {
	return rewriteTitle(title, cfg, false /* isTrack */)
}

// RewriteTrackTitle rewrites the supplied track title as requested by cfg.
// TODO: Should this function take a seed.Track and also extract artists if
// ExtractTrackArtists is set? I've only seen that be needed for Bandcamp albums so far.
func RewriteTrackTitle(title string, cfg *Config) string {
	return rewriteTitle(title, cfg, true /* isTrack */)
}

// rewriteTitle is a helper function used by RewriteReleaseTitle and RewriteTrackTitle.
func rewriteTitle(title string, cfg *Config, isTrack bool) string {
	// Only remove ETI for tracks, since it's possibly still wanted for releases.
	if cfg.RemoveParens && isTrack {
		title = parenRegexp.ReplaceAllLiteralString(title, "")
		title = bracketRegexp.ReplaceAllLiteralString(title, "")
	}

	if cfg.CorrectPunctuation {
		// Replace paired quotation marks with left/right double quotation marks and
		// paired apostrophes with left/right single quotation marks.
		title = replaceQuotes(title, '"', '“', '”')
		title = replaceQuotes(title, '\'', '‘', '’')

		// For other replacements, first search for re and then replace substrings in matches.
		repl := func(re *regexp.Regexp, from, to string) {
			title = re.ReplaceAllStringFunc(title, func(s string) string {
				return strings.ReplaceAll(s, from, to)
			})
		}
		repl(aposLetterNumberRegexp, "'", "’") // preceding apostrophes to right single quotation marks
		repl(letterAposRegexp, "'", "’")       // trailing apostrophes to right single quotation marks
		repl(numberAposRegexp, "'", "′")       // apostrophes after numbers to primes (for minutes/feet)
		repl(numberQuoteRegexp, `"`, `″`)      // quotation marks after numbers to double primes (for seconds/inches)
		repl(ellipsisRegexp, "...", "…")       // three periods to horizontal ellipsis
		repl(yearRangeRegexp, "-", "–")        // hyphens in year ranges to en dash

		repl(doubleUnspacedHyphenRegexp, "--", "—") // two hyphens without spaces to em dash
		repl(doubleSpacedHyphenRegexp, "--", "–")   // two hyphens with spaces to en dash

		// TODO: Consider trying to replace hyphens between numbers with ‒ (figure dash) for e.g.
		// phone numbers. I'm not sure if this is common enough to be worth it, since it'll be wrong
		// for subtraction, where − (minus sign) should be used instead.
	}

	return title
}

// replaceQuotes replaces the supplied old quote character in orig with left and right quotes.
// If the quotes aren't balanced, or left and right quotes aren't preceded and followed by spaces,
// then orig is returned unmodified. I was hoping I could just use a regular expression for this,
// but I'm not sure if that's possible since the regexp package doesn't support lookaround.
func replaceQuotes(orig string, old, left, right rune) string {
	var cleaned string
	var inQuote bool // saw opening, waiting for closing
	runes := []rune(orig)
	for i, ch := range runes {
		if ch != old {
			cleaned += string(ch)
			continue
		}
		if !inQuote {
			if i > 0 && !unicode.IsSpace(runes[i-1]) {
				return orig // not at beginning and not preceded by space
			}
			cleaned += string(left)
		} else {
			if i < len(runes)-1 && !unicode.IsSpace(runes[i+1]) {
				return orig // not at end and not followed by space
			}
			cleaned += string(right)
		}
		inQuote = !inQuote
	}
	if inQuote {
		return orig // unbalanced quotes
	}
	return cleaned
}

var (
	// parenRegexp matches a parenthetical suffix.
	parenRegexp = regexp.MustCompile(`\s+\([^)]+\)\s*$`)
	// bracketRegexp matches a square-bracketed suffix.
	bracketRegexp = regexp.MustCompile(`\s+\[[^]]+\]\s*$`)
	// aposLetterNumberRegexp matches an apostrophe followed by a letter or number.
	aposLetterNumberRegexp = regexp.MustCompile(`'[\p{L}\p{N}]`)
	// letterAposRegexp matches an apostrophe preceded by a letter (but not a number, to
	// avoid matching things like "200'" that are referring to feet).
	letterAposRegexp = regexp.MustCompile(`[\p{L}]'`)
	// numberAposRegexp matches an apostrophe preceded by a number.
	numberAposRegexp = regexp.MustCompile(`[\p{N}]'`)
	// numberQuoteRegexp matches a quotation mark preceded by a number.
	numberQuoteRegexp = regexp.MustCompile(`[\p{N}]"`)
	// ellipsisRegexp matches three periods surrounded by non-period characters.
	ellipsisRegexp = regexp.MustCompile(`(^|[^.])\.\.\.($|[^.])`)
	// doubleUnspacedHyphenRegexp matches two hyphens surrounded by letters or numbers.
	doubleUnspacedHyphenRegexp = regexp.MustCompile(`[\p{L}\p{N}]--[\p{L}\p{N}]`)
	// doubleSpacedHyphenRegexp matches two hyphens surrounded by whitespace.
	doubleSpacedHyphenRegexp = regexp.MustCompile(`\s--\s`)
	// yearRangeRegexp matches various year ranges like "79-80", "1979-1980", and "1979-80".
	yearRangeRegexp = regexp.MustCompile(`\b(19|20)?\d\d-(19|20)?\d\d\b`)
)
