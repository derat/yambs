# Metal Archives testdata

This directory contains mirrored pages from the [Metal Archives] for use in unit
tests.

The pages' original public URLs can be derived by prepending `https://`,
replacing underscores with slashes, and removing the trailing `.html`.

Ownership is retained by Metal Archives... or maybe its contributors? I searched
for a bit but it seems like they haven't documented how user-submitted data is
licensed (which is a pretty metal thing to do!).

[Metal Archives]: https://www.metal-archives.com/
