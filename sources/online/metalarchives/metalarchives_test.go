// Copyright 2022 Daniel Erat.
// All rights reserved.

package metalarchives

import (
	"context"
	"testing"

	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/sources/online/internal"
	"github.com/google/go-cmp/cmp"
)

func TestMakeEdits(t *testing.T) {
	ctx := context.Background()
	var pr Provider

	for _, tc := range []struct {
		url string
		art *seed.Artist
	}{
		{
			url: "https://www.metal-archives.com/artists/Apollyon_Baphomet/473942",
			art: &seed.Artist{
				Name:           "Apollyon Baphomet",
				Type:           seed.ArtistType_Person,
				Gender:         seed.Gender_Male,
				Disambiguation: "Vance Reon Jeffcoat",
				BeginDate:      seed.MakeDate(1981, 3, 31),
				BeginAreaName:  "United States (Columbia, South Carolina)",
				EndDate:        seed.MakeDate(2017, 12, 1),
				Ended:          true,
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/artists/Apollyon_Baphomet/473942",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/artists/Apollyon_Baphomet/473942"),
			},
		},
		{
			url: "https://www.metal-archives.com/artists/Leo_Unnermark/940705",
			art: &seed.Artist{
				Name:           "Leo Unnermark",
				Type:           seed.ArtistType_Person,
				Gender:         seed.Gender_Male,
				Disambiguation: "Leo Anders Unnermark",
				BeginDate:      seed.MakeDate(1997, 3, 17),
				BeginAreaName:  "Sweden (Värmdö)",
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/artists/Leo_Unnermark/940705",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/artists/Leo_Unnermark/940705"),
			},
		},
		{
			url: "https://www.metal-archives.com/artists/Pope_Richard/491533",
			art: &seed.Artist{
				Name:           "Pope Richard",
				Type:           seed.ArtistType_Person,
				Gender:         seed.Gender_NonBinary,
				Disambiguation: "Richard Alan Weeks",
				BeginDate:      seed.MakeDate(1982, 9, 21),
				BeginAreaName:  "Canada (Dartmouth, Nova Scotia)",
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/artists/Pope_Richard/491533",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/artists/Pope_Richard/491533"),
			},
		},
		{
			url: "https://www.metal-archives.com/bands/Carnage/830",
			art: &seed.Artist{
				Name:          "Carnage",
				Type:          seed.ArtistType_Group,
				AreaName:      "Växjö, Kronoberg (early); Stockholm (later)",
				BeginDate:     seed.MakeDate(1988, 0, 0),
				BeginAreaName: "Sweden",
				EndDate:       seed.MakeDate(1990, 0, 0),
				Ended:         true,
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/bands/Carnage/830",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/bands/Carnage/830"),
			},
		},
		{
			url: "https://www.metal-archives.com/bands/Krsn%C4%AB/3540509808",
			art: &seed.Artist{
				Name:          "Krsnī",
				Type:          seed.ArtistType_Group,
				BeginDate:     seed.MakeDate(2019, 0, 0),
				BeginAreaName: "Uzbekistan",
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/bands/Krsn%C4%AB/3540509808",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/bands/Krsn%C4%AB/3540509808"),
			},
		},
		{
			url: "https://www.metal-archives.com/bands/Wings_of_Steel/3540503428",
			art: &seed.Artist{
				Name:          "Wings of Steel",
				Type:          seed.ArtistType_Group,
				AreaName:      "Los Angeles, California",
				BeginDate:     seed.MakeDate(2019, 0, 0),
				BeginAreaName: "United States",
				URLs: []seed.URL{
					{
						URL:      "https://www.metal-archives.com/bands/Wings_of_Steel/3540503428",
						LinkType: seed.LinkType_OtherDatabases_Artist_URL,
					},
				},
				EditNote: internal.MakeEditNote("https://www.metal-archives.com/bands/Wings_of_Steel/3540503428"),
			},
		},
	} {
		t.Run(tc.url, func(t *testing.T) {
			cfg := internal.Config{DisallowNetwork: true}
			edits, err := pr.MakeEdits(ctx, tc.url, nil, &cfg)
			if err != nil {
				t.Fatal("Failed parsing page:", err)
			} else if len(edits) != 1 {
				t.Errorf("Got %d edits", len(edits))
			} else if diff := cmp.Diff(tc.art, edits[0]); diff != "" {
				t.Error("Bad artist data:\n" + diff)
			}
		})
	}
}

func TestCleanURL(t *testing.T) {
	for _, tc := range []struct {
		orig string
		want string
		ok   bool // if false, error should be returned
	}{
		{"https://www.metal-archives.com/bands/Carnage/830", "https://www.metal-archives.com/bands/Carnage/830", true},
		{"https://www.metal-archives.com/artists/Name/1", "https://www.metal-archives.com/artists/Name/1", true},
		{"http://www.metal-archives.com/bands/Carnage/830", "https://www.metal-archives.com/bands/Carnage/830", true},
		{"https://metal-archives.com/artists/Name/1.html?a=b#c", "https://www.metal-archives.com/artists/Name/1", true},
		{"https://metal-archives.com/artists/Name/1.html?a=b#c", "https://www.metal-archives.com/artists/Name/1", true},
		{"https://www.metal-archives.com/albums/Murdershock/Reasoning_Insanity/238432", "", false},
		{"https://example.org/artists/Name/1", "", false},
	} {
		if got, err := cleanURL(tc.orig); !tc.ok && err == nil {
			t.Errorf("cleanURL(%q) = %q; wanted error", tc.orig, got)
		} else if tc.ok && err != nil {
			t.Errorf("cleanURL(%q) failed: %v", tc.orig, err)
		} else if tc.ok && got != tc.want {
			t.Errorf("cleanURL(%q) = %q; want %q", tc.orig, got, tc.want)
		}
	}
}
