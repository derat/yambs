// Copyright 2023 Daniel Erat.
// All rights reserved.

// Package metalarchives extracts information from metal-archives.com pages.
package metalarchives

import (
	"context"
	"errors"
	"fmt"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/slog"
	"codeberg.org/derat/yambs/sources/online/internal"
	"codeberg.org/derat/yambs/web"
)

// Provider implements internal.Provider for the Metal Archives.
//
// There's apparently no API: https://forum.metal-archives.com/viewtopic.php?f=3&t=111208
type Provider struct{}

var _ = internal.Provider(&Provider{})

func (p *Provider) String() string { return "metalarchives" }

func (p *Provider) SupportsHostname(ctx context.Context, hn string, cfg *internal.Config) bool {
	return internal.HostnameHasDomain(hn, "metal-archives.com")
}

// pathRegexp matches a path in a URL accepted by cleanURL.
// The first match group describes the page type.
// The second match group matches junk after the ID, which the site seems to just ignore.
var pathRegexp = regexp.MustCompile(`^/(artists|bands)/[^/]+/\d+(.*)$`)

// cleanURL returns a cleaned version of a metal-archives.com URL like
// "https://www.metal-archives.com/artists/Leo_Unnermark/940705" or
// "https://www.metal-archives.com/bands/Wings_of_Steel/3540503428".
func cleanURL(orig string) (string, error) {
	u, err := url.Parse(orig)
	if err != nil {
		return "", err
	}
	u.Host = strings.ToLower(u.Host)
	if u.Host == "metal-archives.com" {
		u.Host = "www.metal-archives.com" // the site seems do this redirect
	} else if u.Host != "www.metal-archives.com" {
		return "", errors.New("host not www.metal-archives.com")
	}
	if m := pathRegexp.FindStringSubmatch(u.Path); m == nil {
		return "", errors.New(`path not "/<type>/<name>/<id>"`)
	} else if m[1] != "artists" && m[1] != "bands" {
		return "", errors.New(`only "artists" and "bands" types are supported`)
	} else {
		// Trim any junk after the numeric ID.
		u.Path = u.Path[:len(u.Path)-len(m[2])]
	}
	u.Scheme = "https"
	u.User = nil
	u.RawQuery = ""
	u.Fragment = ""
	return u.String(), nil
}

func (p *Provider) ExampleURL() string { return "https://www.metal-archives.com/artists/…" }

// MakeEdits generates a seed.Artist edit from the supplied Metal Archives URL.
func (p *Provider) MakeEdits(ctx context.Context, pageURL string, _ *mbdb.DB, cfg *internal.Config) (
	[]seed.Edit, error) {
	pageURL, err := cleanURL(pageURL)
	if err != nil {
		return nil, internal.NewBadURLError(err)
	}
	page, err := internal.FetchPage(ctx, pageURL, cfg)
	if err != nil {
		return nil, err
	}

	artist := &seed.Artist{EditNote: internal.MakeEditNote(pageURL)}
	var section string // selector for details section
	if artist.Name, err = page.Query("#member_info h1.band_member_name").Text(true); err == nil {
		artist.Type = seed.ArtistType_Person
		section = "#member_info"
	} else if artist.Name, err = page.Query("#band_info h1.band_name").Text(true); err == nil {
		artist.Type = seed.ArtistType_Group
		section = "#band_info"
	} else {
		return nil, errors.New("failed extracting name")
	}

	// Everything else seems to appear in a sequence of <dt> (term) and <dd> (description) elements.
	res := page.QueryAll(section + " dt, " + section + " dd")
	if res.Err != nil {
		return nil, fmt.Errorf("details: %w", res.Err)
	}
	vals := make(map[string]string)
	var term string // current term
	for _, n := range res.Nodes {
		switch n.Data {
		case "dt":
			term = strings.TrimSpace(web.GetText(n, true))
		case "dd":
			if term == "" {
				continue
			}
			vals[term] = strings.TrimSpace(web.GetText(n, true))
			term = ""
		}
	}

	switch artist.Type {
	case seed.ArtistType_Person:
		if rn := vals["Real/full name:"]; rn != artist.Name {
			artist.Disambiguation = rn
		}
		artist.BeginDate = extractDate(ctx, vals["Age:"])
		artist.BeginAreaName = vals["Place of birth:"]
		artist.EndDate = extractDate(ctx, vals["R.I.P.:"])
		artist.Ended = !artist.EndDate.Empty()
		artist.Gender = genderMap[vals["Gender:"]]

	case seed.ArtistType_Group:
		if loc := vals["Location:"]; loc != "N/A" {
			artist.AreaName = loc
		}
		artist.BeginDate = parseYear(vals["Formed in:"])
		artist.BeginAreaName = vals["Country of origin:"]

		// Statuses per https://www.metal-archives.com/news/view/id/283:
		// "There are six options: active, on hold, split-up, unknown, changed name, and disputed."
		if vals["Status:"] == "Split-up" {
			artist.Ended = true
			if m := lastYearRegexp.FindStringSubmatch(vals["Years active:"]); m != nil {
				artist.EndDate = parseYear(m[1])
			}
		}
	}

	// TODO: Also seed band membership and members.

	artist.URLs = append(artist.URLs, seed.URL{
		URL:      pageURL,
		LinkType: seed.LinkType_OtherDatabases_Artist_URL,
	})

	return []seed.Edit{artist}, nil
}

func extractDate(ctx context.Context, s string) seed.Date {
	m := dateRegexp.FindStringSubmatch(s)
	if m == nil {
		return seed.Date{}
	}
	t, err := time.Parse("Jan 2 2006", strings.Join(m[1:], " "))
	if err != nil {
		slog.Error(ctx, "Failed extracting date: ", err)
		return seed.Date{}
	}
	return seed.DateFromTime(t)
}

func parseYear(s string) seed.Date {
	var date seed.Date
	date.Year, _ = strconv.Atoi(s)
	return date
}

var (
	// Extracts the month, day, and year from a string including a date like "Mar 31st, 1981".
	dateRegexp = regexp.MustCompile(`\b([A-Z][a-z]{2})\s+(\d+)(?:st|nd|rd|th),\s+(\d{4})\b`)
	// Extracts the last 4-digit number from a string like "1988, 1988-1990".
	lastYearRegexp = regexp.MustCompile(`(?s).*\b(\d{4})\b`) // ?s to make '.' match newlines
	// Maps from a Metal Archive gender string to the corresponding seed.Gender value.
	genderMap = map[string]seed.Gender{
		"Female":     seed.Gender_Female,
		"Male":       seed.Gender_Male,
		"Non-binary": seed.Gender_NonBinary,
	}
)
