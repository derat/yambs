// Copyright 2023 Daniel Erat.
// All rights reserved.

package online

import (
	"context"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
)

func TestMakeEdits(t *testing.T) {
	// This test exercises the MakeEdits function's code for choosing providers.
	// Individual providers have their own tests to exercise their code for generating edits.
	for _, tc := range []struct {
		url       string // URL to fetch
		dir       string // testdata dir
		edits     int    // 0 means BadURLError should be returned
		seederURL string // expected value for BadURLError.SeederURL if edits is 0
	}{
		{
			url:   "https://louiezong.bandcamp.com/album/cartoon-funk",
			dir:   "bandcamp/testdata",
			edits: 2,
		},
		{
			// URLs should cleaned before they're fetched.
			url:   "http://LOUIEZONG.BANDCAMP.COM/ALBUM/CARTOON-FUNK?some-param=2#id",
			dir:   "bandcamp/testdata",
			edits: 2,
		},
		{
			// URLs should be passed to the correct providers.
			url:   "https://www.qobuz.com/us-en/album/in-rainbows-radiohead/0634904032432",
			dir:   "qobuz/testdata",
			edits: 2,
		},
		{
			// A "bad URL" error should be returned for an unsupported hostname.
			url:   "http://example.org/track/some-song",
			dir:   "bandcamp/testdata", // arbitrary
			edits: 0,
		},
		{
			// Check that other seeders are recommended.
			url:       "https://open.spotify.com/album/5lYMXPw1OKLap9gh0LUvyi",
			dir:       "bandcamp/testdata", // arbitrary
			edits:     0,
			seederURL: atisketURL,
		},
	} {
		db := mbdb.NewDB(mbdb.DisallowQueries)
		cfg := &Config{
			DisallowNetwork: true,
			TestdataDir:     tc.dir,
		}
		edits, err := MakeEdits(context.Background(), tc.url, nil, db, cfg)
		if tc.edits > 0 {
			if err != nil {
				t.Errorf("MakeEdits(ctx, %q, ...) failed: %v", tc.url, err)
			} else if len(edits) != tc.edits {
				t.Errorf("MakeEdits(ctx, %q, ...) returned %d edit(s); want %d",
					tc.url, len(edits), tc.edits)
			}
		} else {
			if err == nil {
				t.Errorf("MakeEdits(ctx, %q, ...) unexpectedly succeeded", tc.url)
			} else if urlErr, ok := err.(*BadURLError); !ok {
				t.Errorf("MakeEdits(ctx, %q, ...) returned wrong error: %v", tc.url, err)
			} else if urlErr.SeederURL != tc.seederURL {
				t.Errorf("MakeEdits(ctx, %q, ...) suggested seeder %q; want %q",
					tc.url, urlErr.SeederURL, tc.seederURL)
			}
		}
	}
}

// Test passing extra "set" commands to MakeEdits.
func TestMakeEdits_SetCommands(t *testing.T) {
	const (
		url   = "https://louiscole.bandcamp.com/album/let-it-happen"
		title = "Some Name"
	)
	db := mbdb.NewDB(mbdb.DisallowQueries)
	cfg := &Config{
		DisallowNetwork: true,
		TestdataDir:     "bandcamp/testdata",
	}
	edits, err := MakeEdits(context.Background(), url, []string{"title=" + title}, db, cfg)
	if err != nil {
		t.Fatal("MakeEdits failed: ", err)
	} else if len(edits) == 0 {
		t.Fatal("MakeEdits returned zero edits")
	}
	if rel, ok := edits[0].(*seed.Release); !ok {
		t.Errorf("MakeEdits returned non-release edit of type %T", edits[0])
	} else if rel.Title != title {
		t.Errorf("MakeEdits returned title %q; want %q", rel.Title, title)
	}
}
