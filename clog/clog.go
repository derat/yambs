// Copyright 2024 Daniel Erat.
// All rights reserved.

// Package clog provides an interface for sending log messages from yambsd to clients.
package clog

import (
	"context"
	"fmt"
)

type clogKeyType string

var clogKey = clogKeyType("clog")

// Logger sends informational log messages to a client.
// This interface exists to prevent circular dependencies: this package can't depend on render,
// since render depends on seed and sources/text and those packages depend on this package to
// perform logging.
type Logger interface {
	// Log writes the supplied message.
	Log(string) error
}

// LoggerContext attaches a Logger to a new context derived from ctx.
// The returned context can later be passed to Log or Logf.
func LoggerContext(ctx context.Context, lg Logger) context.Context {
	return context.WithValue(ctx, clogKey, lg)
}

// Log sends the supplied text, interpreted using fmt.Sprint.
func Log(ctx context.Context, args ...any) error {
	if lg, ok := ctx.Value(clogKey).(Logger); ok {
		return lg.Log(fmt.Sprint(args...))
	}
	return nil
}

// Logf sends the supplied text, intepreted using fmt.Sprintf.
func Logf(ctx context.Context, format string, args ...any) error {
	return Log(ctx, fmt.Sprintf(format, args...))
}
