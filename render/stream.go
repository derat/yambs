// Copyright 2024 Daniel Erat.
// All rights reserved.

package render

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
	"time"
)

// Stream writes JSON-serialized messages to a http.ResponseWriter.
type Stream struct {
	rw      http.ResponseWriter
	rc      *http.ResponseController
	mu      sync.Mutex       // protects rw and rc
	nowFunc func() time.Time // returns the current time
}

// NewStream returns a new Stream.
// Data shouldn't be written to rw before (or after) calling this.
// Non-test code should just pass time.Now as nowFunc.
func NewStream(rw http.ResponseWriter, nowFunc func() time.Time) *Stream {
	rw.Header().Set("Content-Type", "application/stream+json")

	// I'm not sure if this is still necessary, but it's recommend in various places.
	// See e.g. https://stackoverflow.com/a/30603654.
	rw.Header().Set("X-Content-Type-Options", "nosniff")

	return &Stream{
		rw:      rw,
		rc:      http.NewResponseController(rw),
		nowFunc: nowFunc,
	}
}

// Log implements the clog.Logger interface.
func (s *Stream) Log(text string) error {
	return s.write(&message{Time: s.nowFunc().UTC(), Log: text})
}

// Edits writes the supplied edits to the stream.
func (s *Stream) Edits(edits []*EditInfo) error {
	return s.write(&message{Time: s.nowFunc().UTC(), Edits: edits})
}

// Error writes the supplied fatal error to the stream.
func (s *Stream) Error(text string) error {
	return s.write(&message{Time: s.nowFunc().UTC(), Error: text})
}

// write writes msg to s.rw.
func (s *Stream) write(msg *message) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return err
	}
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, err := fmt.Fprintln(s.rw, string(b)); err != nil {
		return err
	}
	return s.rc.Flush()
}

// message describes a single message written by Stream.
type message struct {
	// Time contains the time at which the message was produced.
	Time time.Time `json:"time"`
	// Log contains an informational log message.
	Log string `json:"log"`
	// Error contains a fatal error message. No further messages will be sent.
	Error string `json:"error"`
	// Edits contains the generated edits. No further messages will be sent.
	Edits []*EditInfo `json:"edits"`
}
