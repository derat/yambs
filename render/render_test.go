// Copyright 2022 Daniel Erat.
// All rights reserved.

package render

import (
	"bytes"
	"context"
	"strings"
	"testing"
	"text/template"
	"time"

	"codeberg.org/derat/validate"
	"codeberg.org/derat/yambs/seed"
	"golang.org/x/net/html"
)

const validateTimeout = 30 * time.Second

func TestWrite_Edits(t *testing.T) {
	info, err := seed.NewInfo("Info Description", "https://www.example.org/")
	if err != nil {
		t.Fatal("NewInfo failed:", err)
	}
	edits := []seed.Edit{
		&seed.Release{
			Title:   "Release Title",
			Artists: []seed.ArtistCredit{{Name: "Release Artist"}},
		},
		&seed.Recording{
			Name:    "Recording Name",
			Artists: []seed.ArtistCredit{{Name: "Recording Artist"}},
		},
		info,
	}

	const srvURL = "https://test.musicbrainz.org"
	var b bytes.Buffer
	if err := Write(&b, edits, ServerURL(srvURL)); err != nil {
		t.Fatal("Write failed:", err)
	}

	// Just perform some basic tests that the edit descriptions and URLs were included
	// and that the page is parseable HTML.
	for _, ed := range edits {
		if desc := template.JSEscapeString(ed.Description()); !strings.Contains(b.String(), desc) {
			t.Errorf("Write didn't include edit description %q", desc)
		}
		if url := ed.URL(srvURL); !strings.Contains(b.String(), url) {
			t.Errorf("Write didn't include edit URL %q", url)
		}
	}
	checkHTML(t, b.Bytes())
}

func TestWrite_Form(t *testing.T) {
	// Also check that we can write the no-edits version of the page with the form.
	const version = "20221105-deadbeef"
	var b bytes.Buffer
	if err := Write(&b, nil, Version(version)); err != nil {
		t.Fatal("Write failed:", err)
	}
	if !strings.Contains(b.String(), version) {
		t.Errorf("Write didn't include version %q", version)
	}
	checkHTML(t, b.Bytes())
}

// checkHTML verifies that the supplied HTML document parses and validates.
func checkHTML(t *testing.T, b []byte) {
	if _, err := html.Parse(bytes.NewReader(b)); err != nil {
		t.Error("Write wrote invalid HTML:", err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), validateTimeout)
	defer cancel()

	if issues, _, err := validate.HTML(ctx, bytes.NewReader(b)); err != nil {
		t.Error("HTML validation failed:", err)
	} else {
		for _, issue := range issues {
			t.Error("HTML:", issue.String())
		}
	}
	if issues, _, err := validate.CSS(ctx, bytes.NewReader(b), validate.HTMLDoc); err != nil {
		t.Error("CSS validation failed:", err)
	} else {
		for _, issue := range issues {
			t.Error("CSS:", issue.String())
		}
	}
}
