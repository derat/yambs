// Copyright 2024 Daniel Erat.
// All rights reserved.

//go:build !embedjs

package render

import _ "embed"

var (
	//go:embed res/common.ts
	commonTS string
	//go:embed res/form.ts
	formTS string
	//go:embed res/edit.ts
	editTS string
)

// getJS returns JavaScript code to inject into the HTML template.
//
// This implementation uses esbuild to transpile the original embedded TypeScript code,
// which is more convenient for development.
func getJS(cfg *config) (common, form, edit string, err error) {
	// Transform TypeScript to JavaScript.
	if common, err = TransformTS(commonTS, "res/common.ts", cfg.minifyJS); err != nil {
		return "", "", "", err
	}
	if form, err = TransformTS(formTS, "res/form.ts", cfg.minifyJS); err != nil {
		return "", "", "", err
	}
	if edit, err = TransformTS(editTS, "res/edit.ts", cfg.minifyJS); err != nil {
		return "", "", "", err
	}
	return common, form, edit, nil
}
