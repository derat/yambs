// Copyright 2024 Daniel Erat.
// All rights reserved.

package render

import (
	"errors"
	"fmt"
	"path/filepath"

	"github.com/evanw/esbuild/pkg/api"
)

// As of May 2024, musicbrainz-server appears to still support ES5 (!) per webpack/browserConfig.mjs.
// I think that yambs was using ES2019 features (e.g. Object.fromEntries and String.trimStart) before
// it was using esbuild, so targeting ES2019 seems safe for now.
const esbuildTarget = api.ES2019

// TransformTS transpiles the supplied TypeScript code from the named file to JavaScript.
func TransformTS(ts, fn string, minify bool) (string, error) {
	res := api.Transform(ts, api.TransformOptions{
		Charset:           api.CharsetUTF8,
		Format:            api.FormatDefault,
		Loader:            api.LoaderTS,
		MinifyIdentifiers: minify,
		MinifySyntax:      minify,
		MinifyWhitespace:  minify,
		Sourcefile:        fn,
		Target:            esbuildTarget,
	})
	if len(res.Errors) > 0 {
		return "", errors.New(getTransformMessage(res.Errors[0]))
	}
	return string(res.Code), nil
}

func getTransformMessage(msg api.Message) string {
	var s string
	if loc := msg.Location; loc != nil {
		s = fmt.Sprintf("%s:%d:%d: ", filepath.Base(loc.File), loc.Line, loc.Column)
	}
	s += msg.Text
	return s
}
