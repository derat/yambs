// Copyright 2024 Daniel Erat.
// All rights reserved.

// Corresponds to |message| in render/stream.go.
declare interface EditsMessage {
  time: string; // RFC 3339
  log?: string;
  error?: string;
  edits?: Edit[];
}

// Corresponds to |EditInfo| in render/render.go and
// to |initialEdits| objects in index.tmpl.
declare interface Edit {
  desc: string;
  url: string;
  params: EditParam[];
  serial: boolean;
}

// Corresponds to |paramInfo| in render/render.go.
declare interface EditParam {
  name: string;
  value: string;
}
