// Copyright 2024 Daniel Erat.
// All rights reserved.

// Transpiles TypeScript files to JavaScript for the embedjs build flag.
package main

//go:generate go run gen_js.go

import (
	"log"
	"os"
	"path/filepath"
	"strings"

	"codeberg.org/derat/yambs/render"
)

const (
	inDir  = "."
	outDir = "gen"
)

func main() {
	ps, err := filepath.Glob(filepath.Join(inDir, "*.ts"))
	if err != nil {
		log.Fatal(err)
	} else if len(ps) == 0 {
		log.Fatalf("No .ts files found in %v", inDir)
	}
	if err := os.MkdirAll(outDir, 0755); err != nil {
		log.Fatal(err)
	}
	for _, p := range ps {
		ts, err := os.ReadFile(p)
		if err != nil {
			log.Fatal(err)
		}
		js, err := render.TransformTS(string(ts), p, true /* minify */)
		if err != nil {
			log.Fatal(err)
		}
		dst := filepath.Join(outDir, strings.TrimSuffix(filepath.Base(p), ".ts")+".js")
		log.Printf("Writing %v", dst)
		if err := os.WriteFile(dst, []byte(js), 0644); err != nil {
			log.Fatal(err)
		}
	}
}
