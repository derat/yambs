// Copyright 2024 Daniel Erat.
// All rights reserved.

// These constants are defined by index.tmpl.
// This file is loaded by tsconfig.json to make tsc happy.
const initialEdits: Edit[] = [];
const formTypes: String[] = [];
const formSetExamples: Record<string, string> = {};
const formFieldsExamples: Record<string, string> = {};
const formInputExamples: Record<string, Record<string, string>> = {};
