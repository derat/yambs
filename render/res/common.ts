// Copyright 2024 Daniel Erat.
// All rights reserved.

function $<T extends HTMLElement = HTMLElement>(id: string) {
  const el = document.getElementById(id);
  if (!el) throw new Error(`Didn't find element #${id}`);
  return el as T;
}

const $button = (id: string) => $<HTMLButtonElement>(id);
const $input = (id: string) => $<HTMLInputElement>(id);
const $select = (id: string) => $<HTMLSelectElement>(id);
const $textArea = (id: string) => $<HTMLTextAreaElement>(id);

function createElement(
  type: string,
  parentElement: HTMLElement | null = null,
  className: string | null = null,
  text: string | null = null
) {
  const element = document.createElement(type);
  if (parentElement) parentElement.appendChild(element);
  if (className) element.className = className;
  if (text || text === '') element.appendChild(document.createTextNode(text));
  return element;
}

const show = (el: HTMLElement, visible: Boolean = true) =>
  visible
    ? el.style.removeProperty('display')
    : el.style.setProperty('display', 'none');

const hide = (el: HTMLElement) => show(el, false);
