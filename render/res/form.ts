// Copyright 2024 Daniel Erat.
// All rights reserved.

(() => {
  const uiStateKey = 'uiState';

  const sourceSelect = $select('form-source-select');
  const onlineUrlInput = $input('form-online-url-input');
  const onlineMbidInput = $input('form-online-mbid-input');
  const onlineCountryInput = $input('form-online-country-input');
  const onlineExtractCheckbox = $input('form-online-extract-artists-checkbox');
  const onlineSplitCheckbox = $input('form-online-split-artists-checkbox');
  const onlineParensCheckbox = $input('form-online-remove-parens-checkbox');
  const onlinePunctuationCheckbox = $input('form-online-punctuation-checkbox');
  const textFormatSelect = $select('form-text-format-select');
  const textTypeSelect = $select('form-text-type-select');
  const textReleaseInput = $input('form-text-release-input');
  const textToggleFieldsButton = $('form-text-toggle-fields-button');
  const textSetExample = $('form-text-set-example');
  const textSetTextarea = $textArea('form-text-set-textarea');
  const textFieldsExample = $('form-text-fields-example');
  const textFieldsInput = $input('form-text-fields-input');
  const textInputFormat = $input('form-text-input-format');
  const textInputExample = $('form-text-input-example');
  const textInputTextarea = $textArea('form-text-input-textarea');
  const textInputFileInput = $input('form-text-input-file-input');
  const textInputFileButton = $button('form-text-input-file-button');
  const betaCheckbox = $input('form-beta-checkbox');
  const genButton = $button('form-generate-button');
  const clearButton = $button('form-clear-button');
  const permalink = $<HTMLLinkElement>('form-permalink');
  const errorDiv = $('form-error-div');
  const logSection = $('log-section');
  const logContainer = $('log-container');

  let textFieldsTableShown = false;

  // Updates the state of form-related elements.
  function updateUI(save = true) {
    const source = sourceSelect.value;
    show($('form-online-section'), source === 'online');
    show($('form-text-section'), source === 'text' || source === 'recordings');

    const type = source === 'recordings' ? 'recording' : textTypeSelect.value;
    for (const t of formTypes) {
      show(
        $(`form-text-${t}-fields-table`),
        textFieldsTableShown && t === type
      );
    }
    textToggleFieldsButton.textContent =
      (textFieldsTableShown ? 'Hide' : 'Show') + ' available fields';

    const format = textFormatSelect.value;

    switch (format) {
      case 'csv':
        textInputFormat.textContent = 'lines of comma-separated values';
        break;
      case 'keyval':
        textInputFormat.textContent = '"field=value" lines';
        break;
      case 'tsv':
        textInputFormat.textContent = 'lines of tab-separated values';
        break;
    }

    const rec = source === 'recordings';

    show($('form-text-format-label'), !rec);
    show(textFormatSelect, !rec);
    show($('form-text-type-label'), !rec);
    show(textTypeSelect, !rec);
    show($('form-text-set-row'), format !== 'keyval' || rec);
    show($('form-text-fields-row'), format !== 'keyval' && !rec);
    show($('form-text-input-row'), !rec);
    show($('form-text-release-label'), rec);
    show(textReleaseInput, rec);

    textSetExample.title = formSetExamples[type] || '';
    textFieldsExample.title = formFieldsExamples[type] || '';
    textInputExample.title = (formInputExamples[type] || {})[format] || '';

    // Save the current state to localStorage, deliberately omitting some
    // one-off settings like "extract track artists" and "remove parens".
    if (save) {
      localStorage.setItem(
        uiStateKey,
        JSON.stringify({
          source: sourceSelect.value,
          textFormat: textFormatSelect.value,
          textType: textTypeSelect.value,
          onlinePunctuation: onlinePunctuationCheckbox.checked
            ? '1'
            : undefined,
          beta: betaCheckbox.checked ? '1' : undefined,
        })
      );
    }
    updatePermalink();
    updateGenButton();
  }

  // Updates the permalink's URL to reflect the UI's current state.
  function updatePermalink() {
    // These property names need to match the ones in updateUI() and
    // restoreUI().
    const state: Record<string, string> = { source: sourceSelect.value };

    // Only include state relevant to the selected source.
    const save = (k: string, v: string) => v !== '' && (state[k] = v);
    if (sourceSelect.value === 'online') {
      if (onlineExtractCheckbox.checked) state.onlineExtractArtists = '1';
      if (onlineSplitCheckbox.checked) state.onlineSplitArtists = '1';
      if (onlineParensCheckbox.checked) state.onlineRemoveParens = '1';
      if (onlinePunctuationCheckbox.checked) state.onlinePunctuation = '1';
      save('onlineCountry', onlineCountryInput.value);
    } else if (sourceSelect.value === 'text') {
      state.textFormat = textFormatSelect.value;
      state.textType = textTypeSelect.value;
      save('textSet', textSetTextarea.value);
      save('textFields', textFieldsInput.value);
    }

    permalink.href = '#' + new URLSearchParams(state).toString();
  }

  // Restores the state of the form UI from localStorage and the URL hash.
  function restoreUI() {
    const state: Record<string, string> = { textType: 'recording' };
    try {
      Object.assign(
        state,
        JSON.parse(localStorage.getItem(uiStateKey) || '{}')
      );
    } catch (e) {
      console.error(`Failed loading UI state from localStorage: ${e}`);
    }

    // Layer the state from the URL hash on top of localStorage.
    const hash = window.location.hash.slice(1); // remove '#'
    Object.assign(state, Object.fromEntries([...new URLSearchParams(hash)]));

    // Remove the hash from the URL: I don't want to display a stale hash after
    // the user modifies the fields, but I also think it'll be distracting and
    // annoying if the hash is dynamically updated as the user types. Setting
    // window.location.hash to an empty string leaves '#' hanging around, so use
    // the history API instead.
    history.replaceState(
      '',
      document.title,
      window.location.pathname + window.location.search
    );

    const setSelect = (sel: HTMLSelectElement, val: string | undefined) => {
      if (val === undefined) return;
      sel.value = val;
      if (sel.selectedIndex < 0) sel.selectedIndex = 0;
    };
    setSelect(sourceSelect, state.source);
    setSelect(textFormatSelect, state.textFormat);
    setSelect(textTypeSelect, state.textType);

    const setText = (
      el: HTMLTextAreaElement | HTMLInputElement,
      val: string | undefined
    ) => val !== undefined && (el.value = val);
    setText(onlineCountryInput, state.onlineCountry);
    setText(textSetTextarea, state.textSet);
    setText(textFieldsInput, state.textFields);

    onlineExtractCheckbox.checked = state.onlineExtractArtists === '1';
    onlineSplitCheckbox.checked = state.onlineSplitArtists === '1';
    onlineParensCheckbox.checked = state.onlineRemoveParens === '1';
    onlinePunctuationCheckbox.checked = state.onlinePunctuation === '1';
    betaCheckbox.checked = state.beta === '1';

    updateUI(false /* save */);

    // Autofocus the URL input to make it easy to paste URLs.
    // I'm doing this from JS since the 'autofocus' attribute doesn't work reliably
    // (possibly since the input may not be visible when the page is first loaded).
    if (sourceSelect.value === 'online') onlineUrlInput.focus();
  }

  // Updates |genButton|'s disabled state based on whether input has been provided.
  function updateGenButton() {
    const src = sourceSelect.value;
    genButton.disabled =
      (src === 'online' && onlineUrlInput.value.trim() === '') ||
      (src === 'text' && textInputTextarea.value.trim() === '') ||
      (src === 'recordings' &&
        (textReleaseInput.value.trim() === '' ||
          textSetTextarea.value.trim() === ''));
  }

  // Clears form fields. Select elements are left unchanged.
  function clearFields() {
    onlineUrlInput.value = '';
    onlineMbidInput.value = '';
    onlineCountryInput.value = '';
    onlineExtractCheckbox.checked = false;
    onlineSplitCheckbox.checked = false;
    onlineParensCheckbox.checked = false;
    onlinePunctuationCheckbox.checked = false;
    textReleaseInput.value = '';
    textFieldsInput.value = '';
    textSetTextarea.value = '';
    setTextInputTextareaValue('');
    updatePermalink();
    updateGenButton();
  }

  // Sets |textInputFileInput|'s value.
  function setTextInputTextareaValue(value: string) {
    textInputTextarea.value = value;
    // Setting the value doesn't seem to trigger an input event
    // (at least on Chrome 106), so dispatch one manually.
    textInputTextarea.dispatchEvent(new Event('input'));
  }

  // Displays an error message.
  function showError(msg: string) {
    // If the message ends in a URL, linkify it.
    // This regular expression is deliberately restrictive: in general, we only
    // care about linkifying URLs of other seeders recommended by
    // sources/online/online.go, and we should limit the potential harm if
    // arbitrary text is injected into error messages returned by yambsd.
    const re = /(?<=^Unsupported URL; try )https?:\/\/[-a-zA-Z0-9.\/]+$/;
    msg = msg.trim();
    const match = re.exec(msg);
    if (match) {
      errorDiv.textContent = msg.slice(0, match.index);
      const url = match[0];
      const link = createElement('a', errorDiv, null, url) as HTMLLinkElement;
      link.href = url;
      link.target = '_blank';
    } else {
      errorDiv.textContent = msg;
    }
    show(errorDiv);
    window.setTimeout(() => errorDiv.scrollIntoView(false /* alignToTop */));
  }
  // Hides an error message.
  function hideError() {
    hide(errorDiv);
  }

  // Sends form data to the server and returns a promise for an array of Edit
  // objects that can be passed to editManager.showEdits(). Throws an error if a
  // problem occurs.
  async function generateEdits() {
    const body = new FormData();
    if (betaCheckbox.checked) body.set('beta', '1');

    const addSetCommands = () => {
      // Trim whitespace at the beginning of lines and then drop empty lines.
      // Also unescape backslash sequences so it's possible to supply
      // multiline edit notes.
      textSetTextarea.value
        .split('\n')
        .map((v) => v.trimStart())
        .filter(Boolean)
        .map((v) =>
          v.replace(/\\./g, (m) =>
            m[1] === 'n' ? '\n' : m[1] === 't' ? '\t' : m[1]
          )
        )
        .forEach((v) => body.append('set', v));
    };

    switch (sourceSelect.value) {
      case 'text':
        body.set('source', 'text');
        body.set('type', textTypeSelect.value);

        const format = textFormatSelect.value;
        body.set('format', format);

        // Set commands and field lists aren't used for the keyval format.
        if (format !== 'keyval') {
          addSetCommands();
          const fields = textFieldsInput.value.trim();
          if (fields !== '')
            fields.split(',').forEach((v) => body.append('field', v.trim()));
        }

        // Drop input lines that are empty or only contain whitespace,
        // but preserve whitespace at the beginning or ends of lines.
        body.set(
          'input',
          textInputTextarea.value
            .split('\n')
            .filter((v) => v.trim() !== '')
            .join('\n')
        );

        break;

      case 'online':
        body.set('source', 'online');
        body.set('url', onlineUrlInput.value.trim());
        const mbid = onlineMbidInput.value.trim().toLowerCase();
        if (mbid !== '') body.append('set', `mbid=${mbid}`);
        body.set('country', onlineCountryInput.value.trim().toUpperCase());
        if (onlineExtractCheckbox.checked) body.set('extractTrackArtists', '1');
        if (onlineSplitCheckbox.checked) body.set('splitArtists', '1');
        if (onlineParensCheckbox.checked) body.set('removeParens', '1');
        if (onlinePunctuationCheckbox.checked) body.set('punctuation', '1');
        break;

      case 'recordings':
        body.set('source', 'recordings');
        body.append('release', textReleaseInput.value.trim());
        addSetCommands();
        break;
    }

    const res = await fetch('edits', { method: 'post', body });
    if (!res.ok) throw new Error(await res.text());
    for await (const msg of streamObjects(res.body!)) {
      if (msg.log) appendLogMessage(msg.time, msg.log);
      if (msg.error) throw new Error(msg.error);
      if (msg.edits) return msg.edits;
    }
  }

  // Clears |logContainer|.
  function clearLog() {
    logContainer.textContent = '';
    hide(logSection);
  }

  // Appends |text| to |logContainer|.
  function appendLogMessage(ts: string, text: string) {
    const time = new Date(Date.parse(ts)).toTimeString().slice(0, 8);
    createElement('div', logContainer, 'time', time);
    createElement('div', logContainer, 'text', text);
    show(logSection);
  }

  // Initialize the form.
  restoreUI();

  // updateUI() updates form elements and also saves the current state
  // to localStorage and updates the permalink.
  sourceSelect.addEventListener('change', () => updateUI());
  textFormatSelect.addEventListener('change', () => updateUI());
  textTypeSelect.addEventListener('change', () => updateUI());
  onlinePunctuationCheckbox.addEventListener('input', () => updateUI());
  betaCheckbox.addEventListener('input', () => updateUI());

  // For non-persistent elements, just update the permalink.
  // It'd be nicer to not update the permalink on each keystroke,
  // but 'change' only fires on blur, so we might end up with a stale URL
  // if the user types and then immediately moves the pointer to the link.
  onlineCountryInput.addEventListener('input', updatePermalink);
  onlineExtractCheckbox.addEventListener('input', updatePermalink);
  onlineSplitCheckbox.addEventListener('input', updatePermalink);
  onlineParensCheckbox.addEventListener('input', updatePermalink);
  textSetTextarea.addEventListener('input', updatePermalink);
  textFieldsInput.addEventListener('input', updatePermalink);

  onlineUrlInput.addEventListener('input', updateGenButton);
  textReleaseInput.addEventListener('input', updateGenButton);
  textSetTextarea.addEventListener('input', updateGenButton);
  textInputTextarea.addEventListener('input', updateGenButton);

  textToggleFieldsButton.addEventListener('click', () => {
    textFieldsTableShown = !textFieldsTableShown;
    updateUI();
  });

  // Only show the file button when the input textarea is empty.
  textInputTextarea.addEventListener('input', () => {
    const empty = !textInputTextarea.value.length;
    textInputFileButton.classList.toggle('hidden', !empty);
  });

  // Make the button trigger the hidden file input.
  textInputFileButton.addEventListener('click', () => {
    textInputFileInput.click();
  });
  textInputFileInput.addEventListener('input', () => {
    const files = textInputFileInput.files;
    if (!files || !files.length) return;
    const fr = new FileReader();
    fr.addEventListener('load', () => {
      const text = fr.result as String;
      setTextInputTextareaValue(text.replace('\n+$', ''));
      // Clear the file input so its 'input' event will fire again
      // if the same file is selected later.
      textInputFileInput.value = '';
    });
    fr.readAsText(files[0]);
  });

  // Let the user submit URLs by pressing Enter.
  for (const el of [
    onlineUrlInput,
    onlineMbidInput,
    onlineCountryInput,
    onlineExtractCheckbox,
    onlineSplitCheckbox,
    onlineParensCheckbox,
    onlinePunctuationCheckbox,
    betaCheckbox,
  ]) {
    el.addEventListener('keydown', (e) => {
      if (e.key === 'Enter') {
        genButton.dispatchEvent(
          new MouseEvent('click', { ctrlKey: e.ctrlKey })
        );
        el.blur();
      }
    });
  }

  genButton.addEventListener('click', (ev) => {
    const origText = genButton.textContent;
    genButton.disabled = true;
    genButton.textContent = 'Generating…';
    editManager.showEdits([]);
    hideError();
    clearLog();

    generateEdits()
      .then((edits) => {
        console.log(`Got ${edits.length} edit(s)`);
        // This is done in finally(), but also do it here in case we end up
        // automatically opening the edits from editManager.showEdits(), since
        // I've sometimes seen the button still be disabled when I navigate back
        // to the form.
        genButton.disabled = false;
        genButton.textContent = origText;
        editManager.showEdits(edits, ev.ctrlKey /* autoOpen */);
      })
      .catch((err) => {
        showError(err.message);
      })
      .finally(() => {
        genButton.disabled = false;
        genButton.textContent = origText;
      });
  });

  clearButton.addEventListener('click', () => {
    clearFields();
    hideError();
    clearLog();
    editManager.showEdits([]);

    // Make it easy to paste another URL.
    if (sourceSelect.value === 'online') onlineUrlInput.focus();
  });
})();

// Returns an AsyncIterator for reading newline-terminated JSON objects from
// |stream|.
const streamObjects = (stream: ReadableStream) => ({
  [Symbol.asyncIterator]() {
    const reader = stream.getReader();
    const decoder = new TextDecoder('utf-8', { fatal: true });
    let finished = false; // fed everything from |reader| to |decoder|
    const objects: object[] = []; // buffered parsed objects
    let text = ''; // buffered decoded text

    return {
      async next(): Promise<{ value?: any; done?: boolean }> {
        // If we already have a previously-read object, return it.
        if (objects.length) return { value: objects.shift(), done: false };

        while (true) {
          // Read the next chunk of bytes from the stream.
          const { value, done } = await reader.read();
          if (done) {
            // We're using TextDecoder in streaming mode to handle character
            // sequences that are split over two chunks, so we need to call
            // decode() one last time after we've read the final chunk.
            if (finished) return { done: true };
            text += decoder.decode(value, { stream: false });
            finished = true;
          } else {
            text += decoder.decode(value, { stream: true });
          }

          // The last line is empty if the chunk was newline-terminated
          // or non-empty if the last object is incomplete.
          const lines = text.split('\n');
          text = lines.pop()!;
          if (!lines.length) continue;

          objects.push(...lines.map((ln) => JSON.parse(ln)));
          return { value: objects.shift(), done: false };
        }
      },
    };
  },
});
