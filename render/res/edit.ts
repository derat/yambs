// Copyright 2024 Daniel Erat.
// All rights reserved.

const NUM_EDITS_TO_PRESELECT = 10;

class EditManager {
  #section = $('edit-section');
  #headerCheckbox = $input('edit-header-checkbox');
  #serialNote = $('edit-serial-note');
  #openAllButton = $button('edit-open-all-button');
  #openSelButton = $button('edit-open-selected-button');
  #table = $('edit-table');
  #checkboxes: HTMLInputElement[] = [];
  #forms: (HTMLFormElement | null)[] = []; // null for GETs
  #links: HTMLLinkElement[] = [];
  #lastClickIndex = -1; // index of last-clicked checkbox
  #serial = false; // edits can't be opened in parallel

  constructor() {
    this.#headerCheckbox.addEventListener('click', () => {
      this.#lastClickIndex = -1;
      const empty = this.#numSelected === 0;
      this.#checkboxes.forEach((cb) => (cb.checked = empty));
      this.#updateUI();
    });
    this.#openSelButton.addEventListener('click', () => {
      this.#links
        .filter((_, i) => this.#checkboxes[i].checked)
        .forEach((a) => a.click());
      this.#advanceSelection();
    });
    this.#openAllButton.addEventListener('click', () => {
      for (const a of this.#links) a.click();
    });

    // If there are one or two edits, automatically open them. Doing this for two edits
    // is a hack to handle the case where the second one is a cover image.
    const serial = initialEdits.some((e) => e.serial);
    const numEdits = initialEdits.length;
    const autoOpen = numEdits === 1 || (!serial && numEdits === 2);
    this.showEdits(initialEdits, autoOpen); // calls #updateUI()
    if (autoOpen) show($('edit-opening-overlay'));
  }

  // Returns a 2-element array with the starting and ending index of the selection range.
  // If there isn't a single range, null is returned.
  get #selectionRange(): [number, number] | null {
    let start = -1;
    let end = -1;
    for (const [idx, cb] of this.#checkboxes.entries()) {
      if (!cb.checked) continue;
      if (start < 0) start = end = idx;
      else if (end === idx - 1) end = idx;
      else return null; // not a continuous range
    }
    return start < 0 ? null : [start, end];
  }

  // Returns the number of selected edit rows.
  get #numSelected(): number {
    return this.#checkboxes.filter((cb) => cb.checked).length;
  }

  // If a continuous range of n rows is selected, advances the selection to the next n rows.
  #advanceSelection() {
    const range = this.#selectionRange;
    if (!range || range[1] === this.#links.length - 1) return;

    const start = range[1] + 1;
    const end = start + (range[1] - range[0]);
    this.#checkboxes.forEach(
      (cb, idx) => (cb.checked = idx >= start && idx <= end)
    );
    this.#updateUI();
  }

  // Updates edit header checkbox state and buttons for the currently-checked checkboxes.
  #updateUI() {
    this.#openAllButton.disabled = this.#serial || this.#links.length === 0;

    // Update the "Open selected" button's text and disabled state.
    const range = this.#selectionRange;
    this.#openSelButton.textContent =
      range && range[1] < this.#links.length - 1
        ? 'Open selected and advance'
        : 'Open selected';
    this.#openSelButton.disabled = this.#numSelected === 0;

    // Make the header checkbox checked if any rows are selected, and translucent if only some of
    // the rows are selected.
    const count = this.#numSelected;
    this.#headerCheckbox.checked = count > 0;
    this.#headerCheckbox.classList.toggle(
      'partial',
      count > 0 && count < this.#checkboxes.length
    );
    this.#headerCheckbox.disabled = this.#serial;
  }

  // Displays the supplied array of objects describing edits and calls #updateUI().
  //
  // {
  //   desc: 'Human-readable description',
  //   url: 'https://www.example.org',          // includes params if GET is okay
  //   params: [{name: 'k', value: 'v1'}, ...], // if non-empty, POST is needed
  // }
  //
  // If |autoOpen| is true, the edits will be automatically opened, replacing the current window.
  showEdits(edits: Edit[], autoOpen = false) {
    const tbody = this.#table.querySelector('tbody')!;
    while (tbody.firstChild) tbody.removeChild(tbody.lastChild!);
    this.#checkboxes.length = 0;
    this.#forms.length = 0;
    this.#links.length = 0;
    this.#lastClickIndex = -1;
    this.#serial = edits.some((e) => e.serial);

    // Make sure that we don't open multiple edits if at least one of them needs
    // to be created serially.
    if (this.#serial && edits.length > 1) autoOpen = false;

    if (edits.length) {
      show(this.#section);
      window.setTimeout(() => {
        this.#section.scrollIntoView(false /* alignToTop */);
        if (this.#serial) this.#openSelButton.focus();
        else this.#openAllButton.focus();
      });
    } else {
      hide(this.#section);
    }

    // Add a row for each edit.
    for (const [idx, edit] of edits.entries()) {
      const tr = createElement('tr', tbody);

      // Add a column containing a checkbox.
      const td1 = createElement('td', tr);
      const cb = createElement('input', td1) as HTMLInputElement;
      this.#checkboxes.push(cb);
      cb.type = 'checkbox';
      cb.addEventListener('click', (e) => {
        if (this.#serial) {
          // If edits need to be done one at a time, uncheck any other checkboxes.
          this.#checkboxes.forEach((cb, i) => {
            if (i !== idx) cb.checked = false;
          });
        } else {
          // On shift-click, update the range starting at the last-clicked checkbox.
          if (
            e.shiftKey &&
            this.#lastClickIndex >= 0 &&
            this.#lastClickIndex != idx
          ) {
            const checked = cb.checked;
            const start = Math.min(this.#lastClickIndex, idx);
            const end = Math.max(this.#lastClickIndex, idx);
            for (let i = start; i <= end; i++)
              this.#checkboxes[i].checked = checked;
          }
        }
        this.#lastClickIndex = idx;
        this.#updateUI();
      });
      cb.checked = (!this.#serial && idx < NUM_EDITS_TO_PRESELECT) || idx === 0;

      // If we're going to automatically open the edits and this is the first one,
      // replace the current page instead of opening a new tab.
      const target = autoOpen && idx === 0 ? '_self' : '_blank';

      // Add a second column containing a link.
      // If this edit requires a POST, also add a form.
      const td2 = createElement('td', tr);
      if (edit.params && edit.params.length) {
        const form = createElement('form', td2) as HTMLFormElement;
        this.#forms.push(form);
        form.action = edit.url;
        form.method = 'post';
        form.target = target;
        for (const p of edit.params) {
          const input = createElement('input', form) as HTMLInputElement;
          input.type = 'hidden';
          input.name = p.name;
          input.value = p.value;
        }
      } else {
        this.#forms.push(null);
      }

      const link = createElement('a', td2, null, edit.desc) as HTMLLinkElement;
      this.#links.push(link);
      if (!edit.params || !edit.params.length) link.href = edit.url;
      link.target = target;
      link.addEventListener('click', (e) => {
        // If there's a form (because this edit requires a POST), submit it.
        // Otherwise, just let the link perform its default action.
        const f = this.#forms[idx];
        if (f) {
          f.submit();
          e.preventDefault();
        }
      });
    }

    // Display a message if edits must be performed serially.
    show(this.#serialNote, this.#serial);

    // Automatically open the links if requested.
    if (autoOpen) for (const link of this.#links) link.click();

    this.#updateUI();
  }
}

// Create a global instance so form.ts can call showEdits().
const editManager = new EditManager();
