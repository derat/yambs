// Copyright 2024 Daniel Erat.
// All rights reserved.

package cmdutil

import (
	"context"
	"fmt"
	"math"
	"reflect"
	"sort"
	"strconv"
	"testing"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"github.com/google/go-cmp/cmp"
)

func TestMergeReleaseRecordings(t *testing.T) {
	const (
		mbid1 = "f88b55b2-0617-4abe-95c0-517ec664c5ae"
		mbid2 = "2a4f4294-c7d9-461c-9de7-51a5212ab8c3"
		mbid3 = "6116c425-77ff-4fbb-821b-920576605ffb"
		mbid4 = "5efe58c0-492b-494f-b56e-6e537896db0f"
		mbid5 = "29bfd027-bd0d-49da-95ee-c4f6c080dd27"
	)

	mkmed := func(recIDs ...int32) mbdb.DBMedium {
		var med mbdb.DBMedium
		for _, id := range recIDs {
			med.Tracks = append(med.Tracks, mbdb.DBTrack{
				Recording: mbdb.DBRecording{
					Name: fmt.Sprint(id),
					ID:   id,
				},
			})
		}
		return med
	}
	db := mbdb.NewDB(mbdb.DisallowQueries)
	db.SetDBReleaseForTest(mbid1, mbdb.DBRelease{Mediums: []mbdb.DBMedium{mkmed(10, 20, 30)}})
	db.SetDBReleaseForTest(mbid2, mbdb.DBRelease{Mediums: []mbdb.DBMedium{mkmed(10, 21), mkmed(31)}})
	db.SetDBReleaseForTest(mbid3, mbdb.DBRelease{Mediums: []mbdb.DBMedium{mkmed(10, 20, 32)}})
	db.SetDBReleaseForTest(mbid4, mbdb.DBRelease{Mediums: []mbdb.DBMedium{mkmed(10, 20)}})
	db.SetDBReleaseForTest(mbid5, mbdb.DBRelease{Mediums: []mbdb.DBMedium{mkmed(10, 20, 30)}})

	for _, tc := range []struct {
		desc   string
		mbids  []string
		ranges []string
		want   [][]int32 // IDs in merges, or nil for error
	}{
		// Success:
		{"same lengths", []string{mbid1, mbid2, mbid3, mbid5}, nil,
			[][]int32{{20, 21}, {30, 31, 32}}},
		{"different lengths with ranges", []string{mbid1, mbid4}, []string{"3,1", "1-2"},
			[][]int32{{30, 10}, {10, 20}}},

		// Failure:
		{"same release", []string{mbid1, mbid1}, nil, nil},
		{"no-op merge", []string{mbid1, mbid5}, nil, nil},
		{"different track counts", []string{mbid1, mbid4}, nil, nil},
		{"unparsable range", []string{mbid1, mbid2}, []string{"-3", "1-3"}, nil},
		{"invalid ranges", []string{mbid1, mbid2}, []string{"3-1", "1-3"}, nil},
		{"too few ranges", []string{mbid1, mbid2}, []string{"1-3"}, nil},
		{"too many ranges", []string{mbid1, mbid2}, []string{"1-3", "1-3", "1-3"}, nil},
		{"different range sizes", []string{mbid1, mbid2}, []string{"1-3", "1-2"}, nil},
		{"too-large ranges", []string{mbid1, mbid2}, []string{"1-4", "1-4"}, nil},
		{"huge ranges", []string{mbid1, mbid2},
			[]string{"1-" + strconv.Itoa(math.MaxInt), "1-" + strconv.Itoa(math.MaxInt)}, nil},
	} {
		t.Run(tc.desc, func(t *testing.T) {
			edits, err := MergeReleaseRecordings(context.Background(), db, tc.mbids, tc.ranges)
			var got [][]int32
			for i, ed := range edits {
				if mg, ok := ed.(*seed.Merge); !ok {
					t.Errorf("Edit %v is of non-merge type %t", i, ed)
				} else if et := mg.EntityForTest(); et != seed.RecordingEntity {
					t.Errorf("Edit %v is of non-recording entity %v", i, et)
				} else {
					got = append(got, mg.IDsForTest())
				}
			}
			diff := cmp.Diff(tc.want, got)
			switch {
			case tc.want == nil && err == nil:
				t.Error("MergeReleaseRecordings unexpectedly succeeded:\n" + diff)
			case tc.want != nil && err != nil:
				t.Error("MergeReleaseRecordings failed:", err)
			case tc.want != nil && diff != "":
				t.Error("MergeReleaseRecordings returned incorrect edits:\n" + diff)
			}
		})
	}
}

func TestParseMergeRanges(t *testing.T) {
	for _, tc := range []struct {
		in   string
		want mergeRanges // nil for error
	}{
		{"1", mergeRanges{{1, 1}}},
		{"1,2", mergeRanges{{1, 1}, {2, 2}}},
		{"1-2,5", mergeRanges{{1, 2}, {5, 5}}},
		{"1-2,5-10", mergeRanges{{1, 2}, {5, 10}}},
		{"1-2,5-5", mergeRanges{{1, 2}, {5, 5}}},
		{"5-10,1-4,11", mergeRanges{{5, 10}, {1, 4}, {11, 11}}},
		{"", nil},
		{"foo", nil},
		{"1-a", nil},
		{"0-5", nil},
		{"-1-5", nil},
		{"4-3", nil},
		{"1-5,5-10", nil},
		{"1-5,4-10", nil},
		{"4-10,5-6", nil},
	} {
		got, err := parseMergeRanges(tc.in)
		if err != nil {
			if got != nil {
				t.Errorf("parseMergeRanges(%q) failed: %v", tc.in, err)
			}
		} else if got == nil {
			t.Errorf("parseMergeRanges(%q) returned %v; want error", tc.in, got)
		} else if !reflect.DeepEqual(got, tc.want) {
			t.Errorf("parseMergeRanges(%q) returned %v; want %v", tc.in, got, tc.want)
		}
	}
}

func FuzzParseMergeRanges(f *testing.F) {
	f.Add("1")
	f.Add("1-5")
	f.Add("1,2-4,10")
	f.Add("5,20-42,50-999")
	f.Fuzz(func(t *testing.T, in string) {
		ranges, err := parseMergeRanges(in)
		if err != nil {
			if ranges != nil {
				t.Errorf("Got non-nil ranges %v but also got error %v", ranges, err)
			}
			return
		}
		// I'm just repeating checks from parseMergeRanges here. :-/
		for _, r := range ranges {
			if !r.valid() {
				t.Errorf("Got invalid range %v", r)
			}
		}
		sort.Slice(ranges, func(i, j int) bool { return ranges[i].first < ranges[j].first })
		for i := 0; i < len(ranges)-1; i++ {
			if a, b := ranges[i], ranges[i+1]; a.last >= b.first {
				t.Errorf("Got overlapping ranges %v and %v", a, b)
			}
		}
	})
}
