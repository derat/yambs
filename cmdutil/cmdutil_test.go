// Copyright 2025 Daniel Erat.
// All rights reserved.

package cmdutil

import (
	"testing"
)

func TestAddSchemeToServer(t *testing.T) {
	for _, tc := range []struct{ server, want string }{
		{"example.org", "https://example.org"},
		{"http://example.org", "http://example.org"},
		{"https://example.org", "https://example.org"},
	} {
		if got := AddSchemeToServer(tc.server); got != tc.want {
			t.Errorf("AddSchemeToServer(%q) = %q; want %q", tc.server, got, tc.want)
		}
	}
}
