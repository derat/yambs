// Copyright 2024 Daniel Erat.
// All rights reserved.

// Package cmdutil contains logic shared by executables under the cmd directory.
package cmdutil

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"strings"

	"codeberg.org/derat/yambs/mbdb"
)

// AddSchemeToServer prepends "https://" to server if it doesn't appear to already contain a scheme.
func AddSchemeToServer(server string) string {
	if strings.Contains(server, "://") {
		return server
	}
	return "https://" + server
}

// GetReleaseRecordingsInput fetches the release specified by mbidOrURL and
// returns an io.Reader containing the release's recordings, one MBID per line.
func GetReleaseRecordingsInput(ctx context.Context, db *mbdb.DB, mbidOrURL string) (io.Reader, error) {
	mbid := mbdb.ExtractMBID(mbidOrURL)
	if !mbdb.IsMBID(mbid) {
		return nil, fmt.Errorf("failed extracting release MBID")
	}
	// Fetch the release and read its recording MBIDs as input.
	rel, err := db.GetRelease(ctx, mbid)
	if err != nil {
		return nil, err
	}
	var b bytes.Buffer
	for _, m := range rel.Media {
		for _, tr := range m.Tracks {
			b.WriteString(tr.Recording.ID + "\n")
		}
	}
	return &b, nil
}
