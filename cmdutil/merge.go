// Copyright 2024 Daniel Erat.
// All rights reserved.

package cmdutil

import (
	"context"
	"errors"
	"fmt"
	"sort"
	"strconv"
	"strings"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/strutil"
)

// maxMergeNameLen is the maximum number of characters to include from an
// entity's name in the merge edit description.
const maxMergeNameLen = 32

// MergeReleaseRecordings generates seed.MergeEdit objects to merge the recordings in the same
// tracklist positions in the specified releases.
func MergeReleaseRecordings(ctx context.Context, db *mbdb.DB,
	mbidsOrURLs []string, rangeDescs []string) ([]seed.Edit, error) {
	mbids := make([]string, 0, len(mbidsOrURLs))
	for _, in := range mbidsOrURLs {
		mbid := mbdb.ExtractMBID(in)
		if !mbdb.IsMBID(mbid) {
			return nil, fmt.Errorf("didn't find MBID in %q", in)
		}
		mbids = append(mbids, mbid)
	}
	if len(mbids) < 2 {
		return nil, errors.New("need at least two releases")
	}

	// If ranges were supplied, parse them now.
	var ranges []mergeRanges
	if rangeDescs != nil {
		if len(rangeDescs) != len(mbids) {
			return nil, fmt.Errorf("got %v releases but %v range description(s)",
				len(mbids), len(rangeDescs))
		}
		var firstSize int
		for i, desc := range rangeDescs {
			mr, err := parseMergeRanges(desc)
			if err != nil {
				return nil, fmt.Errorf("%q: %w", desc, err)
			}
			if i == 0 {
				firstSize = mr.size()
			} else if size := mr.size(); size != firstSize {
				return nil, fmt.Errorf("range %q has size %v but %q has size %v",
					rangeDescs[0], firstSize, desc, size)
			}
			ranges = append(ranges, mr)
		}
	}

	// Fetch all of the releases so we can get their database row IDs.
	rels := make([]mbdb.DBRelease, 0, len(mbids))
	for _, mbid := range mbids {
		rel, err := db.GetDBRelease(ctx, mbid)
		if err != nil {
			return nil, fmt.Errorf("%v: %w", mbid, err)
		}
		rels = append(rels, rel)
	}

	if len(ranges) == 0 {
		// If we don't already have ranges, generate them from the releases.
		firstTracks := rels[0].NumTracks()
		for i, rel := range rels[1:] {
			if n := rel.NumTracks(); n != firstTracks {
				return nil, fmt.Errorf("releases %v and %v have different track counts (%v vs. %v)",
					mbids[0], mbids[i+1], firstTracks, n)
			}
		}
		for range rels {
			ranges = append(ranges, mergeRanges{{1, firstTracks}})
		}
	} else {
		// If ranges were supplied, check that none extends beyond the total number of tracks on the
		// corresponding release.
		for i, rel := range rels {
			if max, tracks := ranges[i].max(), rel.NumTracks(); max > tracks {
				return nil, fmt.Errorf("release %v has %v track(s) but range uses track %v",
					mbids[i], max, tracks)
			}
		}
	}

	// Generate a merge edit for each set of tracks.
	numTracks := ranges[0].size()
	edits := make([]seed.Edit, 0, numTracks)
	for i := 0; i < numTracks; i++ {
		names := make([]string, 0, len(rels))
		recIDs := make([]int32, 0, len(rels))
		seen := make(map[int32]struct{})
		for j, rel := range rels {
			n := ranges[j].item(i)
			if n < 1 {
				return nil, fmt.Errorf("failed getting index %v from ranges %v", i, ranges[j])
			}
			tr := rel.GetTrackAtIndex(n - 1)
			if tr == nil {
				return nil, fmt.Errorf("failed getting track %v from %v", n, mbids[j])
			}
			// Skip duplicates (i.e. when some releases already share the same recordings).
			if _, ok := seen[tr.Recording.ID]; !ok {
				recIDs = append(recIDs, tr.Recording.ID)
				seen[tr.Recording.ID] = struct{}{}
				names = append(names, strutil.Truncate(tr.Recording.Name, maxMergeNameLen, true))
			}
		}
		if len(recIDs) < 2 {
			continue
		}
		edit, err := seed.NewMerge(seed.RecordingEntity, recIDs, strings.Join(names, " / "))
		if err != nil {
			return nil, err
		}
		edits = append(edits, edit)
	}
	if len(edits) == 0 {
		return nil, errors.New("releases already share the same recordings")
	}
	return edits, nil
}

// mergeRanges contains non-overlapping ranges specifying the order for merging entities.
type mergeRanges []mergeRange

// parseMergeRanges parses a 1-indexed specification string like "4-8,3,1-2".
func parseMergeRanges(desc string) (mergeRanges, error) {
	var ranges mergeRanges
	for _, s := range strings.Split(desc, ",") {
		var err error
		var rng mergeRange
		if parts := strings.SplitN(s, "-", 2); len(parts) == 1 {
			if rng.first, err = strconv.Atoi(parts[0]); err != nil {
				return nil, err
			}
			rng.last = rng.first
		} else {
			if rng.first, err = strconv.Atoi(parts[0]); err != nil {
				return nil, err
			}
			if rng.last, err = strconv.Atoi(parts[1]); err != nil {
				return nil, err
			}
		}
		if !rng.valid() {
			return nil, fmt.Errorf("invalid range %v", rng)
		}
		ranges = append(ranges, rng)
	}

	// Check that ranges don't overlap.
	sorted := append([]mergeRange(nil), ranges...)
	sort.Slice(sorted, func(i, j int) bool { return sorted[i].first < sorted[j].first })
	for i := 0; i < len(sorted)-1; i++ {
		if a, b := sorted[i], sorted[i+1]; a.last >= b.first {
			return nil, fmt.Errorf("ranges %v and %v overlap", a, b)
		}
	}

	return ranges, nil
}

// size returns the total number of items spanned by all ranges.
func (mr mergeRanges) size() int {
	var sum int
	for _, r := range mr {
		sum += r.size()
	}
	return sum
}

// max returns the maximum value from all ranges.
func (mr mergeRanges) max() int {
	var v int
	for _, r := range mr {
		if r.last > v {
			v = r.last
		}
	}
	return v
}

// item returns the value at 0-based index i.
func (mr mergeRanges) item(i int) int {
	if i < 0 {
		return -1
	}
	for _, r := range mr {
		if i < r.size() {
			return r.first + i
		}
		i -= r.size()
	}
	return -1
}

// mergeRange describes a single inclusive 1-indexed range.
type mergeRange struct{ first, last int }

func (mr mergeRange) String() string { return fmt.Sprintf("[%v, %v]", mr.first, mr.last) }
func (mr *mergeRange) valid() bool   { return mr.first > 0 && mr.last >= mr.first }
func (mr *mergeRange) size() int     { return mr.last - mr.first + 1 }
