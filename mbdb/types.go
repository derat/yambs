// Copyright 2024 Daniel Erat.
// All rights reserved.

package mbdb

// Release describes an individual release.
type Release struct {
	Media []Medium `json:"media"`
}

// Medium describes a medium within a release (e.g. a single compact disc).
type Medium struct {
	Tracks []Track `json:"tracks"`
}

// Track describes a track (i.e. song) within a medium.
type Track struct {
	Title     string    `json:"title"`
	Recording Recording `json:"recording"`
}

// Recording describes the underlying recording appearing on one or more tracks.
type Recording struct {
	Title string `json:"title"`
	ID    string `json:"id"`
}

// DBRelease describes a release loaded from the internal /ws/js service.
type DBRelease struct {
	Mediums []DBMedium `json:"mediums"`
}

// NumTracks returns the number of tracks in rel across all mediums.
func (rel *DBRelease) NumTracks() int {
	var num int
	for _, med := range rel.Mediums {
		num += len(med.Tracks)
	}
	return num
}

// GetTrackAtIndex returns the 0-based i-th track from rel.
func (rel *DBRelease) GetTrackAtIndex(i int) *DBTrack {
	for _, med := range rel.Mediums {
		if i < 0 {
			return nil
		} else if nt := len(med.Tracks); i < nt {
			return &med.Tracks[i]
		} else {
			i -= nt
		}
	}
	return nil
}

// DBMedium describes a medium loaded from the internal /ws/js service.
type DBMedium struct {
	Tracks []DBTrack `json:"tracks"`
}

// DBTrack describes a track loaded from the internal /ws/js service.
type DBTrack struct {
	ID        int32       `json:"id"`
	Name      string      `json:"name"`
	Length    int64       `json:"length"`
	Recording DBRecording `json:"recording"`
}

// DBRecording describes a recording loaded from the internal /ws/js service.
type DBRecording struct {
	ID     int32  `json:"id"`
	Name   string `json:"name"`
	Length int64  `json:"length"`
}
