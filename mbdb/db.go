// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package mbdb contains functionality related to the MusicBrainz database.
package mbdb

import (
	"context"
	"encoding/json"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	"codeberg.org/derat/yambs/cache"
	"codeberg.org/derat/yambs/clog"
	"codeberg.org/derat/yambs/slog"
	"golang.org/x/time/rate"
)

const (
	// https://musicbrainz.org/doc/MusicBrainz_API/Rate_Limiting
	maxQPS         = 1
	rateBucketSize = 5 // https://community.metabrainz.org/t/api-rate-limit/725714/4
	userAgentFmt   = "yambs/%s ( https://codeberg.org/derat/yambs )"

	cacheSize     = 256         // size for various caches
	cacheKeepTime = time.Hour   // TTL for positive caches
	cacheMissTime = time.Minute // TTL for negative caches

	defaultServerURL = "https://musicbrainz.org"
)

// entityType is an entity type sent to the MusicBrainz API.
type entityType string

const (
	artistType entityType = "artist"
	labelType  entityType = "label"
)

var allEntityTypes = []entityType{
	artistType,
	labelType,
}

// incRelParam contains the "inc" parameter to use when querying URL relationships.
var incRelsParam string

func init() {
	incs := make([]string, len(allEntityTypes))
	for i, t := range allEntityTypes {
		incs[i] = string(t) + "-rels"
	}
	incRelsParam = "inc=" + strings.Join(incs, "+")
}

// DB queries the MusicBrainz database using its API.
// See https://musicbrainz.org/doc/MusicBrainz_API.
type DB struct {
	databaseIDs *cache.LRU // string MBID to int32 database ID
	urlRels     *cache.LRU // string URL to cachedEntityInfos
	urlMiss     *cache.LRU // string URL to time.Time of negative lookup

	pendingMutex       sync.Mutex          // protects pending maps
	pendingCond        *sync.Cond          // signaled when pending maps are changed
	pendingDatabaseIDs map[string]struct{} // MBIDs being looked up
	pendingURLRels     map[string]struct{} // URLs being looked up

	limiter         *rate.Limiter    // rate-limits network requests
	disallowQueries bool             // don't allow network traffic
	serverURL       string           // base server URL without trailing slash
	version         string           // included in User-Agent header
	now             func() time.Time // called to get current time

	dbRelsForTest map[string]DBRelease // keyed by MBID, for unit tests
}

// NewDB returns a new DB object.
func NewDB(opts ...Option) *DB {
	db := DB{
		databaseIDs:        cache.NewLRU(cacheSize),
		urlRels:            cache.NewLRU(cacheSize),
		urlMiss:            cache.NewLRU(cacheSize),
		pendingDatabaseIDs: make(map[string]struct{}),
		pendingURLRels:     make(map[string]struct{}),
		limiter:            rate.NewLimiter(maxQPS, rateBucketSize),
		serverURL:          defaultServerURL,
		now:                time.Now,
		dbRelsForTest:      make(map[string]DBRelease),
	}
	db.pendingCond = sync.NewCond(&db.pendingMutex)
	for _, o := range opts {
		o(&db)
	}
	return &db
}

// Option can be passed to NewDB to configure the database.
type Option func(db *DB)

// DisallowQueries is an Option that configures DB to report an error
// when it would need to perform a query over the network.
var DisallowQueries = func(db *DB) { db.disallowQueries = true }

// ServerURL returns an Option that configure DB to make calls to the specified
// base server URL, e.g. "https://musicbrains.org" or "https://test.musicbrainz.org".
func ServerURL(u string) Option { return func(db *DB) { db.serverURL = u } }

// Version returns an Option that sets the application version for the User-Agent header.
func Version(v string) Option { return func(db *DB) { db.version = v } }

// NowFunc injects a function that is called instead of time.Now to get the current time.
func NowFunc(fn func() time.Time) Option { return func(db *DB) { db.now = fn } }

// MaxQPS overrides the default QPS limit for testing.
func MaxQPS(qps int) Option { return func(db *DB) { db.limiter.SetLimit(rate.Limit(qps)) } }

// GetDatabaseID returns the database ID (e.g. artist.id) corresponding to
// the entity with the specified MBID (e.g. artist.gid).
func (db *DB) GetDatabaseID(ctx context.Context, mbid string) (int32, error) {
	if !IsMBID(mbid) {
		return 0, errors.New("malformed MBID")
	}

	// Check if the ID is already cached.
	// I don't think that MBID-to-ID mappings can change,
	// so we probably don't need to reject old entries.
	if id, ok := db.databaseIDs.Get(mbid); ok {
		return id.(int32), nil
	}

	// Wait if the database ID is already being looked up.
	db.pendingMutex.Lock()
	for db.databaseIDIsPending(mbid) {
		db.pendingCond.Wait()
	}
	db.pendingDatabaseIDs[mbid] = struct{}{}
	db.pendingMutex.Unlock()

	defer func() {
		db.pendingMutex.Lock()
		delete(db.pendingDatabaseIDs, mbid)
		db.pendingCond.Signal()
		db.pendingMutex.Unlock()
	}()

	// Check if it got cached while we were waiting.
	if id, ok := db.databaseIDs.Get(mbid); ok {
		return id.(int32), nil
	}

	// Actually query the database. The /ws/js endpoints apparently exist
	// for field completion rather than being part of the API (/ws/2).
	// See https://wiki.musicbrainz.org/Development/Search_Architecture.
	slog.Info(ctx, "Requesting database ID for ", mbid)
	clog.Log(ctx, "Requesting database ID for ", mbid)
	r, err := db.doQuery(ctx, "/ws/js/entity/"+mbid)
	if err != nil {
		return 0, err
	}
	defer r.Close()

	var data struct {
		ID int32 `json:"id"`
	}
	if err := json.NewDecoder(r).Decode(&data); err != nil {
		return 0, err
	} else if data.ID == 0 {
		return 0, errors.New("server didn't return ID")
	}
	slog.Info(ctx, "Got database ID ", data.ID)
	db.databaseIDs.Set(mbid, data.ID)
	return data.ID, nil
}

// EntityInfo contains high-level information about an entity (e.g. artist or label).
// It sadly doesn't seem possible to request aliases here; /ws/2/url is documented as
// only supporting relationship includes.
type EntityInfo struct {
	// MBID contains the entity's UUID.
	MBID string
	// Name contains the entity's name as it appears in the database.
	Name string
}

// cachedEntityInfos contains a set of EntityInfo objects that have been cached.
type cachedEntityInfos struct {
	// infos contains cached information about different types of entities.
	infos map[entityType][]EntityInfo
	// time contains the time at which the entities were looked up.
	time time.Time
}

// GetArtistsFromURL returns artists related to linkURL.
// If no artist is related to the URL, an empty slice is returned.
func (db *DB) GetArtistsFromURL(ctx context.Context, linkURL string) ([]EntityInfo, error) {
	return db.getURLRels(ctx, linkURL, artistType)
}

// GetLabelsFromURL returns labels related to linkURL.
// If no label is related to the URL, an empty slice is returned.
func (db *DB) GetLabelsFromURL(ctx context.Context, linkURL string) ([]EntityInfo, error) {
	return db.getURLRels(ctx, linkURL, labelType)
}

// getURLRels returns entities of the specified type related to linkURL.
func (db *DB) getURLRels(ctx context.Context, linkURL string, entity entityType) ([]EntityInfo, error) {
	// Check if the relationships are already cached.
	checkCache := func() ([]EntityInfo, bool) {
		if v, ok := db.urlRels.Get(linkURL); ok {
			if cei := v.(cachedEntityInfos); db.now().Sub(cei.time) <= cacheKeepTime {
				slog.Infof(ctx, "Have %d %v relation(s) for %v", len(cei.infos[entity]), entity, linkURL)
				return cei.infos[entity], true
			}
		}
		return nil, false
	}
	if infos, ok := checkCache(); ok {
		return infos, nil
	}

	// If we're being called from a test, just pretend like the URL is missing.
	if db.disallowQueries {
		return nil, nil
	}

	// Wait if the relationships are already being looked up.
	db.pendingMutex.Lock()
	for db.urlRelsArePending(linkURL) {
		db.pendingCond.Wait()
	}
	db.pendingURLRels[linkURL] = struct{}{}
	db.pendingMutex.Unlock()

	defer func() {
		db.pendingMutex.Lock()
		delete(db.pendingURLRels, linkURL)
		db.pendingCond.Signal()
		db.pendingMutex.Unlock()
	}()

	// Check if the relationships were cached while we were waiting.
	if infos, ok := checkCache(); ok {
		return infos, nil
	}

	// Give up if we already checked recently.
	if v, ok := db.urlMiss.Get(linkURL); ok && db.now().Sub(v.(time.Time)) <= cacheMissTime {
		return nil, nil
	}

	slog.Infof(ctx, "Requesting relations for %v", linkURL)
	clog.Logf(ctx, "Requesting relations for %v", linkURL)
	path := fmt.Sprintf("/ws/2/url?resource=%s&%s", url.QueryEscape(linkURL), incRelsParam)
	r, err := db.doQuery(ctx, path)
	if err == notFoundError {
		db.urlMiss.Set(linkURL, db.now())
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	defer r.Close()

	// Parse an XML response like the following:
	//
	//  <?xml version="1.0" encoding="UTF-8"?>
	//  <metadata xmlns="http://musicbrainz.org/ns/mmd-2.0#">
	//    <url id="010fc5d6-2ef6-4852-9075-61184fdc972a">
	//  	<resource>https://pillarsinthesky.bandcamp.com/</resource>
	//  	<relation-list target-type="artist">
	//  	  <relation type="bandcamp" type-id="c550166e-0548-4a18-b1d4-e2ae423a3e88">
	//  		<target>7ba8b326-34ba-472b-b710-b01dc1f14f94</target>
	//  		<direction>backward</direction>
	//  		<artist id="7ba8b326-34ba-472b-b710-b01dc1f14f94" type="Person" type-id="b6e035f4-3ce9-331c-97df-83397230b0df">
	//  		  <name>Pillars in the Sky</name>
	//  		  <sort-name>Pillars in the Sky</sort-name>
	//  		</artist>
	//  	  </relation>
	//  	</relation-list>
	//    </url>
	//  </metadata>
	var md struct {
		XMLName       xml.Name `xml:"metadata"`
		RelationLists []struct {
			TargetType string `xml:"target-type,attr"`
			Relations  []struct {
				Target     string `xml:"target"`
				ArtistName string `xml:"artist>name"`
				LabelName  string `xml:"label>name"`
			} `xml:"relation"`
		} `xml:"url>relation-list"`
	}
	if err := xml.NewDecoder(r).Decode(&md); err != nil {
		return nil, err
	}

	cei := cachedEntityInfos{make(map[entityType][]EntityInfo), db.now()}
	for _, list := range md.RelationLists {
		et := entityType(list.TargetType)
		for _, rel := range list.Relations {
			var name string
			switch entity {
			case artistType:
				name = rel.ArtistName
			case labelType:
				name = rel.LabelName
			}
			cei.infos[et] = append(cei.infos[et], EntityInfo{MBID: rel.Target, Name: name})
		}
	}
	slog.Infof(ctx, "Got %d %v relation(s) for %v", len(cei.infos[entity]), entity, linkURL)
	db.urlRels.Set(linkURL, cei)
	return cei.infos[entity], nil
}

// GetRelease returns information about the release with the supplied MBID.
func (db *DB) GetRelease(ctx context.Context, mbid string) (Release, error) {
	if !IsMBID(mbid) {
		return Release{}, errors.New("invalid MBID")
	}
	slog.Infof(ctx, "Requesting release %v", mbid)
	clog.Logf(ctx, "Requesting release %v", mbid)
	path := "/ws/2/release/" + mbid + "?inc=recordings&fmt=json"
	r, err := db.doQuery(ctx, path)
	if err != nil {
		return Release{}, err
	}
	defer r.Close()
	var rel Release
	err = json.NewDecoder(r).Decode(&rel)
	return rel, nil
}

// GetDBRelease uses the internal, undocumented /ws/js API to fetch information about
// the release with the supplied MBID. The only reason to use this is to get internal
// database row IDs for entities rather than their MBIDs.
func (db *DB) GetDBRelease(ctx context.Context, mbid string) (DBRelease, error) {
	if !IsMBID(mbid) {
		return DBRelease{}, errors.New("invalid MBID")
	}
	if rel, ok := db.dbRelsForTest[mbid]; ok {
		return rel, nil
	}
	slog.Infof(ctx, "Requesting DB release %v", mbid)
	clog.Logf(ctx, "Requesting DB release %v", mbid)
	r, err := db.doQuery(ctx, "/ws/js/release/"+mbid+"?inc=recordings&fmt=json")
	if err != nil {
		return DBRelease{}, err
	}
	var rel DBRelease
	err = json.NewDecoder(r).Decode(&rel)
	return rel, nil

}

// notFoundError is returned by doQuery if a 404 error was received.
var notFoundError = errors.New("not found")

// doQuery sends a GET request for path and returns the response body.
// The caller is responsible for closing the body if the error is non-nil.
func (db *DB) doQuery(ctx context.Context, path string) (io.ReadCloser, error) {
	if db.disallowQueries {
		return nil, errors.New("querying not allowed")
	}

	// Wait until we can perform a query.
	if err := db.limiter.Wait(ctx); err != nil {
		return nil, err
	}

	u := db.serverURL + path
	slog.Info(ctx, "Sending GET request for ", u)
	req, err := http.NewRequestWithContext(ctx, "GET", u, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("User-Agent", fmt.Sprintf(userAgentFmt, db.version))

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != 200 {
		resp.Body.Close()
		if resp.StatusCode == 404 {
			return nil, notFoundError
		}
		return nil, fmt.Errorf("server returned %v: %v", resp.StatusCode, resp.Status)
	}
	return resp.Body, nil
}

// databaseIDIsPending returns true if the supplied MBID's database ID is
// currently being looked up by a call to GetDatabaseID.
func (db *DB) databaseIDIsPending(mbid string) bool {
	_, ok := db.pendingDatabaseIDs[mbid]
	return ok
}

// urlRelsArePending returns true if the supplied URL's relationships are
// currently being looked up by a call to GetArtistsFromURL or GetLabelsFromURL.
func (db *DB) urlRelsArePending(url string) bool {
	_, ok := db.pendingURLRels[url]
	return ok
}

// SetDatabaseIDForTest hardcodes an ID for GetDatabaseID to return.
func (db *DB) SetDatabaseIDForTest(mbid string, id int32) {
	db.databaseIDs.Set(mbid, id)
}

// SetURLEntityInfosForTest hardcodes artists for GetArtistsFromURL and GetLabelsFromURL to return.
func (db *DB) SetURLEntityInfosForTest(url string, artists, labels []EntityInfo) {
	db.urlRels.Set(url, cachedEntityInfos{
		infos: map[entityType][]EntityInfo{artistType: artists, labelType: labels},
		time:  db.now(),
	})
}

func (db *DB) SetDBReleaseForTest(mbid string, rel DBRelease) {
	db.dbRelsForTest[mbid] = rel
}

// MakeEntityInfosForTest is a helper function for tests that creates EntityInfo objects given a
// sequence of MBID and name pairs.
func MakeEntityInfosForTest(mbidNamePairs ...string) []EntityInfo {
	if len(mbidNamePairs)%2 != 0 {
		panic(fmt.Sprintf("Need mbid/name pairs but got %q", mbidNamePairs))
	}
	infos := make([]EntityInfo, len(mbidNamePairs)/2)
	for i := 0; i < len(mbidNamePairs)/2; i++ {
		infos[i].MBID = mbidNamePairs[i*2]
		infos[i].Name = mbidNamePairs[i*2+1]
	}
	return infos
}

// mbidRegexp matches a MusicBrainz ID (i.e. a UUID).
var mbidRegexp = regexp.MustCompile(
	`(?i)^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$`)

// IsMBID returns true if mbid looks like a correctly-formatted MBID (i.e. a UUID).
// Note that this method does not check that the MBID is actually assigned to anything.
func IsMBID(mbid string) bool { return mbidRegexp.MatchString(mbid) }

// ExtractMBID returns the first plausible MBID found after spliting the supplied string on slashes.
// It returns an empty string if no MBID is found. It is safe to call with a bare MBID.
func ExtractMBID(url string) string {
	for _, p := range strings.Split(url, "/") {
		if IsMBID(p) {
			return p
		}
	}
	return ""
}

// ShortenContext returns a context derived from ctx with its deadline shortened by t.
// If ctx does not have a deadline, a derived deadline-less context is returned.
// The caller must call the returned cancel function to release resources.
func ShortenContext(ctx context.Context, t time.Duration) (context.Context, context.CancelFunc) {
	if dl, ok := ctx.Deadline(); ok {
		return context.WithDeadline(ctx, dl.Add(-t))
	}
	return context.WithCancel(ctx)
}
