# This is based on
# https://github.com/GoogleCloudPlatform/golang-samples/blob/main/run/helloworld/Dockerfile.
#
# I'd prefer to just use the automatic buildpack stuff described at
# https://cloud.google.com/run/docs/deploying-source-code, but there doesn't
# seem to be any way to build a package that isn't in the base directory of the
# repo: https://stackoverflow.com/questions/76779958

# Build the server binary.
FROM golang:bookworm as builder
WORKDIR /app
COPY go.* ./
RUN go mod download
COPY . ./
RUN go build -v -o yambsd ./cmd/yambsd

# Create a minimal production container.
FROM debian:bookworm-slim
RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    ca-certificates && \
    rm -rf /var/lib/apt/lists/*
COPY --from=builder /app/yambsd /app/yambsd
CMD ["/app/yambsd"]
