// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package main implements a web server for generating seeded edits.
package main

import (
	"bytes"
	"context"
	_ "embed"
	"flag"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"

	"codeberg.org/derat/yambs/clog"
	"codeberg.org/derat/yambs/cmdutil"
	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/render"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/slog"
	"codeberg.org/derat/yambs/sources/online"
	"codeberg.org/derat/yambs/sources/online/tidal"
	"codeberg.org/derat/yambs/sources/text"
	"codeberg.org/derat/yambs/web"

	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"
	"github.com/tdewolff/minify/v2/html"
	"github.com/tdewolff/minify/v2/js"
	"github.com/tdewolff/minify/v2/svg"
)

const (
	// These limits are arbitrary, but I haven't heard any complaints yet.
	maxReqBytes        = 128 * 1024
	textEditsTimeout   = 10 * time.Second
	onlineEditsTimeout = time.Minute // can take longer due to MB API rate limit
	maxEdits           = 200
	maxFields          = 1000

	editsDelay       = time.Second
	editsRateMapSize = 256

	betaServerURL = "https://beta.musicbrainz.org"
)

var version string

func init() {
	// When deploying to GCP, build/deploy_app.sh passes the version string via an environment variable.
	if v := os.Getenv("APP_VERSION"); v != "" {
		version = v
	}
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %v [flag]...\n"+
			"Runs a web server for seeding MusicBrainz edits.\n\n", os.Args[0])
		flag.PrintDefaults()
	}
	addr := flag.String("addr", "localhost:8999", `Address to listen on for HTTP requests`)
	minifyForm := flag.Bool("minify", true, "Minify HTML pages")
	server := flag.String("server", "musicbrainz.org", "MusicBrainz server hostname")
	flag.Parse()

	if flag.NArg() != 0 {
		flag.Usage()
		os.Exit(2)
	}

	// Just generate the page once.
	form, err := genForm(*minifyForm)
	if err != nil {
		slog.Error(context.Background(), "Failed generating page: ", err)
		os.Exit(1)
	}

	serverURL := cmdutil.AddSchemeToServer(*server)
	db := mbdb.NewDB(mbdb.ServerURL(serverURL), mbdb.Version(version))
	web.SetUserAgent(fmt.Sprintf("yambs/%s (+https://codeberg.org/derat/yambs)", version))
	rm := newRateMap(editsDelay, editsRateMapSize)

	http.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		ctx := slog.RequestContext(req)
		if req.URL.Path != "/" {
			http.NotFound(w, req)
			return
		}
		if req.Method != http.MethodGet {
			http.Error(w, http.StatusText(http.StatusMethodNotAllowed), http.StatusMethodNotAllowed)
			return
		}
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		if _, err := w.Write(form); err != nil {
			slog.Error(ctx, "Failed writing page: ", err)
		}
	})

	// Generate edits requested via the form.
	http.HandleFunc("/edits", func(w http.ResponseWriter, req *http.Request) {
		ctx := slog.RequestContext(req)
		stream := render.NewStream(w, time.Now)
		ctx = clog.LoggerContext(ctx, stream)
		caddr := clientAddr(req)
		infos, err := getEditsForRequest(ctx, w, req, serverURL, rm, db)
		if err != nil {
			msg := "Generating edits failed"
			if uerr, ok := err.(*userError); ok {
				msg = uerr.msg
			}
			slog.Errorf(ctx, "Returning error to %s: %v", caddr, err)
			stream.Error(msg)
			return
		}
		slog.Infof(ctx, "Returning %d edit(s) to %s", len(infos), caddr)
		clog.Logf(ctx, "Generated %d edit(s)", len(infos))
		if err := stream.Edits(infos); err != nil {
			slog.Errorf(ctx, "Failed sending edits to %s: %v", caddr, err)
		}
	})

	// Perform redirects for URLs returned by seed.MakeAddCoverArtRedirectURI.
	http.HandleFunc("/redirect-add-cover-art", func(w http.ResponseWriter, req *http.Request) {
		mbid := req.FormValue("release_mbid")
		if !mbdb.IsMBID(mbid) {
			http.Error(w, "Invalid release_mbid parameter", http.StatusBadRequest)
			return
		}
		dst, err := url.Parse(req.Referer())
		if err != nil || !mbSrvRegexp.MatchString(dst.Host) {
			http.Error(w, "Bad referrer", http.StatusBadRequest)
			return
		}
		dst.Path = "/release/" + mbid + "/add-cover-art"
		dst.Fragment = ""

		// Copy query parameters used to seed form inputs.
		vals := make(url.Values)
		for _, k := range []string{"edit_note", "make_votable"} {
			if v := req.FormValue(k); v != "" {
				vals.Set("add-cover-art."+k, v)
			}
		}
		dst.RawQuery = vals.Encode()

		http.Redirect(w, req, dst.String(), http.StatusFound)
	})

	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "image/x-icon")
		w.Write(faviconData)
	})
	http.HandleFunc("/robots.txt", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		io.WriteString(w, "User-agent: *\nAllow: /\n")
	})

	// Handle GCP specifying the port to listen on.
	if port := os.Getenv("PORT"); port != "" {
		*addr = ":" + port
	}
	slog.Info(context.Background(), "Listening on ", *addr)
	if err := http.ListenAndServe(*addr, nil); err != nil {
		slog.Error(context.Background(), "Failed listening: ", err)
		os.Exit(1)
	}
}

// mbSrvRegexp matches a hostname under musicbrainz.org.
var mbSrvRegexp = regexp.MustCompile(`(?i)(?:^|\.)musicbrainz\.org$`)

// userError implements the error interface but also wraps a message that
// should be returned to the user.
type userError struct {
	msg string // message to display to user
	err error  // actual underlying error to log
}

func (e *userError) Error() string { return e.err.Error() }

// userErrorf returns an *userError with the supplied user-visible message
// and an err field constructed from format and args.
func userErrorf(msg, format string, args ...interface{}) *userError {
	return &userError{msg: msg, err: fmt.Errorf(format, args...)}
}

// genForm generates a page with an empty form.
func genForm(minifyForm bool) ([]byte, error) {
	var b bytes.Buffer
	w := io.Writer(&b)
	close := func() error { return nil }

	if minifyForm {
		m := minify.New()
		m.AddFunc("text/css", css.Minify)
		m.AddFunc("text/html", html.Minify)
		m.AddFunc("text/svg+xml", svg.Minify)
		m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)
		mw := m.Writer("text/html", &b)
		w = mw
		close = mw.Close
	}

	if err := render.Write(w, nil, render.Version(version), render.MinifyJS(minifyForm)); err != nil {
		return nil, err
	}
	if err := close(); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// getEditsForRequest generates render.EditInfo objects in response to an /edits request to the server.
func getEditsForRequest(ctx context.Context, w http.ResponseWriter, req *http.Request,
	serverURL string, rm *rateMap, db *mbdb.DB) ([]*render.EditInfo, error) {
	if req.Method != http.MethodPost {
		return nil, userErrorf("Invalid method", "bad method %q", req.Method)
	}

	now := time.Now()
	caddr := clientAddr(req)

	ip, _, err := net.SplitHostPort(caddr)
	if err != nil {
		ip = caddr
	}
	if !rm.attempt(ip, now) {
		return nil, userErrorf("Please wait a few seconds and try again", "too many requests")
	}

	req.Body = http.MaxBytesReader(w, req.Body, maxReqBytes)
	if err := req.ParseMultipartForm(maxReqBytes); err != nil {
		return nil, &userError{"Failed parsing request", err}
	}

	if req.FormValue("beta") == "1" {
		serverURL = betaServerURL
	}

	src := req.FormValue("source")
	slog.Infof(ctx, "Got %q edits request from %v", src, caddr)

	var edits []seed.Edit
	switch src {
	case "online":
		reqURL := req.FormValue("url")
		slog.Infof(ctx, "Handling request for %q", reqURL)
		ctx, cancel := context.WithTimeout(ctx, onlineEditsTimeout)
		defer cancel()
		cfg := online.Config{
			CountryCode:         strings.ToUpper(strings.TrimSpace(req.FormValue("country"))),
			CorrectPunctuation:  req.FormValue("punctuation") == "1",
			ExtractTrackArtists: req.FormValue("extractTrackArtists") == "1",
			RemoveParens:        req.FormValue("removeParens") == "1",
			SplitArtists:        req.FormValue("splitArtists") == "1",
		}
		if !countryCodeRegexp.MatchString(cfg.CountryCode) {
			return nil, userErrorf(
				"Invalid country code (should be two letters)",
				"invalid country %q", cfg.CountryCode,
			)
		} else if cfg.CountryCode == tidal.AllCountriesCode {
			return nil, userErrorf(
				"Use yambs command-line program to query all countries",
				"not querying all countries",
			)
		}
		edits, err = online.MakeEdits(ctx, reqURL, req.Form["set"], db, &cfg)
		if urlErr, ok := err.(*online.BadURLError); ok {
			var msg string
			switch {
			case urlErr.SeederURL != "":
				// If this message is changed, the regular expression in showError() in
				// render/res/form.ts also needs to be updated.
				msg = "Unsupported URL; try " + urlErr.SeederURL
			case urlErr.NoProvider:
				urls := make([]string, len(online.ExampleURLs))
				for i, u := range online.ExampleURLs {
					urls[i] = " • " + u
				}
				msg = "Sorry, only URLs like the following are supported:\n" + strings.Join(urls, "\n")
			default:
				msg = fmt.Sprint("Failed checking URL: ", err)
			}
			return nil, userErrorf(msg, "%q: %v", req.FormValue("url"), err)
		} else if err != nil {
			return nil, &userError{fmt.Sprint("Failed getting edits: ", err), err}
		}

	case "recordings", "text":
		ctx, cancel := context.WithTimeout(ctx, textEditsTimeout)
		defer cancel()

		var typ seed.Entity
		var fields []string
		var format text.Format
		var input io.Reader

		if src == "recordings" {
			typ = seed.RecordingEntity
			fields = []string{"mbid"}
			format = text.CSV

			var err error
			rel := req.FormValue("release")
			if input, err = cmdutil.GetReleaseRecordingsInput(ctx, db, rel); err != nil {
				return nil, userErrorf("Failed getting recordings", "getting %q recordings: %v", rel, err)
			}
		} else {
			// Sigh, so dumb.
			allowedTypes := make([]interface{}, len(seed.EntityTypes))
			for i, t := range seed.EntityTypes {
				allowedTypes[i] = t
			}
			typ = seed.Entity(req.FormValue("type"))
			if !checkEnum(typ, allowedTypes...) {
				return nil, userErrorf("Invalid entity type", "bad type %q", string(typ))
			}
			fields = req.Form["field"]
			format = text.Format(req.FormValue("format"))
			if !checkEnum(format, text.CSV, text.KeyVal, text.TSV) {
				return nil, userErrorf("Invalid format", "bad format %q", string(format))
			}
			input = io.Reader(strings.NewReader(req.FormValue("input")))
		}

		slog.Infof(ctx, "Handling %v-byte %v %v request", req.ContentLength, typ, format)
		var err error
		if edits, err = text.Read(ctx, input, format, typ, fields, req.Form["set"], db,
			text.MaxEdits(maxEdits), text.MaxFields(maxFields)); err != nil {
			return nil, &userError{fmt.Sprint("Failed getting edits: ", err), err}
		}

	default:
		return nil, userErrorf("Invalid source", "bad source %q", src)
	}
	return render.NewEditInfos(edits, serverURL)
}

// countryCodeRegexp is used to validate the optional "country" query parameter.
var countryCodeRegexp = regexp.MustCompile(`^(?:|[A-Z][A-Z])$`)

// clientAddr returns the client's address (which may be either "ip" or "ip:port").
func clientAddr(req *http.Request) string {
	// When running under App Engine, connections come from 127.0.0.1,
	// so get the client IP from the X-Forwarded-For header.
	// Also check for K_SERVICE to detect Cloud Run:
	// https://cloud.google.com/run/docs/container-contract#services-env-vars
	if os.Getenv("GAE_ENV") != "" || os.Getenv("K_SERVICE") != "" {
		if hdr := req.Header.Get("X-Forwarded-For"); hdr != "" {
			// X-Forwarded-For: <client>, <proxy1>, <proxy2>
			return strings.SplitN(hdr, ", ", 2)[0]
		}
	}
	return req.RemoteAddr
}

// checkEnum returns true if input appears in valid.
func checkEnum(input interface{}, valid ...interface{}) bool {
	for _, v := range valid {
		if input == v {
			return true
		}
	}
	return false
}

//go:embed favicon.ico
var faviconData []byte
