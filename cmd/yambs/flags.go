// Copyright 2022 Daniel Erat.
// All rights reserved.

package main

import (
	"errors"
	"strings"
)

// enumFlag accepts one or multiple strings from a list of allowed values.
type enumFlag struct {
	vals    []string // specified values (also default)
	allowed []string // acceptable values
	multi   bool     // accept multiple values
}

func (ef *enumFlag) String() string { return strings.Join(ef.vals, ",") }
func (ef *enumFlag) Set(v string) error {
	err := errors.New("want " + ef.allowedList())
	parts := strings.Split(v, ",")
	if v == "" || (len(parts) > 1 && !ef.multi) {
		return err
	}
	var vals []string
PartsLoop:
	for _, p := range parts {
		for _, a := range ef.allowed {
			if p == a {
				vals = append(vals, p)
				continue PartsLoop
			}
		}
		return err
	}
	ef.vals = vals
	return nil
}

// firstVal returns the first value that was supplied.
// If no values were supplied, an empty string is returned.
func (ef *enumFlag) firstVal() string {
	if len(ef.vals) == 0 {
		return ""
	}
	return ef.vals[0]
}

// hasVal returns true if the supplied value was supplied.
func (ef *enumFlag) hasVal(want string) bool {
	for _, v := range ef.vals {
		if v == want {
			return true
		}
	}
	return false
}

// allowedList returns a comma-separated list of allowed values.
func (ef *enumFlag) allowedList() string { return strings.Join(ef.allowed, ", ") }

// repeatedFlag can be specified multiple times to supply string values.
type repeatedFlag []string

func (rf *repeatedFlag) String() string { return strings.Join(*rf, ",") }
func (rf *repeatedFlag) Set(v string) error {
	*rf = append(*rf, v)
	return nil
}
