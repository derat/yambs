// Copyright 2023 Daniel Erat.
// All rights reserved.

package main

import (
	"testing"
)

func TestEnumFlag_Single(t *testing.T) {
	ef := enumFlag{
		vals:    []string{"foo"},
		allowed: []string{"foo", "bar"},
	}
	if got := ef.String(); got != "foo" {
		t.Errorf("String() = %q; want %q", got, "foo")
	}

	if err := ef.Set("bar"); err != nil {
		t.Fatalf("Set(%q) failed: %v", "bar", err)
	}
	if got := ef.String(); got != "bar" {
		t.Errorf("String() = %q; want %q", got, "bar")
	}
	if got := ef.firstVal(); got != "bar" {
		t.Errorf("firstVal() = %q; want %q", got, "bar")
	}
	if !ef.hasVal("bar") {
		t.Errorf("hasVal(%q) = %v; want %v", "bar", false, true)
	}
	if ef.hasVal("foo") {
		t.Errorf("hasVal(%q) = %v; want %v", "foo", true, false)
	}

	for _, v := range []string{"baz", "foo,bar", "", ","} {
		if err := ef.Set(v); err == nil {
			t.Errorf("Set(%q) unexpectedly succeeded", v)
		}
	}
}

func TestEnumFlag_Multi(t *testing.T) {
	ef := enumFlag{
		vals:    []string{"foo", "bar"},
		allowed: []string{"foo", "bar", "baz"},
		multi:   true,
	}
	if got := ef.String(); got != "foo,bar" {
		t.Errorf("String() = %q; want %q", got, "foo,bar")
	}

	if err := ef.Set("bar,baz"); err != nil {
		t.Fatalf("Set(%q) failed: %v", "bar,baz", err)
	}
	if got := ef.String(); got != "bar,baz" {
		t.Errorf("String() = %q; want %q", got, "bar,baz")
	}
	if got := ef.firstVal(); got != "bar" {
		t.Errorf("firstVal() = %q; want %q", got, "bar")
	}
	if !ef.hasVal("bar") {
		t.Errorf("hasVal(%q) = %v; want %v", "bar", false, true)
	}
	if !ef.hasVal("baz") {
		t.Errorf("hasVal(%q) = %v; want %v", "baz", false, true)
	}
	if ef.hasVal("foo") {
		t.Errorf("hasVal(%q) = %v; want %v", "foo", true, false)
	}

	for _, v := range []string{"blah", "foo,blah", "", ","} {
		if err := ef.Set(v); err == nil {
			t.Errorf("Set(%q) unexpectedly succeeded", v)
		}
	}
}
