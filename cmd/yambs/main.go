// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package main implements a command-line program for generating seeded edits.
package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"

	"codeberg.org/derat/yambs/cmdutil"
	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/render"
	"codeberg.org/derat/yambs/seed"
	"codeberg.org/derat/yambs/sources/feed"
	"codeberg.org/derat/yambs/sources/mp3"
	"codeberg.org/derat/yambs/sources/online"
	"codeberg.org/derat/yambs/sources/text"
	"codeberg.org/derat/yambs/web"
)

var version = "[non-release]"

const (
	actionOpen  = "open"  // open the page from a temp file
	actionPrint = "print" // print URLs
	actionServe = "serve" // open the page from a local HTTP server
	actionWrite = "write" // write the page to stdout
)

const (
	modifyExtractArtists = "extract-artists" // extract artist from "Artist - Title"
	modifyPunctuation    = "punctuation"     // try to correct punctuation
	modifyRemoveParens   = "remove-parens"   // remove parenthetical suffixes
	modifySplitArtists   = "split-artists"   // split artist names on join phrases
)

func main() {
	action := enumFlag{
		vals:    []string{defaultAction()},
		allowed: []string{actionOpen, actionPrint, actionServe, actionWrite},
	}
	var entity enumFlag // empty default
	for _, t := range seed.EntityTypes {
		entity.allowed = append(entity.allowed, string(t))
	}
	format := enumFlag{
		vals:    []string{string(text.TSV)},
		allowed: []string{string(text.CSV), string(text.KeyVal), string(text.TSV)},
	}
	modifyData := enumFlag{
		vals: nil,
		allowed: []string{
			modifyExtractArtists,
			modifyPunctuation,
			modifyRemoveParens,
			modifySplitArtists,
		},
		multi: true,
	}

	var setCmds repeatedFlag

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage: %v [flag]... <FILE/URL>\n"+
			"Seeds MusicBrainz edits.\n\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Var(&action, "action", fmt.Sprintf("Action to perform with seed URLs (%v)", action.allowedList()))
	addr := flag.String("addr", "localhost:8999", `Address to listen on for -action=serve`)
	charset := flag.String("charset", "", `Charset for text input if not UTF-8 (IANA or MIME, e.g. "ISO-8859-1" or "latin1")`)
	country := flag.String("country", "", `Country code for querying Tidal API (ISO 3166, e.g. "US" or "DE"; "XW" for all)`)
	editRecordings := flag.String("edit-release-recordings", "", `Release MBID or URL whose recordings will be edited`)
	fields := flag.String("fields", "", `Comma-separated fields for CSV/TSV columns (e.g. "artist,name,length")`)
	flag.Var(&format, "format", fmt.Sprintf("Format for text input (%v)", format.allowedList()))
	listFields := flag.Bool("list-fields", false, "Print available fields for -type and exit")
	mergeRanges := flag.String("merge-ranges", "",
		`Colon-separated list of comma-separated custom ranges for merging (e.g. "1-4,6-10:1-9")`)
	mergeRecordings := flag.String("merge-release-recordings", "",
		`Comma-separated release MBIDs or URLs whose recordings will be merged`)
	flag.Var(&modifyData, "modify-data",
		fmt.Sprintf("Comma-separated modifications for online data (%v)", modifyData.allowedList()))
	server := flag.String("server", "musicbrainz.org", "MusicBrainz server hostname")
	flag.Var(&setCmds, "set", `Set a field for all entities (e.g. "edit_note=from https://www.example.org")`)
	timeout := flag.Duration("timeout", 0, `Timeout for generating edits (e.g. "30s" or "2m")`)
	flag.Var(&entity, "type", fmt.Sprintf("Entity type for text or MP3 input (%v)", entity.allowedList()))
	verbose := flag.Bool("verbose", false, "Enable verbose logging")
	printVersion := flag.Bool("version", false, "Print the version and exit")
	flag.Parse()

	os.Exit(func() int {
		if *printVersion {
			fmt.Println("yambs " + version)
			return 0
		}

		if *listFields {
			if entity.firstVal() == "" {
				fmt.Fprintln(os.Stderr, "Must specify entity type via -type")
				return 2
			}
			var list [][2]string // name, desc
			var max int
			for name, desc := range text.ListFields(seed.Entity(entity.firstVal()), false /* html */) {
				list = append(list, [2]string{name, desc})
				if len(name) > max {
					max = len(name)
				}
			}
			sort.Slice(list, func(i, j int) bool { return list[i][0] < list[j][0] })
			// Some terminals (e.g. the Chrome OS one) select the contents within a pair of ASCII
			// doublequotes on double-click but include the quote characters if Unicode “ and ” are
			// used instead, so update descriptions to use the ASCII characters.
			repl := strings.NewReplacer(
				"’", "'",
				"“", `"`,
				"”", `"`,
			)
			for _, f := range list {
				fmt.Printf("%-"+strconv.Itoa(max)+"s  %s\n", f[0], repl.Replace(f[1]))
			}
			return 0
		}

		if !*verbose {
			log.SetOutput(io.Discard)
		}

		var ctx context.Context
		var cancel context.CancelFunc
		if *timeout > 0 {
			ctx, cancel = context.WithTimeout(context.Background(), *timeout)
		} else {
			ctx, cancel = context.WithCancel(context.Background())
		}
		defer cancel()

		serverURL := cmdutil.AddSchemeToServer(*server)
		db := mbdb.NewDB(mbdb.ServerURL(serverURL), mbdb.Version(version))
		web.SetUserAgent(fmt.Sprintf("yambs/%s (+https://codeberg.org/derat/yambs)", version))

		var r io.Reader
		var srcURL string
		switch {
		case *editRecordings != "":
			if flag.NArg() != 0 {
				fmt.Fprintln(os.Stderr, "-edit-release-recordings conflicts with positional args")
				return 2
			} else if *fields != "" {
				fmt.Fprintln(os.Stderr, "-edit-release-recordings overrides -fields")
				return 2
			}
			var err error
			if r, err = cmdutil.GetReleaseRecordingsInput(ctx, db, *editRecordings); err != nil {
				fmt.Fprintln(os.Stderr, "Failed getting release:", err)
				return 1
			}
			*fields = "mbid"
			format.Set(string(text.CSV))
			entity.Set(string(seed.RecordingEntity))
		case *mergeRecordings != "":
			if flag.NArg() != 0 {
				fmt.Fprintln(os.Stderr, "-merge-release-recordings conflicts with positional args")
				return 2
			}
		case flag.NArg() == 0:
			r = os.Stdin
		case flag.NArg() == 1:
			if arg := flag.Arg(0); urlRegexp.MatchString(arg) {
				srcURL = arg
			} else {
				f, err := os.Open(arg)
				if err != nil {
					fmt.Fprintln(os.Stderr, err)
					return 1
				}
				defer f.Close()
				r = f
			}
		default:
			flag.Usage()
			return 2
		}

		var edits []seed.Edit
		switch {
		case *mergeRecordings != "":
			var err error
			rels := strings.Split(*mergeRecordings, ",")
			var ranges []string
			if *mergeRanges != "" {
				ranges = strings.Split(*mergeRanges, ":")
			}
			if edits, err = cmdutil.MergeReleaseRecordings(ctx, db, rels, ranges); err != nil {
				fmt.Fprintln(os.Stderr, "Failed generating merges:", err)
				return 1
			}
		case srcURL != "":
			var err error
			cfg := online.Config{
				CountryCode:         strings.ToUpper(*country),
				CorrectPunctuation:  modifyData.hasVal(modifyPunctuation),
				ExtractTrackArtists: modifyData.hasVal(modifyExtractArtists),
				RemoveParens:        modifyData.hasVal(modifyRemoveParens),
				SplitArtists:        modifyData.hasVal(modifySplitArtists),
			}
			if edits, err = online.MakeEdits(ctx, srcURL, setCmds, db, &cfg); err != nil {
				fmt.Fprintln(os.Stderr, "Failed fetching page:", err)
				return 1
			}
		default:
			if entity.firstVal() == "" {
				fmt.Fprintln(os.Stderr, "Must specify entity type via -type")
				return 2
			}
			f, isFile := r.(*os.File)
			var ext string
			if isFile {
				ext = strings.ToLower(filepath.Ext(f.Name()))
			}
			var err error
			if isFile && ext == ".mp3" {
				if edits, err = mp3.ReadFile(f, seed.Entity(entity.firstVal()), setCmds); err != nil {
					fmt.Fprintln(os.Stderr, "Failed reading MP3 file:", err)
					return 1
				}
			} else if isFile && ext == ".xml" {
				if edits, err = feed.ReadFile(ctx, f, seed.Entity(entity.firstVal()), setCmds); err != nil {
					fmt.Fprintln(os.Stderr, "Failed reading feed:", err)
					return 1
				}
			} else {
				var opts []text.Option
				if *charset != "" {
					opts = append(opts, text.Charset(*charset))
				}
				if edits, err = text.Read(ctx, r, text.Format(format.firstVal()), seed.Entity(entity.firstVal()),
					strings.Split(*fields, ","), setCmds, db, opts...); err != nil {
					fmt.Fprintln(os.Stderr, "Failed reading edits:", err)
					return 1
				}
			}
		}

		renderOpts := []render.Option{
			render.ServerURL(serverURL),
			render.Version(version), // not actually displayed
		}

		switch action.firstVal() {
		case actionOpen:
			if err := render.OpenFile(ctx, edits, renderOpts...); err != nil {
				fmt.Fprintln(os.Stderr, "Failed opening page:", err)
				return 1
			}
		case actionPrint:
			for _, ed := range edits {
				if ed.Method() != http.MethodGet {
					fmt.Fprintf(os.Stderr, "Can't print bare URL; %s edit requires %s request\n",
						ed.Entity(), ed.Method())
					return 1
				}
				u, err := url.Parse(ed.URL(serverURL))
				if err != nil {
					fmt.Fprintln(os.Stderr, "Failed parsing URL:", err)
					return 1
				}
				u.RawQuery = ed.Params().Encode()
				fmt.Println(u.String())
			}
		case actionServe:
			// Don't use ctx here; it's just for generating the edits.
			// The local HTTP server needs to continue running until the user opens them.
			if err := render.OpenHTTP(context.Background(), *addr, edits, renderOpts...); err != nil {
				fmt.Fprintln(os.Stderr, "Failed serving page:", err)
				return 1
			}
		case actionWrite:
			if err := render.Write(os.Stdout, edits, renderOpts...); err != nil {
				fmt.Fprintln(os.Stderr, "Failed writing page:", err)
				return 1
			}
		}

		return 0
	}())
}

func defaultAction() string {
	// If we're running in a Chrome OS Crostini container, the external Chrome process won't
	// be able to access files that we write to /tmp, so start a web server instead.
	if _, err := exec.LookPath("garcon-url-handler"); err == nil && os.Getenv("BROWSER") == "" {
		return actionServe
	}
	return actionOpen
}

var urlRegexp = regexp.MustCompile("(?i)^https?://")
