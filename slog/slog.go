// Copyright 2024 Daniel Erat.
// All rights reserved.

// slog performs structured logging when running on GCP.
package slog

import (
	"context"
	"encoding/json"
	"fmt"
	golog "log"
	"net/http"
	"os"
	"strings"
	"sync"
)

// Info logs an informational message in the manner of log.Print.
func Info(ctx context.Context, v ...any) {
	writeEntry(ctx, fmt.Sprint(v...), severityInfo)
}

// Errorf logs an informational message in the manner of log.Printf.
func Infof(ctx context.Context, format string, v ...any) {
	writeEntry(ctx, fmt.Sprintf(format, v...), severityInfo)
}

// Error logs an error in the manner of log.Print.
func Error(ctx context.Context, v ...any) {
	writeEntry(ctx, fmt.Sprint(v...), severityError)
}

// Errorf logs an error in the manner of log.Printf.
func Errorf(ctx context.Context, format string, v ...any) {
	writeEntry(ctx, fmt.Sprintf(format, v...), severityError)
}

// onGCP is true when running on Cloud Run or App Engine.
var onGCP = os.Getenv("K_SERVICE") != "" || os.Getenv("GAE_APPLICATION") != ""

type traceKeyType string

var traceKey = traceKeyType("trace")

// RequestContext attempts to extract a GCP trace ID from req and attach it to a new context
// derived from req.Context().
func RequestContext(req *http.Request) context.Context {
	if projectID := os.Getenv("GOOGLE_CLOUD_PROJECT"); projectID != "" {
		// https://cloud.google.com/trace/docs/trace-context:
		//  X-Cloud-Trace-Context: TRACE_ID/SPAN_ID;o=OPTIONS
		//  TRACE_ID is a 32-character hexadecimal value representing a 128-bit number.
		parts := strings.SplitN(req.Header.Get("X-Cloud-Trace-Context"), "/", 2)
		if len(parts) == 2 && len(parts[0]) == 32 {
			// I have no idea if this format is actually documented anywhere,
			// but it appears to be what the legacy App Engine logging library uses.
			trace := fmt.Sprintf("projects/%s/traces/%s", projectID, parts[0])
			return context.WithValue(req.Context(), traceKey, trace)
		}
	}
	return req.Context()
}

// severity describes a severity level for a GCP log entry:
// https://cloud.google.com/logging/docs/structured-logging
type severity string

const (
	severityInfo  severity = "INFO"
	severityError severity = "ERROR"
)

// entryMutex synchronizes writes of structured log messages to stdout.
var entryMutex sync.Mutex

// writeEntry JSON-marshals a GCP structured log entry to stdout if running on GCP.
// Otherwise, it passes msg to log.Print from the standard library.
func writeEntry(ctx context.Context, msg string, sev severity) {
	if !onGCP {
		golog.Print(msg)
		return
	}

	id, _ := ctx.Value(traceKey).(string)
	e := entry{
		Message:  msg,
		Severity: string(sev),
		Trace:    id,
	}
	entryMutex.Lock()
	json.NewEncoder(os.Stdout).Encode(&e)
	entryMutex.Unlock()
}

// entry defines a GCP structured log entry:
// https://cloud.google.com/appengine/docs/standard/writing-application-logs?tab=go#stdout_stderr
type entry struct {
	Message  string `json:"message"`
	Severity string `json:"severity,omitempty"`
	Trace    string `json:"logging.googleapis.com/trace,omitempty"`
}
