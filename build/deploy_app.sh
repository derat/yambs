#!/bin/sh -e

usage=$(cat <<EOF
Usage: $0 [flags]...
Deploy the app to Google Cloud Run.

Flags:
  -c  Short commit hash
  -p  GCP project ID to use
  -r  GCP region to deploy to
  -s  Cloud Run service name
EOF
)

commit=unknown
project=
service=
while getopts c:p:r:s: o; do
  case $o in
    c) commit="$OPTARG" ;;
    p) project="$OPTARG" ;;
    r) region="$OPTARG" ;;
    s) service="$OPTARG" ;;
    \?) echo "$usage" >&2 && exit 2 ;;
  esac
done
shift $(expr $OPTIND - 1)

if [ -z "$project" ]; then
  echo 'GCP project ID must be specified using -p' >&2
  exit 2
elif [ -z "$region" ]; then
  echo 'Region must be specified using -r' >&2
  exit 2
elif [ -z "$service" ]; then
  echo 'Service name must be specified using -s' >&2
  exit 2
fi

# Inject a version string.
version="$(date --utc +%Y%m%d)-${commit}"

gcloud --project="$project" \
  run deploy "$service" \
  --region="$region" \
  --source=. \
  --allow-unauthenticated \
  --min-instances=0 \
  --max-instances=1 \
  --set-env-vars=APP_VERSION="$version" \
  --set-env-vars=GOOGLE_CLOUD_PROJECT="$project"
