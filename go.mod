module codeberg.org/derat/yambs

go 1.19

require (
	cloud.google.com/go/translate v1.4.0
	codeberg.org/derat/mpeg v0.0.0-20230512002748-0564001124d1
	codeberg.org/derat/validate v0.0.0-20240416124056-31d40a517109
	github.com/andybalholm/cascadia v1.3.1
	github.com/evanw/esbuild v0.20.2
	github.com/google/go-cmp v0.5.9
	github.com/hjfreyer/taglib-go v0.0.0-20230511211649-38a11b8e3726
	github.com/pkg/browser v0.0.0-20210911075715-681adbf594b8
	github.com/tdewolff/minify/v2 v2.12.7
	golang.org/x/net v0.34.0
	golang.org/x/text v0.21.0
	golang.org/x/time v0.1.0
)

require (
	cloud.google.com/go v0.105.0 // indirect
	cloud.google.com/go/compute v1.12.1 // indirect
	cloud.google.com/go/compute/metadata v0.2.1 // indirect
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/enterprise-certificate-proxy v0.2.0 // indirect
	github.com/googleapis/gax-go/v2 v2.6.0 // indirect
	github.com/tdewolff/parse/v2 v2.6.6 // indirect
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/oauth2 v0.0.0-20221014153046-6fdb5e3db783 // indirect
	golang.org/x/sys v0.29.0 // indirect
	google.golang.org/api v0.102.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20221027153422-115e99e71e1c // indirect
	google.golang.org/grpc v1.50.1 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)

replace golang.org/x/sys => golang.org/x/sys v0.0.0-20220811171246-fbc7d0a398ab
