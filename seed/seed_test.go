// Copyright 2022 Daniel Erat.
// All rights reserved.

package seed

import (
	"testing"
)

func TestMakeDesc(t *testing.T) {
	for _, tc := range []struct {
		mbid  string
		other []string
		want  string
	}{
		{"", nil, "[unknown]"},
		{"", []string{"", ""}, "[unknown]"},
		{"", []string{"foo"}, "foo"},
		{"", []string{"foo", "bar"}, "foo / bar"},
		{"", []string{"foo", "", "bar"}, "foo / bar"},
		{"", []string{"12345678901234567890123456789012345678901234"},
			"123456789012345678901234567890123456789…"}, // per maxDescLen
		{"37a9fc00-45d5-434a-85e4-de5d8ed25ec2", nil, "37a9fc00"},
		{"37a9fc00-45d5-434a-85e4-de5d8ed25ec2", []string{"", ""}, "37a9fc00"},
		{"37a9fc00-45d5-434a-85e4-de5d8ed25ec2", []string{"foo", "bar"}, "37a9fc00 / foo / bar"},
	} {
		if got := makeDesc(tc.mbid, tc.other...); got != tc.want {
			t.Errorf("makeDesc(%q, %q) = %q; want %q", tc.mbid, tc.other, got, tc.want)
		}
	}
}

func TestMakeURL(t *testing.T) {
	const serverURL = "https://example.org"
	for _, tc := range []struct {
		createPath  string
		editPathFmt string
		mbid        string
		want        string
	}{
		{"/foo/create", "/foo/%v/edit", "", serverURL + "/foo/create"},
		{"/foo/create", "/foo/%v/edit", "37a9fc00-45d5-434a-85e4-de5d8ed25ec2",
			serverURL + "/foo/37a9fc00-45d5-434a-85e4-de5d8ed25ec2/edit"},
	} {
		if got := makeURL(serverURL, tc.createPath, tc.editPathFmt, tc.mbid); got != tc.want {
			t.Errorf("makeURL(%q, %q, %q, %q) = %q; want %q",
				serverURL, tc.createPath, tc.editPathFmt, tc.mbid, got, tc.want)
		}
	}
}
