// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"net/url"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestPlace_URL(t *testing.T) {
	const srvURL = "https://test.musicbrainz.org"
	for _, tc := range []struct{ mbid, want string }{
		{"", srvURL + "/place/create"},
		{"d98928e8-6757-4196-a945-e7145d94d9e4", srvURL + "/place/d98928e8-6757-4196-a945-e7145d94d9e4/edit"},
	} {
		p := Place{MBID: tc.mbid}
		if got := p.URL(srvURL); got != tc.want {
			t.Errorf("MBID %q yielded URL %q; want %q", tc.mbid, got, tc.want)
		}
	}
}

func TestPlace_Params(t *testing.T) {
	p := Place{
		Name:           "A New Place",
		Disambiguation: "for testing",
		Type:           PlaceType_Venue,
		Address:        "123 Elm St",
		AreaName:       "Anytown",
		Coordinates:    "44.967243, -103.771556",
		BeginDate:      Date{1980, 12, 1},
		EndDate:        Date{2000, 4, 23},
		Ended:          true,
		Relationships: []Relationship{{
			Target: "0f50beab-d77d-4f0f-ac26-0b87d3e9b11b",
			Type:   LinkType_Founder_Artist_Place,
		}},
		URLs: []URL{{
			URL:      "https://example.org/my-favorite-venue",
			LinkType: LinkType_Fanpage_Place_URL,
		}},
		EditNote:    "here's the edit note",
		MakeVotable: true,
	}

	rel := p.Relationships[0]
	want := url.Values{
		"edit-place.name":                    {p.Name},
		"edit-place.comment":                 {p.Disambiguation},
		"edit-place.type_id":                 {strconv.Itoa(int(p.Type))},
		"edit-place.address":                 {p.Address},
		"edit-place.area.name":               {p.AreaName},
		"edit-place.coordinates":             {p.Coordinates},
		"edit-place.period.begin_date.year":  {strconv.Itoa(p.BeginDate.Year)},
		"edit-place.period.begin_date.month": {strconv.Itoa(p.BeginDate.Month)},
		"edit-place.period.begin_date.day":   {strconv.Itoa(p.BeginDate.Day)},
		"edit-place.period.end_date.year":    {strconv.Itoa(p.EndDate.Year)},
		"edit-place.period.end_date.month":   {strconv.Itoa(p.EndDate.Month)},
		"edit-place.period.end_date.day":     {strconv.Itoa(p.EndDate.Day)},
		"edit-place.period.ended":            {"1"},
		"rels.0.target":                      {rel.Target},
		"rels.0.type":                        {strconv.Itoa(int(rel.Type))},
		"edit-place.url.0.text":              {p.URLs[0].URL},
		"edit-place.url.0.link_type_id":      {strconv.Itoa(int(p.URLs[0].LinkType))},
		"edit-place.edit_note":               {p.EditNote},
		"edit-place.make_votable":            {"1"},
	}
	if diff := cmp.Diff(want, p.Params()); diff != "" {
		t.Error("Incorrect query params:\n" + diff)
	}
}
