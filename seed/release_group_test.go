// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"net/url"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestReleaseGroup_URL(t *testing.T) {
	const srvURL = "test.musicbrainz.org"
	for _, tc := range []struct{ mbid, want string }{
		{"", srvURL + "/release-group/create"},
		{"d98928e8-6757-4196-a945-e7145d94d9e4", srvURL + "/release-group/d98928e8-6757-4196-a945-e7145d94d9e4/edit"},
	} {
		rg := ReleaseGroup{MBID: tc.mbid}
		if got := rg.URL(srvURL); got != tc.want {
			t.Errorf("MBID %q yielded URL %q; want %q", tc.mbid, got, tc.want)
		}
	}
}

func TestReleaseGroup_Params(t *testing.T) {
	rg := ReleaseGroup{
		Name: "Some Album",
		Artists: []ArtistCredit{
			{MBID: "2c05d655-14d0-43a9-a1ef-89dd6e79d52e", NameAsCredited: "First Artist", JoinPhrase: " & "},
			{Name: "Some Other Artist"},
		},
		Disambiguation: "for testing",
		PrimaryType:    ReleaseGroupTypeID_Album,
		SecondaryTypes: []ReleaseGroupTypeID{
			ReleaseGroupTypeID_Remix,
			ReleaseGroupTypeID_Live,
		},
		Relationships: []Relationship{{
			Target: "0f50beab-d77d-4f0f-ac26-0b87d3e9b11b",
			Type:   LinkType_Cover_ReleaseGroup_ReleaseGroup,
		}},
		URLs: []URL{{
			URL:      "https://example.org/",
			LinkType: LinkType_Discography_ReleaseGroup_URL,
		}},
		MakeVotable: true,
		EditNote:    "here's the edit note",
	}

	rel := rg.Relationships[0]
	want := url.Values{
		"edit-release-group.name":                              {rg.Name},
		"edit-release-group.artist_credit.names.0.join_phrase": {rg.Artists[0].JoinPhrase},
		"edit-release-group.artist_credit.names.0.mbid":        {rg.Artists[0].MBID},
		"edit-release-group.artist_credit.names.0.name":        {rg.Artists[0].NameAsCredited},
		"edit-release-group.artist_credit.names.1.artist.name": {rg.Artists[1].Name},
		"edit-release-group.comment":                           {rg.Disambiguation},
		"edit-release-group.primary_type_id":                   {strconv.Itoa(int(rg.PrimaryType))},
		"edit-release-group.secondary_type_ids": {
			strconv.Itoa(int(rg.SecondaryTypes[0])),
			strconv.Itoa(int(rg.SecondaryTypes[1])),
		},
		"rels.0.target":                         {rel.Target},
		"rels.0.type":                           {strconv.Itoa(int(rel.Type))},
		"edit-release-group.url.0.text":         {rg.URLs[0].URL},
		"edit-release-group.url.0.link_type_id": {strconv.Itoa(int(rg.URLs[0].LinkType))},
		"edit-release-group.edit_note":          {rg.EditNote},
		"edit-release-group.make_votable":       {"1"},
	}
	if diff := cmp.Diff(want, rg.Params()); diff != "" {
		t.Error("Incorrect query params:\n" + diff)
	}
}
