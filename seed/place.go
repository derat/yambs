// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"codeberg.org/derat/yambs/mbdb"
)

// Place holds data used to seed the "Add place" form at https://musicbrainz.org/place/create
// and the edit-place form at https://musicbrainz.org/place/<MBID>/edit.
// See https://musicbrainz.org/doc/Place for more information about place entities.
type Place struct {
	// MBID contains the place's MBID (for editing an existing place rather than creating a new one).
	MBID string
	// Name contains the place's name.
	Name string
	// Disambiguation differentiates this place from other places with similar names.
	// See https://musicbrainz.org/doc/Disambiguation_Comment.
	Disambiguation string
	// Type describes what kind of place this is.
	// See https://musicbrainz.org/doc/Place#Type.
	Type PlaceType
	// Address describes the location of the place using the standard addressing format for the
	// country it is located in. It should not not include the country.
	Address string
	// AreaName is used to fill the search field for the area (e.g. city) where the place is
	// located.
	AreaName string
	// Coordinates contains the latitude and longitude of the place's location, e.g. "38.635, -3.698".
	Coordinates string
	// BeginDate contains the date when the place was founded.
	BeginDate Date
	// EndDate contains the date when the place was closed.
	EndDate Date
	// Ended describes whether the place has closed.
	Ended bool
	// Relationships contains (non-URL) relationships between this place and other entities.
	Relationships []Relationship
	// URLs contains relationships between this place and one or more URLs.
	// See https://musicbrainz.org/doc/Style/Relationships/URLs.
	URLs []URL
	// EditNote contains the note attached to the edit.
	// See https://musicbrainz.org/doc/Edit_Note.
	EditNote string
	// MakeVotable indicates that edits should be made votable even if they would be auto-edits.
	MakeVotable bool
}

func (p *Place) Entity() Entity      { return PlaceEntity }
func (p *Place) Description() string { return makeDesc(p.MBID, p.Name) }
func (p *Place) URL(serverURL string) string {
	return makeURL(serverURL, "/place/create", "/place/%v/edit", p.MBID)
}

func (p *Place) Params() url.Values {
	vals := make(url.Values)
	if p.Name != "" {
		vals.Set("edit-place.name", p.Name)
	}
	if p.Disambiguation != "" {
		vals.Set("edit-place.comment", p.Disambiguation)
	}
	if p.Type != 0 {
		vals.Set("edit-place.type_id", strconv.Itoa(int(p.Type)))
	}
	if p.Address != "" {
		vals.Set("edit-place.address", p.Address)
	}
	if p.AreaName != "" {
		vals.Set("edit-place.area.name", p.AreaName)
	}
	if p.Coordinates != "" {
		vals.Set("edit-place.coordinates", p.Coordinates)
	}
	p.BeginDate.setParams(vals, "edit-place.period.begin_date.")
	p.EndDate.setParams(vals, "edit-place.period.end_date.")
	if p.Ended {
		vals.Set("edit-place.period.ended", "1")
	}
	for i, rel := range p.Relationships {
		rel.setParams(vals, fmt.Sprintf("rels.%d.", i))
	}
	for i, u := range p.URLs {
		u.setParams(vals, fmt.Sprintf("edit-place.url.%d.", i), p.Method())
	}
	if p.EditNote != "" {
		vals.Set("edit-place.edit_note", p.EditNote)
	}
	if p.MakeVotable {
		vals.Set("edit-place.make_votable", "1")
	}
	return vals
}

func (p *Place) Method() string                                { return http.MethodGet }
func (p *Place) Serial() bool                                  { return false }
func (p *Place) Finish(ctx context.Context, db *mbdb.DB) error { return nil }
