// Copyright 2022 Daniel Erat.
// All rights reserved.

// Package seed generates URLs that pre-fill fields when adding entities to MusicBrainz.
package seed

import (
	"context"
	"fmt"
	"math"
	"net/url"
	"strings"
	"time"

	"codeberg.org/derat/yambs/mbdb"
	"codeberg.org/derat/yambs/strutil"
)

//go:generate go run gen/gen_enums.go

const (
	maxDescLen    = 40 // max length for description components
	mbidPrefixLen = 8

	// MaxAudioLength is the maximum duration for a track or recording.
	// The track and recording tables' 'length' fields have type INTEGER.
	MaxAudioLength = math.MaxInt32 * time.Millisecond
)

// Entity describes a type of MusicBrainz entity being edited.
type Entity string

const (
	ArtistEntity       Entity = "artist"
	EventEntity        Entity = "event"
	LabelEntity        Entity = "label"
	PlaceEntity        Entity = "place"
	RecordingEntity    Entity = "recording"
	ReleaseEntity      Entity = "release"
	ReleaseGroupEntity Entity = "release-group"
	SeriesEntity       Entity = "series"
	WorkEntity         Entity = "work"
	InfoEntity         Entity = "info"  // informational edit; not a true entity
	MergeEntity        Entity = "merge" // merge edit; not a true entity
)

// EntityTypes lists real database entity types in alphabetical order.
var EntityTypes = []Entity{
	ArtistEntity,
	EventEntity,
	LabelEntity,
	PlaceEntity,
	RecordingEntity,
	ReleaseEntity,
	ReleaseGroupEntity,
	SeriesEntity,
	WorkEntity,
}

// Edit represents a seeded MusicBrainz edit.
type Edit interface {
	// Entity returns the type of entity being edited.
	Entity() Entity
	// Description returns a human-readable description of the edit.
	Description() string
	// URL returns a URL to seed the edit form.
	// serverURL contains the base MusicBrainz server URL without a trailing slash,
	// e.g. "https://musicbrainz.org" or "https://test.musicbrainz.org".
	URL(serverURL string) string
	// Params returns form values that should be sent to seed the edit form.
	// Note that some parameters contain multiple values (i.e. don't call Get()).
	Params() url.Values
	// Method returns the HTTP method that should be used for the request for URL.
	// GET is preferable since it avoids an anti-CSRF interstitial page.
	Method() string
	// Serial returns true if this edit cannot be performed in parallel
	// with other edits (i.e. due to MusicBrainz server limitations).
	Serial() bool
	// Finish fixes up fields in the edit.
	// This should be called once after filling the edit's fields.
	// This only exists because recordings are dumb and require
	// artists' database IDs rather than their MBIDs.
	Finish(ctx context.Context, db *mbdb.DB) error
}

// makeDesc is a helper function for Edit.Description implementations.
// It returns a slash-separated string consisting of mbid and other.
// Long fields are truncated and empty fields are omitted.
func makeDesc(mbid string, other ...string) string {
	parts := make([]string, 0, len(other)+1)
	if mbid != "" {
		parts = append(parts, strutil.Truncate(mbid, mbidPrefixLen, false))
	}
	for _, s := range other {
		if s != "" {
			parts = append(parts, strutil.Truncate(s, maxDescLen, true))
		}
	}
	if len(parts) == 0 {
		return "[unknown]"
	}
	return strings.Join(parts, " / ")
}

// makeURL is a helper function for Edit.URL implementations.
// The supplied paths should be slash-prefixed, and editPathFmt
// should contain "%v" at the point where mbid should be inserted.
func makeURL(serverURL, createPath, editPathFmt, mbid string) string {
	if mbid != "" {
		return serverURL + fmt.Sprintf(editPathFmt, mbid)
	}
	return serverURL + createPath
}

// setParams copies m to vals for all pairs with a non-empty value.
func setParams(vals url.Values, m map[string]string, prefix string) {
	for k, v := range m {
		if v != "" {
			vals.Set(prefix+k, v)
		}
	}
}
