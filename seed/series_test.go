// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"net/url"
	"strconv"
	"testing"

	"github.com/google/go-cmp/cmp"
)

func TestSeries_URL(t *testing.T) {
	const srvURL = "https://test.musicbrainz.org"
	for _, tc := range []struct{ mbid, want string }{
		{"", srvURL + "/series/create"},
		{"d98928e8-6757-4196-a945-e7145d94d9e4", srvURL + "/series/d98928e8-6757-4196-a945-e7145d94d9e4/edit"},
	} {
		s := Series{MBID: tc.mbid}
		if got := s.URL(srvURL); got != tc.want {
			t.Errorf("MBID %q yielded URL %q; want %q", tc.mbid, got, tc.want)
		}
	}
}

func TestSeries_Params(t *testing.T) {
	s := Series{
		Name:           "A New Series",
		Disambiguation: "for testing",
		Type:           SeriesType_ReleaseGroupSeries,
		OrderingType:   SeriesOrderingType_Automatic,
		Relationships: []Relationship{{
			Target: "0f50beab-d77d-4f0f-ac26-0b87d3e9b11b",
			Type:   LinkType_PartOf_ReleaseGroup_Series,
		}},
		URLs: []URL{{
			URL:      "https://example.org/funding",
			LinkType: LinkType_Crowdfunding_Series_URL,
		}},
		EditNote:    "here's the edit note",
		MakeVotable: true,
	}

	rel := s.Relationships[0]
	want := url.Values{
		"edit-series.name":               {s.Name},
		"edit-series.comment":            {s.Disambiguation},
		"edit-series.type_id":            {strconv.Itoa(int(s.Type))},
		"edit-series.ordering_type_id":   {strconv.Itoa(int(s.OrderingType))},
		"rels.0.target":                  {rel.Target},
		"rels.0.type":                    {strconv.Itoa(int(rel.Type))},
		"edit-series.url.0.text":         {s.URLs[0].URL},
		"edit-series.url.0.link_type_id": {strconv.Itoa(int(s.URLs[0].LinkType))},
		"edit-series.edit_note":          {s.EditNote},
		"edit-series.make_votable":       {"1"},
	}
	if diff := cmp.Diff(want, s.Params()); diff != "" {
		t.Error("Incorrect query params:\n" + diff)
	}
}
