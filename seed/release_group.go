// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"codeberg.org/derat/yambs/mbdb"
)

// ReleaseGroup holds data used to seed the "Add release group" form at
// https://musicbrainz.org/release-group/create and the edit-release-group form
// at https://musicbrainz.org/release-group/<MBID>/edit.
// Note that release groups are automatically created when adding releases.
// See https://musicbrainz.org/doc/Release_Group for more information about
// release group entities.
type ReleaseGroup struct {
	// MBID contains the release group's MBID (for editing an existing release group
	// rather than creating a new one).
	MBID string
	// Name contains the release group's name.
	Name string
	// Artists contains artists credited with the release group.
	Artists []ArtistCredit
	// Disambiguation differentiates this release group from others with similar names.
	// See https://musicbrainz.org/doc/Disambiguation_Comment.
	Disambiguation string
	// PrimaryType describes what kind of release group this is.
	// Note that this field requires an integer database ID rather than the string values used by Release.Types.
	// See https://musicbrainz.org/doc/Release_Group/Type.
	PrimaryType ReleaseGroupTypeID
	// SecondaryTypes contains extra information about what kind of release group this is.
	// Note that this field requires integer database IDs rather than the string values used by Release.Types.
	// See https://musicbrainz.org/doc/Release_Group/Type.
	SecondaryTypes []ReleaseGroupTypeID
	// Relationships contains (non-URL) relationships between this release group and other entities.
	Relationships []Relationship
	// URLs contains relationships between this release group and one or more URLs.
	// See https://musicbrainz.org/doc/Style/Relationships/URLs.
	URLs []URL
	// EditNote contains the note attached to the edit.
	// See https://musicbrainz.org/doc/Edit_Note.
	EditNote string
	// MakeVotable indicates that edits should be made votable even if they would be auto-edits.
	MakeVotable bool
}

func (rg *ReleaseGroup) Entity() Entity { return ReleaseGroupEntity }
func (rg *ReleaseGroup) Description() string {
	return makeDesc(rg.MBID, rg.Name, artistCreditsDesc(rg.Artists))
}
func (rg *ReleaseGroup) URL(serverURL string) string {
	return makeURL(serverURL, "/release-group/create", "/release-group/%v/edit", rg.MBID)
}

func (rg *ReleaseGroup) Params() url.Values {
	vals := make(url.Values)
	if rg.Name != "" {
		vals.Set("edit-release-group.name", rg.Name)
	}
	for i, ac := range rg.Artists {
		ac.setParams(vals, fmt.Sprintf("edit-release-group.artist_credit.names.%d.", i))
	}
	if rg.Disambiguation != "" {
		vals.Set("edit-release-group.comment", rg.Disambiguation)
	}
	if rg.PrimaryType != 0 {
		vals.Set("edit-release-group.primary_type_id", strconv.Itoa(int(rg.PrimaryType)))
	}
	for _, id := range rg.SecondaryTypes {
		vals.Add("edit-release-group.secondary_type_ids", strconv.Itoa(int(id)))
	}
	for i, rel := range rg.Relationships {
		rel.setParams(vals, fmt.Sprintf("rels.%d.", i))
	}
	for i, u := range rg.URLs {
		u.setParams(vals, fmt.Sprintf("edit-release-group.url.%d.", i), rg.Method())
	}
	if rg.EditNote != "" {
		vals.Set("edit-release-group.edit_note", rg.EditNote)
	}
	if rg.MakeVotable {
		vals.Set("edit-release-group.make_votable", "1")
	}
	return vals
}

func (rg *ReleaseGroup) Method() string                                { return http.MethodGet }
func (rg *ReleaseGroup) Serial() bool                                  { return false }
func (rg *ReleaseGroup) Finish(ctx context.Context, db *mbdb.DB) error { return nil }
