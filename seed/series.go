// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"context"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"codeberg.org/derat/yambs/mbdb"
)

// Series holds data used to seed the "Add series" form at https://musicbrainz.org/series/create
// and the edit-series form at https://musicbrainz.org/series/<MBID>/edit.
// See https://musicbrainz.org/doc/Series for more information about series entities.
type Series struct {
	// MBID contains the series's MBID (for editing an existing series rather than creating a new one).
	MBID string
	// Name contains the series's name.
	Name string
	// Disambiguation differentiates this series from other series with similar names.
	// See https://musicbrainz.org/doc/Disambiguation_Comment.
	Disambiguation string
	// Type describes the kind of series that the series is.
	// See https://musicbrainz.org/doc/Series#Type.
	Type SeriesType
	// OrderingType determines whether the series is ordered automatically or manually.
	OrderingType SeriesOrderingType
	// Relationships contains (non-URL) relationships between this series and other entities.
	Relationships []Relationship
	// URLs contains relationships between this series and one or more URLs.
	// See https://musicbrainz.org/doc/Style/Relationships/URLs.
	URLs []URL
	// EditNote contains the note attached to the edit.
	// See https://musicbrainz.org/doc/Edit_Note.
	EditNote string
	// MakeVotable indicates that edits should be made votable even if they would be auto-edits.
	MakeVotable bool
}

func (s *Series) Entity() Entity      { return SeriesEntity }
func (s *Series) Description() string { return makeDesc(s.MBID, s.Name) }
func (s *Series) URL(serverURL string) string {
	return makeURL(serverURL, "/series/create", "/series/%v/edit", s.MBID)
}

func (s *Series) Params() url.Values {
	vals := make(url.Values)
	if s.Name != "" {
		vals.Set("edit-series.name", s.Name)
	}
	if s.Disambiguation != "" {
		vals.Set("edit-series.comment", s.Disambiguation)
	}
	if s.Type != 0 {
		vals.Set("edit-series.type_id", strconv.Itoa(int(s.Type)))
	}
	if s.OrderingType != 0 {
		vals.Set("edit-series.ordering_type_id", strconv.Itoa(int(s.Type)))
	}
	for i, rel := range s.Relationships {
		rel.setParams(vals, fmt.Sprintf("rels.%d.", i))
	}
	for i, u := range s.URLs {
		u.setParams(vals, fmt.Sprintf("edit-series.url.%d.", i), s.Method())
	}
	if s.EditNote != "" {
		vals.Set("edit-series.edit_note", s.EditNote)
	}
	if s.MakeVotable {
		vals.Set("edit-series.make_votable", "1")
	}
	return vals
}

func (s *Series) Method() string                                { return http.MethodGet }
func (s *Series) Serial() bool                                  { return false }
func (s *Series) Finish(ctx context.Context, db *mbdb.DB) error { return nil }
