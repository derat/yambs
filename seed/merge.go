// Copyright 2024 Daniel Erat.
// All rights reserved.

package seed

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"

	"codeberg.org/derat/yambs/mbdb"
)

// Merge contains information about an edit to merge multiple entities of the same type.
type Merge struct {
	entity     Entity
	ids        []int32 // database IDs to merge
	entityComp string  // entity component in URLs, e.g. "recording" or "release_group" (note underscore)
	desc       string  // human-readable description
}

func NewMerge(entity Entity, ids []int32, desc string) (*Merge, error) {
	comp, ok := mergeEntityComps[entity]
	if !ok {
		return nil, fmt.Errorf("don't know how to merge %v entities", entity)
	}
	if len(ids) < 2 {
		return nil, errors.New("need at least 2 entities to merge")
	}

	return &Merge{
		entity:     entity,
		ids:        ids,
		entityComp: comp,
		desc:       desc,
	}, nil
}

// mergeEntityComps maps from entity types to the corresponding path component in merge URLs.
// This is just future-proofing since the server seems to weirdly use /release_group/merge
// instead of the "release-group" form that appears in regular URLs.
var mergeEntityComps = map[Entity]string{
	RecordingEntity: "recording",
}

func (mg *Merge) Entity() Entity              { return MergeEntity }
func (mg *Merge) Description() string         { return mg.desc }
func (mg *Merge) URL(serverURL string) string { return serverURL + "/" + mg.entityComp + "/merge" }
func (mg *Merge) Params() url.Values {
	// At the outer level, make a cancel request to the /merge endpoint to clear the queue.
	// (The server should probably require this to be a POST, but it doesn't -- lucky us.)
	// Then make successive /merge_queue?add-to-merge requests and abuse^Wuse the returnto param
	// to redirect to add the remaining entities to the queue.
	vals := make(url.Values)
	wrap := func() {
		enc := "/" + mg.entityComp + "/merge_queue?" + vals.Encode()
		vals = make(url.Values)
		vals.Set("returnto", enc)
	}
	// I'm adding the IDs in reverse order here so the first one in mg.ids will be at the
	// outermost level, but the server seems to make up its own mind about the order in which
	// the entities should be displayed sometimes. :-/
	for i := len(mg.ids) - 1; i >= 0; i-- {
		// TODO: Should we set the innermost level's "returnto" param to redirect back to the
		// first entity? I think we need the MBID to do that rather than the database row ID,
		// and we'd also need to redirect to e.g. /release-group rather than /release_group
		// (see mergeEntityComps). It seems like the server may automatically redirect back to the
		// target recording by default, anyway.
		if len(vals) > 0 {
			wrap()
		}
		vals.Set("add-to-merge", fmt.Sprintf("%v", mg.ids[i]))
	}
	wrap()
	vals.Set("submit", "cancel")
	return vals
}
func (mg *Merge) Method() string                                { return http.MethodGet }
func (mg *Merge) Serial() bool                                  { return true }
func (mg *Merge) Finish(ctx context.Context, db *mbdb.DB) error { return nil }

func (mg *Merge) EntityForTest() Entity { return mg.entity }
func (mg *Merge) IDsForTest() []int32   { return mg.ids }
